package com.softech.ccpatient.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity.RESULT_OK
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*

import com.softech.ccpatient.R

import butterknife.ButterKnife
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.koushikdutta.ion.Ion
import com.softech.ccpatient.adapters.UploadDocAdapter
import com.softech.ccpatient.apiconnections.ApiConnection
import com.softech.ccpatient.data.models.models.LoadDocuments
import com.softech.ccpatient.interfaces.IWebListener
import com.softech.ccpatient.sharedpreferences.FetchData
import com.softech.ccpatient.utils.*
import kotlinx.android.synthetic.main.fragment_doc_upload.*
import org.json.JSONException
import org.json.JSONObject
import java.io.File
import java.util.ArrayList
import java.util.HashMap

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [UploadDocFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [UploadDocFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class UploadDocFragment : Fragment(), OnItemClick {

    override fun onClick(value: String) {
        folderId = value
    }

    internal var fragmentManager: FragmentManager? = null

    internal var outputFile: File? = null
    private var pDialog: ProgressDialog? = null
    private var recyclerviewDoc: RecyclerView? = null
    internal var mimetype: String? = String()
    var ivDoc: ImageView? = null

    var folderId: String? = null

    internal var documentArrayList = ArrayList<LoadDocuments>()

    private val mListener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }


    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_doc_upload, container, false)
        ButterKnife.bind(this, view)
        fragmentManager = getFragmentManager()

        recyclerviewDoc = view.findViewById<RecyclerView>(R.id.recyclerviewDoc)
        ivDoc = view.findViewById<ImageView>(R.id.ivDoc)

        recyclerviewDoc!!.setLayoutManager(LinearLayoutManager(activity))
        recyclerviewDoc!!.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        recyclerviewDoc!!.setHasFixedSize(true)

        getDocument()

        pDialog = ProgressDialog(activity)
        pDialog!!.setMessage("Loading...")
        pDialog!!.setCancelable(false)

        val selectFileBtn = view.findViewById<Button>(R.id.selectFileBtn)
        selectFileBtn.setOnClickListener() {

            if (checkWriteExternalPermission()) {
                showDialog()
            }
            else{
                ActivityCompat.requestPermissions(activity!!,
                        arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                        110)
            }
        }

        val uploadFileBtn = view.findViewById<Button>(R.id.uploadFileBtn)
        uploadFileBtn.setOnClickListener() {
            if (folderId != null && outputFile != null)
                uploadDocument()
            else
                Toast.makeText(activity, "Plaese select file and folder", Toast.LENGTH_LONG).show()

        }




        return view
    }

    private fun checkWriteExternalPermission(): Boolean {
        val permission = Manifest.permission.READ_EXTERNAL_STORAGE
        val res = activity!!.checkCallingOrSelfPermission(permission)
        return (res == PackageManager.PERMISSION_GRANTED);
    }


    fun getDocument() {
        val userId = FetchData.getData(activity!!, "userId")
        val params = HashMap<String, String>()
        params["action"] = "Load Folders"
                params.put("patientId", userId);
        //        params.put("patientId", "17");
//        params["patientId"] = "337"


        val apiConnection = ApiConnection(object : IWebListener {
            override fun success(response: String?) {

                Log.e("response", "response: " + response)

                if (response != null) {

                    try {
                        val jsonObject = JSONObject(response)
                        val meta = jsonObject.getJSONObject("meta")
                        val code = meta.getString("code")
                        if (Integer.valueOf(code) == 200) {
                            documentArrayList.clear()
                            val responseArray = jsonObject.getJSONArray("response")

                            val gson = Gson()
                            val type = object : TypeToken<ArrayList<LoadDocuments>>() {

                            }.type
                            documentArrayList = gson.fromJson<ArrayList<LoadDocuments>>(responseArray.toString(), type)

                            if (documentArrayList.size > 0) {
                                recyclerviewDoc!!.setVisibility(View.VISIBLE)

                                val mAdapter = UploadDocAdapter(activity, documentArrayList, this@UploadDocFragment)
                                val mLayoutManager = LinearLayoutManager(activity)
                                recyclerviewDoc!!.setLayoutManager(mLayoutManager)
                                recyclerviewDoc!!.setItemAnimator(DefaultItemAnimator())
                                recyclerviewDoc!!.setAdapter(mAdapter)
                                recyclerviewDoc!!.addItemDecoration(DividerItemDecoration(activity!!, DividerItemDecoration.VERTICAL))
                            } else {
                                recyclerviewDoc!!.setVisibility(View.GONE)
                            }

                        } else if (Integer.valueOf(code) == 504) {
                            val message = meta.getString("message")
                            recyclerviewDoc!!.setVisibility(View.GONE)
                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    Logs.showLog("Login", response)
                }
            }

            override fun error(response: String) {
                Log.d(response, response)
            }
        }, activity, params,
                Const.LoadFoldersForPatient)
        apiConnection.makeStringReq()
    }

    private fun showDialog() {
        val dialog = activity?.let { Dialog(it) }
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.custom_dialog)

//        val rvAudio = dialog.findViewById<RelativeLayout>(R.id.rvAudio)
//        val ivAudio = dialog.findViewById<ImageView>(R.id.ivAudio)
//
//        val rvVideo = dialog.findViewById<RelativeLayout>(R.id.rvVideo)
//        val ivVideo = dialog.findViewById<ImageView>(R.id.ivVideo)

        val rvImage = dialog.findViewById<RelativeLayout>(R.id.rvImage)
        val ivImage = dialog.findViewById<ImageView>(R.id.ivImage)

        val rvDocument = dialog.findViewById<RelativeLayout>(R.id.rvDocument)
        val ivDocument = dialog.findViewById<ImageView>(R.id.ivDocument)

        val tvCancel = dialog.findViewById<TextView>(R.id.tvCancel)
        val tvConfirm = dialog.findViewById<TextView>(R.id.tvConfirm)

//        rvAudio.setOnClickListener {
//            ivAudio.setBackgroundResource(R.drawable.ic_tick)
//            ivVideo.setBackgroundResource(0)
//            ivImage.setBackgroundResource(0)
//            ivDocument.setBackgroundResource(0)
//            openAudio()
//            dialog.dismiss()
//        }
//        rvVideo.setOnClickListener {
//            ivAudio.setBackgroundResource(0)
//            ivVideo.setBackgroundResource(R.drawable.ic_tick)
//            ivImage.setBackgroundResource(0)
//            ivDocument.setBackgroundResource(0)
//            openVideo()
//            dialog.dismiss()
//        }
        rvImage.setOnClickListener {
//            ivAudio.setBackgroundResource(0)
//            ivVideo.setBackgroundResource(0)
            ivImage.setBackgroundResource(R.drawable.ic_tick)
            ivDocument.setBackgroundResource(0)
            openImage()
            dialog.dismiss()
        }
        rvDocument.setOnClickListener {
//            ivAudio.setBackgroundResource(0)
//            ivVideo.setBackgroundResource(0)
            ivImage.setBackgroundResource(0)
            ivDocument.setBackgroundResource(R.drawable.ic_tick)
            openDoc()
            dialog.dismiss()
        }

        cancelDoc.setOnClickListener {
            ivDoc!!.setBackgroundResource(R.drawable.file)
            cancelDoc.visibility=View.GONE
            outputFile=null
            mimetype=""
            folderId=null
        }


        tvCancel.setOnClickListener {
            dialog.dismiss()
        }
        tvConfirm.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()

    }

    private fun openImage() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, Const.PICK_IMAGE)
    }

    private fun openAudio() {
//        val intent = Intent(Intent.ACTION_PICK)
//        intent.type = "audio/*"
//        startActivityForResult(intent, Const.PICK_AUDIO)

        val intent = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(intent, Const.PICK_AUDIO)
    }

    private fun openVideo() {
//        val intent = Intent(Intent.ACTION_GET_CONTENT)
//        intent.type = "video/*";
//        startActivityForResult(intent, Const.PICK_VIDEO)

        val intent = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI)
        intent.type = "video/*"
        startActivityForResult(intent, Const.PICK_VIDEO)
    }

    private fun openDoc() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "application/pdf"
        startActivityForResult(intent, Const.PICK_DOC)
    }

    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {

        // TODO: Rename and change types and number of parameters
        fun newInstance(): UploadDocFragment {
            return UploadDocFragment()
        }
    }

    private fun uploadDocument() {
        if (!pDialog?.isShowing!!) {
            pDialog?.show()
        }
        val userId = FetchData.getData(activity!!, "userId")

        Log.d("Call", "Call mimetype:" + outputFile)
        Log.d("Call", "Call folderId:" + folderId)
        Log.d("Call", "Call fileName:" + outputFile?.name)
        Log.d("Call", "Call fileType:" + mimetype)

        Ion.with(this).load(Const.BASE_URL + Const.UPLOAD_DOCUMENT)
                .setMultipartParameter("action", "Document Upload")
                .setMultipartFile("File", mimetype, outputFile)
                .setMultipartParameter("patientId", userId)
                .setMultipartParameter("folderId", folderId)
                .setMultipartParameter("fileName", outputFile?.name)
                .setMultipartParameter("fileType", mimetype)
                .asString()
                .setCallback { e, result ->

                    Log.d("Call", "Call e:" + e)
                    Log.d("Call", "Call result:" + result)

                    try {
                        if (pDialog?.isShowing()!!) {
                            pDialog?.dismiss()
                        }

                        if (result != null){
                            val jsonObject = JSONObject(result)
                            val meta = jsonObject.getJSONObject("meta")
                            val code = meta.getString("code")
                            if (Integer.valueOf(code) == 200) {
                                val message = meta.getString("message")
//                            Toast.makeText(activity, message, Toast.LENGTH_LONG).show()
                                Toast.makeText(activity, "File Uploaded", Toast.LENGTH_LONG).show()


                                // TabsActivity.refreshFragments()

                                val transaction = fragmentManager!!.beginTransaction()
                                transaction.replace(R.id.frag, DocumentFragment.newInstance())
                                transaction.commit()

                            } else if (Integer.valueOf(code) == 404) {
                                val message = jsonObject.getString("error")
                                Toast.makeText(activity, message, Toast.LENGTH_LONG).show()
                            }
                        }
                        else
                        {
                            Toast.makeText(activity,"Please choose correct file",Toast.LENGTH_SHORT).show()
                        }

                    } catch (e1: JSONException) {
                        e1.printStackTrace()
                    }
                }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == RESULT_OK) {

            Log.d("Result", "Result resultCode: " + resultCode)

            val selectedUri = data?.getData()
            if (selectedUri !== null) {
                val path = FileUtils.getPath(activity, selectedUri)

                outputFile = File(path!!)

                var file_size = Integer.parseInt(((outputFile!!.length()/1024).toString()))


                Log.d("Call", "Call mimetype:" + file_size)
                outputFile!!.exists()
                mimetype = FileUtils.getMimeType(File(path))

                ivDoc!!.setBackgroundResource(R.drawable.file_select)
                cancelDoc.visibility=View.VISIBLE

            }

            /*when (requestCode) {
                Const.PICK_AUDIO -> {
                    var audio = data!!.data
                    Log.d("Result", "Result 1:" + audio)
                }
                Const.PICK_DOC -> {
                    var doc = data!!.data
                    Log.d("Result", "Result 2:" + doc)
                }
                Const.PICK_IMAGE -> {
                    var image = data!!.data
                    Log.d("Result", "Result 3:" + image)
                }
                Const.PICK_VIDEO -> {
                    var video = data!!.data
                    Log.d("Result", "Result 4:" + video)
                }
            }*/


        }

        Log.d("Result", "Result Frag: " + data)
    }



}
