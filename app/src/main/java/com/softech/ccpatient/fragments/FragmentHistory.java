package com.softech.ccpatient.fragments;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.softech.ccpatient.R;
import com.softech.ccpatient.adapters.AppointmentsAdapter;
import com.softech.ccpatient.apiconnections.ApiConnection;
import com.softech.ccpatient.data.models.models.PatientAppointments;
import com.softech.ccpatient.interfaces.IWebListener;
import com.softech.ccpatient.sharedpreferences.FetchData;
import com.softech.ccpatient.utils.Const;
import com.softech.ccpatient.utils.Logs;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import butterknife.BindView;
import butterknife.ButterKnife;


public class FragmentHistory extends Fragment implements IWebListener, SwipeRefreshLayout.OnRefreshListener, AppointmentsAdapter.OnDeleteAptClickListenter {

    @BindView(R.id.rvAppointments)
    RecyclerView rvAppointments;
    @BindView(R.id.tvFoundNothing)
    TextView tvFoundNothing;
    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout swiperefresh;

    public FragmentHistory() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history, container, false);
        ButterKnife.bind(this, view);

        getPastAppointments();
        swiperefresh.setOnRefreshListener(FragmentHistory.this);

        return view;
    }


    public void getPastAppointments() {
        String userId = FetchData.getData(getActivity(), "userId");

        Map<String, String> params = new HashMap<String, String>();
        params.put("action", "List Of Patient Past Appointments");
        params.put("patId", userId);
//        params.put("patId", "17");
        ApiConnection apiConnection = new ApiConnection(FragmentHistory.this,
                getActivity(), params, Const.VIEWPASTAPPOINTMENTS);
        apiConnection.makeStringReq();
    }

    @Override
    public void success(String response) {
        if (response != null) {

            Log.d("Response", "Response response: " + response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONObject meta = jsonObject.getJSONObject("meta");
                String code = meta.getString("code");
                if (Integer.valueOf(code) == 200) {
                    JSONArray responseArr = jsonObject.getJSONArray("response");

                    Gson gson = new Gson();
                    Type type = new TypeToken<ArrayList<PatientAppointments>>() {
                    }.getType();

                    ArrayList<PatientAppointments> pastAppointmentsArrayList = gson.fromJson(responseArr.toString(), type);

                    if (pastAppointmentsArrayList.size() > 0) {
                        rvAppointments.setVisibility(View.VISIBLE);
                        tvFoundNothing.setVisibility(View.GONE);

                        AppointmentsAdapter mAdapter = new AppointmentsAdapter(pastAppointmentsArrayList, getActivity(), this);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                        rvAppointments.setLayoutManager(mLayoutManager);
                        rvAppointments.setItemAnimator(new DefaultItemAnimator());
                        rvAppointments.setAdapter(mAdapter);
                        rvAppointments.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
                    } else {
                        rvAppointments.setVisibility(View.GONE);
                        tvFoundNothing.setVisibility(View.VISIBLE);
                    }


                } else if (Integer.valueOf(code) == 504) {
                    String message = meta.getString("message");
                    rvAppointments.setVisibility(View.GONE);
                    tvFoundNothing.setVisibility(View.VISIBLE);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            Logs.showLog("Login", response);
        }
    }

    @Override
    public void error(String response) {

    }


    @Override
    public void onRefresh() {
        getPastAppointments();
        swiperefresh.setRefreshing(false);
    }

    @Override
    public void OnDeleteAptClick(View caller, PatientAppointments mItem) {
//        callingDeleteAppointmentService(mItem);


    }
}