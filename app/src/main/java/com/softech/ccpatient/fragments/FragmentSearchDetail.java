package com.softech.ccpatient.fragments;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.softech.ccpatient.R;
import com.softech.ccpatient.adapters.AppointmentsAdapter;
import com.softech.ccpatient.apiconnections.ApiConnection;
import com.softech.ccpatient.data.models.models.PatientAppointments;
import com.softech.ccpatient.interfaces.IWebListener;
import com.softech.ccpatient.sharedpreferences.FetchData;
import com.softech.ccpatient.utils.Const;
import com.softech.ccpatient.utils.Logs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FragmentSearchDetail extends Fragment implements IWebListener {


    @BindView(R.id.recyclViewAppointments)
    RecyclerView recyclViewAppointments;
    @BindView(R.id.textViewFoundNothing)
    TextView textViewFoundNothing;

    ArrayList<PatientAppointments> appointmentsModelArrayList = new ArrayList<PatientAppointments>();
    private AppointmentsAdapter mAdapter;

    public FragmentSearchDetail() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_appointments, container, false);
        ButterKnife.bind(this, view);

        getAppointments();


        return view;
    }

    private void getAppointments() {
        String consultantID = FetchData.getData(getActivity(), "userId");
        String access_token = FetchData.getData(getActivity(), "access_token");

        Map<String, String> params = new HashMap<String, String>();
        params.put("action", "List Of Consultant Appointments");
        params.put("consultantId", consultantID);
        params.put("access_token", access_token);

        ApiConnection apiConnection = new ApiConnection(FragmentSearchDetail.this,
                getActivity(), params, Const.VIEWAPPOINTMENTS);
        apiConnection.makeStringReq();

    }

    @Override
    public void success(String response) {
        if (response != null) {

            try {

                JSONObject jsonObject = new JSONObject(response);
                JSONObject meta = jsonObject.getJSONObject("meta");
                String code = meta.getString("code");
                    if (Integer.valueOf(code) == 200) {
                        String message = meta.getString("message");

                        JSONArray jsonArray = jsonObject.getJSONArray("response");
                        Gson gson = new Gson();
                        Type type = new TypeToken<ArrayList<PatientAppointments>>() {
                        }.getType();
                        appointmentsModelArrayList = gson.fromJson(jsonArray.toString(), type);

                        mAdapter = new AppointmentsAdapter(appointmentsModelArrayList, getActivity());
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                        recyclViewAppointments.setLayoutManager(mLayoutManager);
                        recyclViewAppointments.setItemAnimator(new DefaultItemAnimator());
                        recyclViewAppointments.setAdapter(mAdapter);
                        // mAdapter.notifyDataSetChanged();
                        recyclViewAppointments.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));


                    } else if (Integer.valueOf(code) == 504) {
                        String message = meta.getString("message");

                        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                        recyclViewAppointments.setVisibility(View.GONE);
                        textViewFoundNothing.setVisibility(View.VISIBLE);

                    }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Logs.showLog("Login", response);

        }
        Log.d("Appointments", response);
    }

    @Override
    public void error(String response) {
        Log.d(response, response);
    }
}