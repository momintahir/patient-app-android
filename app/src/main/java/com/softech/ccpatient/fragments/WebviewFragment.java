package com.softech.ccpatient.fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.github.barteksc.pdfviewer.PDFView;
import com.softech.ccpatient.R;
import com.softech.ccpatient.sharedpreferences.SaveData;
import com.softech.ccpatient.utils.AESEncryption;
import com.softech.ccpatient.utils.Const;

import java.io.File;
import java.io.FileOutputStream;

import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link WebviewFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link WebviewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WebviewFragment extends Fragment {
    FragmentManager fragmentManager;

//    @BindView(R.id.webView)
//    WebView webView;
    PDFView fileWebView;
    ProgressDialog pDialog;
    ImageView fileImageView;
    private OnFragmentInteractionListener mListener;

    public WebviewFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static WebviewFragment newInstance() {
        WebviewFragment fragment = new WebviewFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_webview, container, false);
        ButterKnife.bind(this, view);
        fragmentManager = getFragmentManager();
        fileWebView=view.findViewById(R.id.fileWebView);
        fileImageView=view.findViewById(R.id.fileImageView);


//        WebSettings webSettings = webView.getSettings();
//        webSettings.setJavaScriptEnabled(true);
// Zoom in out start
//        webSettings.setBuiltInZoomControls(true);
// Zoom in out end

// Zoom button hide start
//        webSettings.setDisplayZoomControls(false);
// Zoom button hide end

// initial Zoom out start
//        webSettings.setLoadWithOverviewMode(true);
//        webSettings.setUseWideViewPort(true);
// initial Zoom out end

        String filePath = getArguments().getString(Const.FILE_PATH);
        String fileType = getArguments().getString("fileType");
        String file = getArguments().getString("file");


        File inputfile = null;
        File outputfile = null;
        String decryptedString = AESEncryption.decrypt(filePath, Const.Encryption_Key, Const.Encryption_IV);
        SaveData.SaveData(getActivity(), "webLink", decryptedString);

        byte[] decodedBytes= Base64.decode(file,1);
        inputfile = getfileFromBase64String(decodedBytes, fileType);
        outputfile = getfileFromBase64String(decodedBytes, fileType);
        AESEncryption.encryptFile(2, inputfile, Const.Encryption_Key, Const.Encryption_IV, outputfile);

//        showProgressDialog();

        Log.d("Path","filePath: "+filePath);
//        webView.setWebViewClient(new WebViewClient() {
//            @Override
//            public void onPageStarted(WebView view, String url, Bitmap favicon) {
//                super.onPageStarted(view, url, favicon);
////                showProgressDialog();
//            }
//
//            @Override
//            public void onPageFinished(WebView view, String url) {
//                super.onPageFinished(view, url);
//                hideProgressDialog();
//            }
//
//        });
//        if(fileType.equals("application/pdf"))
//        {
//            String myPdfUrl = outputfile.getAbsolutePath();
//            webView.loadUrl("http://docs.google.com/gview?embedded=true&url="+myPdfUrl);
//        }
//        else
//        {
//            webView.loadUrl(outputfile.getAbsolutePath());
//        }

        if (fileType.contains("pdf")) {
            fileWebView.setVisibility(View.VISIBLE);
            fileImageView.setVisibility(View.GONE);
            fileWebView.fromFile(new File(outputfile.getAbsolutePath()))
                    .defaultPage(0)
                    .enableSwipe(true).load();
        }
        else if(fileType.contains("jpeg")||fileType.contains("png"))
        {
            fileImageView.setVisibility(View.VISIBLE);
            fileWebView.setVisibility(View.GONE);

//            Glide.with(this).load(new File(outputfile.getAbsolutePath()).

                    Glide.with(getActivity())
                    .load(outputfile.getAbsoluteFile())
                    .into(fileImageView);// Uri of the picture
        }

        return view;
    }


    private void showProgressDialog() {
        try {
            if (!pDialog.isShowing() && pDialog != null) {
                pDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public File getfileFromBase64String(byte[] dataBytes, String filetype) {
        File file = null;

        try {
            filetype = filetype.substring(filetype.length() - 3);

            if (filetype.equals("peg")) {
                filetype = "jpg";
            }
            File directory = getContext().getCacheDir();
            Long tsLong = System.currentTimeMillis() / 1000;
            String ts = tsLong.toString();

            String filename = "ReportTest_" + ts + "." + filetype;

            file = new File(directory, filename);

            FileOutputStream fos = new FileOutputStream(file);
            fos.write(dataBytes);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return file;
    }

    private void hideProgressDialog() {
        try {
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


}
