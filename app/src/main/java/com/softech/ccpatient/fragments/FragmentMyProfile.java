package com.softech.ccpatient.fragments;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.softech.ccpatient.activities.ChangePasswordActivity;
import com.softech.ccpatient.activities.LoginActivity;
import com.softech.ccpatient.R;
import com.softech.ccpatient.apiconnections.ApiConnection;
import com.softech.ccpatient.apiconnections.ApiConnectionBackground;
import com.softech.ccpatient.interfaces.IWebListener;
import com.softech.ccpatient.sharedpreferences.FetchData;
import com.softech.ccpatient.sharedpreferences.SaveData;
import com.softech.ccpatient.utils.AESEncryption;
import com.softech.ccpatient.utils.CallBacks;
import com.softech.ccpatient.utils.Const;
import com.softech.ccpatient.utils.Dialogues;
import com.softech.ccpatient.utils.Logs;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.zelory.compressor.Compressor;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;


public class FragmentMyProfile extends Fragment {

    @BindView(R.id.etFirstName)
    EditText etFirstName;
    @BindView(R.id.etLastName)
    EditText etLastName;
    @BindView(R.id.etPhoneNumber)
    EditText etPhoneNumber;
    @BindView(R.id.tvDOB)
    TextView tvDOB;
    @BindView(R.id.etCity)
    EditText etCity;
    @BindView(R.id.etAddress)
    EditText etAddress;
    @BindView(R.id.tvName)
    TextView tName;

    @BindView(R.id.btnUpdateProfile)
    Button btnUpdateProfile;
    @BindView(R.id.btnChangePassword)
    Button btnChangePassword;
    @BindView(R.id.btnLogout)
    Button btnLogout;
    @BindView(R.id.ivPic)
    ImageView ivPic;

    @BindView(R.id.tvAddress)
    TextView tvAddress;
    @BindView(R.id.tvPhone)
    TextView tvPhone;


    final Calendar myCalendar = Calendar.getInstance();
    File mFile;
    ProgressDialog pDialog;

    public FragmentMyProfile() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_myprofile, container, false);

        ButterKnife.bind(this, view);

        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);

        ivPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectProfileImage();

            }
        });

        btnChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ChangePasswordActivity.class));
            }
        });

        btnUpdateProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etFirstName.getText().length() > 0 && etLastName.getText().length() > 0
                        && tvDOB.getText().length() > 0 && etPhoneNumber.getText().length() == 16
                        && etAddress.getText().length() > 0 && etCity.getText().length() > 0) {
                    updateProfile();
                }
                else {
                    Toast.makeText(getActivity(), "Please select all fields", Toast.LENGTH_SHORT).show();
                }
            }
        });

        tvDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DatePickerDialog dialog =  new DatePickerDialog(getContext(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                dialog.show();
            }
        });

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logOut();
            }
        });

        getProfile();

        etPhoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if(i ==11)
                {
                    String input = etPhoneNumber.getText().toString();
                    final String number = input.replaceFirst("(\\d{2})(\\d{3})(\\d+)", "(+$1)$2-$3");//(+92)XXX-XXXXXXX
                    System.out.println(number);
                    etPhoneNumber.setText(number);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        return view;
    }

    private void logOut() {

        Dialogues.yesnoDialogue(getActivity(), "Are you sure you want to logout?", new CallBacks() {
            @Override
            public void yes() {

                String email= FetchData.getData(getActivity(), "emailAddress");

                Map<String, String> params1 = new HashMap<String, String>();
                String encrypt = AESEncryption.encrypt(email, Const.Encryption_Key, Const.Encryption_IV);
                params1.put("Email", encrypt);

                ApiConnection apiConnection = new ApiConnection(new IWebListener() {
                    @Override
                    public void success(String response) {
                        if (response != null) {

                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject meta = jsonObject.getJSONObject("meta");
                                String code = meta.getString("code");
                                if (Integer.valueOf(code) == 200) {
                                    Log.d("Let's see Success",response.toString());
                                    SaveData.SaveData(getActivity(), "exists", "false");
                                    SharedPreferences preferences = getActivity().getSharedPreferences("appData", Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = preferences.edit();
                                    editor.clear();
                                    editor.apply();
//                        getContext().stopService(new Intent(getActivity(), MyService.class));

                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    getActivity().finish();
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            Logs.showLog("Login", response);
                        }
                    }

                    @Override
                    public void error(String response) {

                    }
                },
                        getActivity(), params1, Const.LOGOUT);
                apiConnection.makeStringReq();







//                Map<String, String> params = new HashMap<String, String>();
//                String email= FetchData.getData(getActivity(), "emailAddress");
//                params.put("email", email);
//                params.put("IsOnline", String.valueOf(0));
//                ApiConnection apiConnection = new ApiConnection(new IWebListener() {
//                    @Override
//                    public void success(String response) {
//
//
//
//
//
//                    }
//
//                    @Override
//                    public void error(String response) {
//
//                    }
//                }, params, Const.SEND_ONLINE_STATUS);
//                apiConnection.makeStringReqService();

            }

            @Override
            public void no() {

            }
        });

    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }

    };

    private void updateLabel() {
        String myFormat = "MM/dd/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        tvDOB.setText(sdf.format(myCalendar.getTime()));
    }

    private void updateProfile() {

        Map<String, String> params = new HashMap<String, String>();
        String userId = FetchData.getData(getActivity(), "userId");

        params.put("action", "Create New User");
        params.put("vFirstName", etFirstName.getText().toString());
        params.put("vLastName", etLastName.getText().toString());
        params.put("dDOB", tvDOB.getText().toString());
        params.put("IPatID", userId);
        params.put("vCellNumber", etPhoneNumber.getText().toString());
        params.put("vAddress", etAddress.getText().toString());
        params.put("vCity", etCity.getText().toString());
        params.put("vCountry", "PK");

        JSONObject jsonObject = new JSONObject(params);
        try {

            String encrypt = AESEncryption.encrypt(jsonObject.toString(), Const.Encryption_Key, Const.Encryption_IV);
            Log.d("dsasda", encrypt);
            Map<String, String> finalParams = new HashMap<String, String>();
            finalParams.put("encryptObj", encrypt);

            ApiConnection apiConnection = new ApiConnection(new IWebListener() {
                @Override
                public void success(String response) {
                    if (response != null) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONObject meta = jsonObject.getJSONObject("meta");
                            String code = meta.getString("code");
                            if (Integer.valueOf(code) == 200) {
                                Toast.makeText(getActivity(), "Profile Updated.", Toast.LENGTH_SHORT).show();
                            }

                            tvAddress.setText(etAddress.getText().toString());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Logs.showLog("Login", response);
                    }
                }

                @Override
                public void error(String response) {
                    try{
                        Logs.showLog("Login", response);
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                }
            }, getActivity(), finalParams, Const.UPDATEPROFILE);
            apiConnection.makeStringReq();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void updateProfileWithImage(File file) {
        if (!pDialog.isShowing()) {
            pDialog.show();
        }
        int filesize = Integer.parseInt(String.valueOf(file.length()/1024));
        //   Toast.makeText(getActivity(), ""+filesize, Toast.LENGTH_SHORT).show();

        String userId = FetchData.getData(getActivity(), "userId");
        File  compressedImageFile = null;

        try {
            compressedImageFile = new Compressor(getActivity()).compressToFile(file);
            int file_size = Integer.parseInt(String.valueOf(compressedImageFile.length()/1024));
            // Toast.makeText(getActivity(), ""+file_size, Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String mimetype = FetchData.getMimeType(mFile.getPath());
        Ion.with(this)
                .load(Const.BASE_URL + Const.UPDATEPROFILEIMAGE)
                .setMultipartParameter("action", "Patient Profile Picture Update")
                .setMultipartParameter("patientId", userId)
                .setMultipartFile("imageFile", mimetype, compressedImageFile)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        if (pDialog.isShowing()) {
                            pDialog.dismiss();
                        }
                        if (result != null) {

                            try {
                                JSONObject jsonObject = new JSONObject(result);
                                JSONObject meta = jsonObject.getJSONObject("meta");
                                String code = meta.getString("code");
                                if (Integer.valueOf(code) == 200) {
                                    Toast.makeText(getActivity(), "Profile Updated.", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e1) {
                                e.printStackTrace();
                            }

                            Logs.showLog("Profile update", result);
                        }
                    }
                });
    }

    private void getProfile() {

        Map<String, String> params = new HashMap<String, String>();
        String userId = FetchData.getData(getActivity(), "userId");

        params.put("action", "Patient Profile");
        params.put("patientId", userId);

        ApiConnectionBackground apiConnection = new ApiConnectionBackground(new IWebListener() {
            @Override
            public void success(String response) {

                if (response != null) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject meta = jsonObject.getJSONObject("meta");
                        String code = meta.getString("code");
                        if (Integer.valueOf(code) == 200) {
                            JSONObject jsonResponse = jsonObject.getJSONObject("response");

                            String firstName = jsonResponse.getString("FirstName");
                            String lastName = jsonResponse.getString("LastName");
                            String fullName = jsonResponse.getString("FullName");
                            String dob = jsonResponse.getString("DOB");
                            String gender = jsonResponse.getString("Gender");
                            String cellNumber = jsonResponse.getString("CellNumber");
                            String address = jsonResponse.getString("Address");
                            String city = jsonResponse.getString("City");
                            String country = jsonResponse.getString("Country");
                            String mrn = jsonResponse.getString("MRN");
                            String avatar = jsonResponse.getString("Avatar");
                            String filPath = jsonResponse.getString("FilePath");

                            tName.setText(fullName);
                            etFirstName.setText(firstName);
                            etLastName.setText(lastName);
                            etPhoneNumber.setText(cellNumber);
                            etAddress.setText(address);
                            etCity.setText(city);
                            tvDOB.setText(dob);

                            tvAddress.setText(address);
                            tvPhone.setText(cellNumber);

                            if (!filPath.isEmpty())
//                                Picasso.get()
//                                        .load(filPath)
//                                .placeholder(R.drawable.no_img)
//                                        .into(ivPic);


                                    Glide
                                    .with(getContext())
                                    .load(filPath)
                                    .centerCrop()
                                            .skipMemoryCache(true)
                                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                                            .placeholder(R.drawable.no_img)
                                    .into(ivPic);

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Logs.showLog("Login", response);
                }
            }

            @Override
            public void error(String response) {
                System.out.println(""+response);
            }
        }, getActivity(), params, Const.GETPROFILE);
        apiConnection.makeStringReq();

    }


    /*public void getSpecialities() {
        Map<String, String> params = new HashMap<String, String>();

        ApiConnection apiConnection = new ApiConnection(new IWebListener() {
            @Override
            public void success(String response) {

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject meta = jsonObject.getJSONObject("meta");
                        String code = meta.getString("code");
                        if (Integer.valueOf(code) == 200) {
                            JSONArray jsonResponse = jsonObject.getJSONArray("response");
                            Gson gson = new Gson();
                            Type type = new TypeToken<ArrayList<SpecialitiesModel>>() {
                            }.getType();
                            specialitiesModelList = gson.fromJson(jsonResponse.toString(), type);
                            Logs.showLog("Login", response);

                            ArrayAdapter<SpecialitiesModel> adapter =
                                    new ArrayAdapter<SpecialitiesModel>(getActivity(), R.layout.spinner, specialitiesModelList);


                            adapter.setDropDownViewResource(R.layout.spinner);
                            spinnerSpeciality.setAdapter(adapter);
                            spinnerSpeciality.setPrompt("Select Site");

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Logs.showLog("Login", response);
                }
                getProfile();
            }

            @Override
            public void error(String response) {
                getProfile();
            }
        }, getActivity(), params, Const.GETSPECIALITIES);
        apiConnection.makeStringReq();
    }
*/
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, getActivity(), new DefaultCallback() {
            @Override
            public void onImagesPicked(@NonNull List<File> list, EasyImage.ImageSource imageSource, int i) {
                mFile = list.get(0);
                File inputfile = new File(mFile.getPath());
                Glide.with(getActivity()).load(mFile).into(ivPic);
                if (mFile != null) {
                    updateProfileWithImage(inputfile);
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 100:

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (FetchData.CheckPermissionFragment(FragmentMyProfile.this, "Camera", Manifest.permission.CAMERA, getActivity(), 100)) {
                        EasyImage.openCameraForImage(FragmentMyProfile.this, 100);
                    }
                } else {
                    if (FetchData.CheckPermissionFragment(FragmentMyProfile.this, "Camera", Manifest.permission.CAMERA, getActivity(), 100)) {
                        EasyImage.openCameraForImage(FragmentMyProfile.this, 100);
                    }
                }
                break;
            case 101:

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (FetchData.CheckPermissionFragment(FragmentMyProfile.this, "Storage", Manifest.permission.READ_EXTERNAL_STORAGE, getActivity(), 101)) {
                        EasyImage.openGallery(FragmentMyProfile.this, 101);
                    }
                } else {
                    if (FetchData.CheckPermissionFragment(FragmentMyProfile.this,"Storage", Manifest.permission.READ_EXTERNAL_STORAGE, getActivity(), 101)) {
                        EasyImage.openGallery(FragmentMyProfile.this, 101);
                    }
                }

                break;
        }
    }

    private void selectProfileImage() {
        final CharSequence[] items = {"Select Camera", "Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Photo");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Select Camera")) {
                    if (FetchData.CheckPermissionFragment(FragmentMyProfile.this, "Camera", Manifest.permission.CAMERA, getActivity(), 100)) {
                        EasyImage.openCameraForImage(FragmentMyProfile.this, 100);
                    }
                } else if (items[item].equals("Gallery")) {
                    if (FetchData.CheckPermissionFragment(FragmentMyProfile.this, "Storage", Manifest.permission.WRITE_EXTERNAL_STORAGE, getActivity(), 101)) {
                        EasyImage.openGallery(FragmentMyProfile.this, 101);
                    }
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


}