package com.softech.ccpatient.fragments;

import android.app.DatePickerDialog;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.softech.ccpatient.R;
import com.softech.ccpatient.adapters.AppointmentsAdapter;
import com.softech.ccpatient.apiconnections.ApiConnection;
import com.softech.ccpatient.data.models.models.PatientAppointments;
import com.softech.ccpatient.data.models.models.CliniclistModel;
import com.softech.ccpatient.interfaces.IWebListener;
import com.softech.ccpatient.sharedpreferences.FetchData;
import com.softech.ccpatient.utils.Const;
import com.softech.ccpatient.utils.Logs;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import butterknife.BindView;
import butterknife.ButterKnife;


public class FragmentAppointments extends Fragment implements IWebListener, SwipeRefreshLayout.OnRefreshListener, AppointmentsAdapter.OnDeleteAptClickListenter {


    @BindView(R.id.recyclViewAppointments)
    RecyclerView recyclViewAppointments;
    @BindView(R.id.textViewFoundNothing)
    TextView textViewFoundNothing;
    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout swiperefresh;
    @BindView(R.id.leftarrow)
    ImageView leftArrow;
    @BindView(R.id.rightarrow)
    ImageView rightarrow;
    String clinicID, selectedDate;
    Date currentDated;
    final Calendar myCalendar = Calendar.getInstance();
    ArrayList<PatientAppointments> patientAppointmentsArrayList = new ArrayList<PatientAppointments>();
    private AppointmentsAdapter mAdapter;

    public FragmentAppointments() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_appointments, container, false);
        ButterKnife.bind(this, view);
        String ClinicList = FetchData.getData(getActivity(), "cliniclist");
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<CliniclistModel>>() {
        }.getType();

        final ArrayList<CliniclistModel> connectedClinicsModelArrayList = gson.fromJson(ClinicList.toString(), type);

        ArrayAdapter<CliniclistModel> adapter =
                new ArrayAdapter<CliniclistModel>(getActivity(), R.layout.spinner, connectedClinicsModelArrayList);
        adapter.setDropDownViewResource(R.layout.spinner);

        getAppointments();

        swiperefresh.setOnRefreshListener(FragmentAppointments.this);

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };


        return view;
    }

    public String getCurrentDate(Date date) {
        System.out.println("Current time => " + date);
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat df1 = new SimpleDateFormat("MMM dd, yyyy");
        String newDate = df1.format(date);
        selectedDate = df.format(date);
        Log.d("new Date", newDate);
        currentDated = FetchData.dateFromString(selectedDate);
        return newDate;
    }

    private void updateLabel() {

        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
    }

    public void getAppointments() {
        patientAppointmentsArrayList.clear();
        String userId = FetchData.getData(getActivity(), "userId");

        Map<String, String> params = new HashMap<String, String>();
        params.put("action", "List Of Patient Appointments");
        params.put("patId", userId);
//        params.put("patId", "17");

        ApiConnection apiConnection = new ApiConnection(FragmentAppointments.this,
                getActivity(), params, Const.VIEWAPPOINTMENTS);
        apiConnection.makeStringReq();
    }

    @Override
    public void success(String response) {
        if (response != null) {

            Log.d("Response","Response response: "+response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONObject meta = jsonObject.getJSONObject("meta");
                String code = meta.getString("code");
                if (Integer.valueOf(code) == 200) {
                    JSONObject jsonResponse = jsonObject.getJSONObject("response");
                    JSONArray UpcomingAppointments = jsonResponse.getJSONArray("UpcomingAppointments");
                    Gson gson = new Gson();
                    Type type = new TypeToken<ArrayList<PatientAppointments>>() {
                    }.getType();
                    patientAppointmentsArrayList.clear();
                    patientAppointmentsArrayList = gson.fromJson(UpcomingAppointments.toString(), type);

                    if (patientAppointmentsArrayList.size() > 0) {
                        recyclViewAppointments.setVisibility(View.VISIBLE);
                        textViewFoundNothing.setVisibility(View.GONE);

                        mAdapter = new AppointmentsAdapter(patientAppointmentsArrayList, getActivity(), this);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                        recyclViewAppointments.setLayoutManager(mLayoutManager);
                        recyclViewAppointments.setItemAnimator(new DefaultItemAnimator());
                        recyclViewAppointments.setAdapter(mAdapter);
                        // mAdapter.notifyDataSetChanged();
                        recyclViewAppointments.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
                    } else {

                        recyclViewAppointments.setVisibility(View.GONE);
                        textViewFoundNothing.setVisibility(View.VISIBLE);
                    }


                } else if (Integer.valueOf(code) == 504) {
                    String message = meta.getString("message");

                    recyclViewAppointments.setVisibility(View.GONE);
                    textViewFoundNothing.setVisibility(View.VISIBLE);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            Logs.showLog("Login", response);
        }
        Log.d("Appointments", response);
    }

    @Override
    public void error(String response) {

    }

    public void callingDeleteAppointmentService(PatientAppointments model) {
        String accesstoken = FetchData.getData(getActivity(), "access_token");
        String patientID = FetchData.getData(getActivity(), "userId");
        Map<String, String> params = new HashMap<String, String>();
        params.put("appointmentId", model.getIAppID());
        params.put("consultantId", model.getIConsultID());
        params.put("patientId", patientID);

        ApiConnection apiConnection = new ApiConnection(new IWebListener() {
            @Override
            public void success(String response) {
                Log.d("asd","asd");
                getAppointments();
            }


            @Override
            public void error(String response) {

            }
        },
                getActivity(), params, Const.ACTION_DELETE_APPOINTMENT);
        apiConnection.makeStringReq();

    }

    @Override
    public void onRefresh() {
        getAppointments();
        swiperefresh.setRefreshing(false);
    }

    @Override
    public void OnDeleteAptClick(View caller, PatientAppointments mItem) {
        callingDeleteAppointmentService(mItem);


    }

}