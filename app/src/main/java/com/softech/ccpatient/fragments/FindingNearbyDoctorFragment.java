package com.softech.ccpatient.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.softech.ccpatient.R;
import com.softech.ccpatient.databinding.FragmentFindDoctorBinding;
import com.softech.ccpatient.databinding.FragmentFindingNearbyDoctorBinding;
import com.softech.ccpatient.utils.ProgressBarAnimation;


/**
 * A simple {@link Fragment} subclass.
 */
public class FindingNearbyDoctorFragment extends Fragment implements View.OnClickListener {

    FragmentFindingNearbyDoctorBinding binding;
    FoundDoctorFragment foundDoctorFragment;
    AnimatedVectorDrawableCompat avdProgress;
    ProgressBarAnimation progressBarAnimation;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_finding_nearby_doctor, container, false);

        initialize();
        initClick();

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressBarAnimation.repeatAnimation();

    }

    private void initialize() {
        avdProgress = AnimatedVectorDrawableCompat.create(getActivity(), R.drawable.avd_line);
        binding.ivLine.setBackground(avdProgress);
        progressBarAnimation = new ProgressBarAnimation(avdProgress);
    }

    private void initClick() {
        binding.cancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch(v.getId()) {

            case R.id.cancel:
                launchFragment();

                break;

        }
    }

    private void launchFragment(){

        progressBarAnimation.stopProgressAnimation();
        foundDoctorFragment = new FoundDoctorFragment();
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.bottom_fragment,foundDoctorFragment  );
        ft.commit();
    }
}
