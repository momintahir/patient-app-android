package com.softech.ccpatient.fragments;

import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.softech.ccpatient.R;
import com.softech.ccpatient.adapters.DocumentAdapter;
import com.softech.ccpatient.apiconnections.ApiConnection;
import com.softech.ccpatient.data.models.models.LoadDocuments;
import com.softech.ccpatient.interfaces.IWebListener;
import com.softech.ccpatient.sharedpreferences.FetchData;
import com.softech.ccpatient.utils.Const;
import com.softech.ccpatient.utils.Logs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DocumentFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DocumentFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DocumentFragment extends Fragment implements IWebListener, SwipeRefreshLayout.OnRefreshListener  {
    FragmentManager fragmentManager;

    @BindView(R.id.rvDocuments)
    RecyclerView rvDocuments;
    @BindView(R.id.tvFoundNothing)
    TextView tvFoundNothing;
    ArrayList<LoadDocuments> documentArrayList = new ArrayList<LoadDocuments>();
    private DocumentAdapter mAdapter;

    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout swiperefresh;

    private OnFragmentInteractionListener mListener;

    public DocumentFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static DocumentFragment newInstance() {
        DocumentFragment fragment = new DocumentFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public void getDocument() {
        String userId = FetchData.getData(getActivity(), "userId");
        Map<String, String> params = new HashMap<String, String>();
        params.put("action", "Load Folders");
        params.put("patientId", userId);

        ApiConnection apiConnection = new ApiConnection(DocumentFragment.this,
                getActivity(), params, Const.LoadFoldersForPatient);
        apiConnection.makeStringReq();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_document, container, false);
        ButterKnife.bind(this, view);
        fragmentManager = getFragmentManager();


        getDocument();
        swiperefresh.setOnRefreshListener(DocumentFragment.this);
        return view;
    }

    @Override
    public void success(String response) {
        if (response != null) {

            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONObject meta = jsonObject.getJSONObject("meta");
                String code = meta.getString("code");
                if (Integer.valueOf(code) == 200) {
                    documentArrayList .clear();
                    JSONArray responseArray = jsonObject.getJSONArray("response");

                    Gson gson = new Gson();
                    Type type = new TypeToken<ArrayList<LoadDocuments>>() {
                    }.getType();
                    documentArrayList  = gson.fromJson(responseArray.toString(), type);

                    if (documentArrayList .size() > 0) {
                        rvDocuments.setVisibility(View.VISIBLE);
                        tvFoundNothing.setVisibility(View.GONE);
                        mAdapter = new DocumentAdapter(getActivity(), documentArrayList);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                        rvDocuments.setLayoutManager(mLayoutManager);
                        rvDocuments.setItemAnimator(new DefaultItemAnimator());
                        rvDocuments.setAdapter(mAdapter);
                        rvDocuments.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
                    } else {
                        rvDocuments.setVisibility(View.GONE);
                        tvFoundNothing.setVisibility(View.VISIBLE);
                    }

                } else if (Integer.valueOf(code) == 504) {
                    String message = meta.getString("message");
                    rvDocuments.setVisibility(View.GONE);
                    tvFoundNothing.setVisibility(View.VISIBLE);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            Logs.showLog("Login", response);
        }

    }

    @Override
    public void error(String response) {

    }

    // TODO: Rename method, update argument and hook method into UI event


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onRefresh() {
        getDocument();
        swiperefresh.setRefreshing(false);
    }
}
