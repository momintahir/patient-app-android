package com.softech.ccpatient.fragments;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.softech.ccpatient.R;
import com.softech.ccpatient.databinding.FragmentFoundDoctorBinding;


/**
 * A simple {@link Fragment} subclass.
 */
public class FoundDoctorFragment extends Fragment {

    FragmentFoundDoctorBinding binding;
    UpdateMap updateMap ;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_found_doctor, container, false);

        updateMap = (UpdateMap) getActivity();
        updateMap.update(true);

        return binding.getRoot();
    }



    public interface UpdateMap {
        void update(boolean status);
    }


}
