package com.softech.ccpatient.fragments;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.softech.ccpatient.R;
import com.softech.ccpatient.adapters.SearchAdapter;
import com.softech.ccpatient.apiconnections.ApiConnection;
import com.softech.ccpatient.data.models.models.CliniclistModel;
import com.softech.ccpatient.data.models.models.SearchAppointmentModel;
import com.softech.ccpatient.interfaces.IWebListener;
import com.softech.ccpatient.sharedpreferences.FetchData;
import com.softech.ccpatient.utils.Const;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Robin.Yaqoob on 06-Dec-17.
 */

public class
FragmentSearch extends Fragment implements IWebListener {

    @BindView(R.id.editTextContactnumber)
    EditText editTextContactnumber;
    @BindView(R.id.buttonSearch)
    ImageView buttonSearch;
    @BindView(R.id.textViewClinic)
    TextView textViewClinic;
    @BindView(R.id.textViewDate)
    TextView textViewDate;
    @BindView(R.id.textViewTime)
    TextView textViewTime;
    @BindView(R.id.linearLayout1)
    LinearLayout linearLayout1;
    @BindView(R.id.imageViewPatient)
    ImageView imageViewPatient;
    @BindView(R.id.spinnerClinicList)
    Spinner spinnerClinicList;
    String clinicID;

    @BindView(R.id.recyclViewSearch)
    RecyclerView recyclViewSearch;

    ArrayList<SearchAppointmentModel> appointmentsModelArrayList = new ArrayList<SearchAppointmentModel>();
    private SearchAdapter mAdapter;

    public FragmentSearch() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_search, container, false);
        ButterKnife.bind(this, view);
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<CliniclistModel>>() {
        }.getType();
        String ClinicList = FetchData.getData(getActivity(), "cliniclist");
        final ArrayList<CliniclistModel> connectedClinicsModelArrayList = gson.fromJson(ClinicList.toString(), type);

        ArrayAdapter<CliniclistModel> adapter =
                new ArrayAdapter<CliniclistModel>(getActivity(), R.layout.spinner, connectedClinicsModelArrayList);
        adapter.setDropDownViewResource(R.layout.spinner);
        spinnerClinicList.setAdapter(adapter);
        spinnerClinicList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                CliniclistModel cliniclistModel = connectedClinicsModelArrayList.get(position);
                clinicID = cliniclistModel.getOID();
                searchAppointmentsFromClinic();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //editTextContactnumber.setText("923224341025");
        buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editTextContactnumber.getText().toString().length()>0)
                {
                    getSearchData(editTextContactnumber.getText().toString());
                }else{
                    Toast.makeText(getActivity(), "Please add contact number.", Toast.LENGTH_SHORT).show();

                }
            }
        });

//        linearLayout1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent intent = new Intent(getActivity(), PreviousAppointmentsActivity.class);
//                startActivity(intent);
//            }
//        });

        editTextContactnumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(i ==11)
                {
                    String input = editTextContactnumber.getText().toString();
                    final String number = input.replaceFirst("(\\d{2})(\\d{3})(\\d+)", "(+$1)$2-$3");//(+92)XXX-XXXXXXX
                    System.out.println(number);
                    editTextContactnumber.setText(number);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        return  view;

    }
   private void searchAppointmentsFromClinic()
   {
       String consultantID = FetchData.getData(getActivity(), "userId");
       String access_token = FetchData.getData(getActivity(), "access_token");

       Map<String, String> params = new HashMap<String, String>();
       params.put("action", "Patients List from Clinic");
       params.put("consultantId", consultantID);
       params.put("access_token", access_token);
       params.put("clinicId", clinicID);

       ApiConnection apiConnection = new ApiConnection(FragmentSearch.this,
               getActivity(),params, Const.SEARCHPatientByClinic);
       apiConnection.makeStringReq();
   }
    private void getSearchData(String contactNo)
    {
        String consultantID = FetchData.getData(getActivity(), "userId");
        String access_token = FetchData.getData(getActivity(), "access_token");

        Map<String, String> params = new HashMap<String, String>();
        params.put("action", "Search Patient Result");
        params.put("consultantId", consultantID);
        params.put("access_token", access_token);
        params.put("contactNo", contactNo);

        ApiConnection apiConnection = new ApiConnection(FragmentSearch.this,
                getActivity(),params, Const.SEARCH);
        apiConnection.makeStringReq();

    }

    @Override
    public void success(String response) {
        /*if(response !=null) {

            try {

                JSONObject jsonObject = new JSONObject(response);
                JSONObject meta = jsonObject.getJSONObject("meta");
                String code = meta.getString("code");
                if(Integer.valueOf(code)== 200)
                {

                    String message = meta.getString("message");

                    JSONArray jsonArray = jsonObject.getJSONArray("response");
                    Gson gson = new Gson();
                    Type type = new TypeToken<ArrayList<SearchAppointmentModel>>(){}.getType();
                    appointmentsModelArrayList =gson.fromJson(jsonArray.toString(), type);

//                    linearLayout1.setVisibility(View.VISIBLE);
                    mAdapter = new SearchAdapter(appointmentsModelArrayList, getActivity());
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                    recyclViewSearch.setLayoutManager(mLayoutManager);
                    recyclViewSearch.setItemAnimator(new DefaultItemAnimator());
                    recyclViewSearch.setAdapter(mAdapter);
                    // mAdapter.notifyDataSetChanged();
                    recyclViewSearch.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
//                    SearchAppointmentModel clinicsModels = appointmentsModelArrayList.get(0);
//                    textViewClinic.setText(clinicsModels.getPatient());
//                    textViewDate.setText(clinicsModels.getAppDate());
//                    textViewTime.setText(clinicsModels.getvAppTimeFrom());
//
//                    Picasso.with(getActivity())
//                            .load("https://www.healthcarepakistan.pk/Patient_training/Content/patientImages/" +clinicsModels.getPatId()+ ".png")
//                            .placeholder(R.mipmap.pic)
//                            .into(imageViewPatient);

                    SaveData.SaveData(getActivity(), "searchData", jsonArray.toString());




                }else if(Integer.valueOf(code)== 504)
                {
                    appointmentsModelArrayList.clear();
                    String message = meta.getString("message");

                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                  //  recyclViewSearch.setVisibility(View.GONE);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            Logs.showLog("Login", response);
        }*/
    }

    @Override
    public void error(String response) {

        Log.d(response, response);
    }
}