package com.softech.ccpatient.sharedpreferences;

import android.app.Activity;
import android.content.SharedPreferences;

/**
 * Created by Robin on 23-Nov-17.
 */

public class SaveData {

    public static void SaveData(Activity activity, String key, String value) {
        SharedPreferences sp = activity.getApplicationContext().getSharedPreferences("appData", 0);
        SharedPreferences.Editor editor;
        editor = sp.edit();
        editor.putString(key, value);
        editor.apply();
    }

}
