package com.softech.ccpatient.sharedpreferences;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Robin on 23-Nov-17.
 */

public class FetchData {

    public static String getData(Activity activity, String key) {
        String value = "";
        SharedPreferences prfs = activity.getSharedPreferences("appData", Context.MODE_PRIVATE);
        value = prfs.getString(key, "");
        return value;

    }

    public static boolean getBoolean(Activity activity, String key) {
        boolean value = false;
        SharedPreferences prfs = activity.getSharedPreferences("appData", Context.MODE_PRIVATE);
        value = prfs.getBoolean(key, false);
        return value;

    }


    public static String getData(Context activity, String key) {
        String value = "";
        SharedPreferences prfs = activity.getSharedPreferences("appData", Context.MODE_PRIVATE);
        value = prfs.getString(key, "");
        return value;

    }

    public static Date dateFromString(String string) {
        Date mDate = null;
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        try {
            mDate = format.parse(string);
            System.out.println(mDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return mDate;
    }

    public static String getPreviousOrNextDate(String dateString, int i) {
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Calendar calendar = Calendar.getInstance();
        Date date = null;
        try {
            date = df.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        calendar.setTime(date);
        calendar.add(Calendar.DATE, i);
        String yesterdayAsString = df.format(calendar.getTime());

        return yesterdayAsString;
    }

    public static boolean CheckPermission(String Text, String permission, Context mContext, int reqCode) {
        if (ContextCompat.checkSelfPermission(mContext, permission) != PackageManager.PERMISSION_GRANTED) {
            reqPermission(permission, mContext, reqCode, Text);
            return false;
        } else {
            return true;
        }
    }

    public static void reqPermission(String permission, final Context mContext, int reqCode, final String Message) {
        if (!ActivityCompat.shouldShowRequestPermissionRationale((Activity) mContext, permission)) {
            ActivityCompat.requestPermissions((Activity) mContext, new String[]{permission}, reqCode);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            builder.setCancelable(false);
            builder.setTitle("Permission");
            builder.setMessage("You needs to Grant Access " + Message + " From Settings >> AppInfo >> Permissions For Using Application");
            builder.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    //
                    dialog.dismiss();
                    String packageName = "com.SocialFeedBack.android";
                    try {
                        //Open the specific App Info page:
                        Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        intent.setData(Uri.parse("package:" + packageName));
                        mContext.startActivity(intent);
                    } catch (ActivityNotFoundException e) {
                        //e.printStackTrace();
                        Toast.makeText(mContext, "You need to Grant Access " + Message + " From Settings >> AppInfo >> Permissions For Using Application", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            // Create the AlertDialog object and return it
            builder.create().show();
        }
    }

    public static boolean CheckPermissionFragment(Fragment fragment, String Text, String permission, Context mContext, int reqCode) {
        if (ContextCompat.checkSelfPermission(mContext, permission) != PackageManager.PERMISSION_GRANTED) {
            reqFragmentPermission(fragment, (Activity) mContext, Text, reqCode, permission);
            return false;
        } else {
            return true;
        }
    }


    public static void reqFragmentPermission(Fragment fragment, final Activity mContext, final String Text, int PermissionCode, String Permission) {
        if (!fragment.shouldShowRequestPermissionRationale(Permission)) {
            fragment.requestPermissions(new String[]{Permission}, PermissionCode);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            builder.setCancelable(false);
            builder.setTitle("Permission");
            builder.setMessage("You needs to Grant Access " + Text + " From Settings >> AppInfo >> Permissions For Using Application");
            builder.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    //
                    dialog.dismiss();
                    String packageName = "com.SocialFeedBack.android";
                    try {
                        //Open the specific App Info page:
                        Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        intent.setData(Uri.parse("package:" + packageName));
                        mContext.startActivity(intent);
                    } catch (ActivityNotFoundException e) {
                        //e.printStackTrace();
                        Toast.makeText(mContext, "You need to Grant Access " + Text + " From Settings >> AppInfo >> Permissions For Using Application", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            // Create the AlertDialog object and return it
            builder.create().show();
        }
    }

    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }
}
