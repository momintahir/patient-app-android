package com.softech.ccpatient.adapters;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.softech.ccpatient.R;
import com.softech.ccpatient.activities.SlotsActivity;
import com.softech.ccpatient.apiconnections.ApiConnection;
import com.softech.ccpatient.data.models.models.PatientAppointments;
import com.softech.ccpatient.fragments.FragmentAppointments;
import com.softech.ccpatient.interfaces.IWebListener;
import com.softech.ccpatient.sharedpreferences.FetchData;
import com.softech.ccpatient.utils.Const;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class AppointmentsAdapter extends RecyclerView.Adapter<AppointmentsAdapter.MyViewHolder> {

    private List<PatientAppointments> appointmentsModelList;
    private Activity activity;
    OnDeleteAptClickListenter listener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView ivTime, tvPatientName, tvDate, tvClinic, tvNotes,btnRescedule;
        ImageView ivPatient, btnDelete;
        int position;

        public MyViewHolder(View view,  OnDeleteAptClickListenter listenter) {
            super(view);

            ivPatient = view.findViewById(R.id.ivPatient);
            ivTime =  view.findViewById(R.id.ivTime);
            tvPatientName =  view.findViewById(R.id.tvPatientName);
            tvDate =  view.findViewById(R.id.tvDate);
            tvClinic =  view.findViewById(R.id.tvClinic);
            tvNotes =  view.findViewById(R.id.tvNotes);
            btnDelete = view.findViewById(R.id.btnDelete);
            btnRescedule=view.findViewById(R.id.btnRescedule);

//            btnDelete = view.findViewById(R.id.btnDelete);
            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.OnDeleteAptClick(v, appointmentsModelList.get(position));
                }
            });


            btnRescedule.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                addAppointmentService();
                    Intent intent = new Intent(activity, SlotsActivity.class);
                    intent.putExtra("isRescheduled",true);
                    intent.putExtra("ConsultantId",appointmentsModelList.get(position).getIConsultID());
                    intent.putExtra("OrganizationId",appointmentsModelList.get(position).getClinicID());
                    intent.putExtra("clinicName",appointmentsModelList.get(position).getClinic());
                    intent.putExtra("appointmentId",appointmentsModelList.get(position).getIAppID());
                    activity.startActivity(intent);
                }
            });

        }
    }

    public interface OnDeleteAptClickListenter {
        void OnDeleteAptClick(View caller, PatientAppointments mItem);
    }

    public AppointmentsAdapter(List<PatientAppointments> appointmentsModelList, Activity activity, OnDeleteAptClickListenter listener) {
        this.appointmentsModelList = appointmentsModelList;
        this.activity = activity;
        this.listener = listener;
    }
    public AppointmentsAdapter(List<PatientAppointments> appointmentsModelList, Activity activity) {
        this.appointmentsModelList = appointmentsModelList;
        this.activity = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.design_appointments, parent, false);

        return new MyViewHolder(itemView,listener);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.position = position;
        final PatientAppointments clinicsModels = appointmentsModelList.get(position);
        holder.ivTime.setText(clinicsModels.getVAppTimeFrom());
        holder.tvPatientName.setText(clinicsModels.getConsultant());
        holder.tvDate.setText(clinicsModels.getAppDate());
        holder.tvClinic.setText(clinicsModels.getClinic());
        holder.tvNotes.setText(clinicsModels.getVNotes());

        if (!Objects.requireNonNull(clinicsModels.getImageUrl()).isEmpty()) {
//            Picasso.get()
//                    .load(clinicsModels.getImageUrl())
//                    .placeholder(R.drawable.no_img)
//                    .into(holder.ivPatient);
//


                    Glide
                    .with(activity)
                    .load(clinicsModels.getImageUrl())
                    .centerCrop()
                    .placeholder(R.drawable.no_img)
                    .into(holder.ivPatient);
        }

    }

    @Override
    public int getItemCount() {
        return appointmentsModelList.size();
    }

//    private void addAppointmentService(PatientAppointments patientAppointments) {
//        val userId = FetchData.getData(, "userId")
//
//        Log.d("Call", "response consultantId: " + consultantId)
//        Log.d("Call", "response clincID: " + clincID)
//        Log.d("Call", "response date: " + date)
//        Log.d("Call", "response TimeStart: " + timeStart)
//        Log.d("Call", "response TimeEnd: " + timeEnd)
//        Log.d("Call", "response SlotID: " + slotID)
//        Log.d("Call", "response CalendarID: " + calendarID)
//        Log.d("Call", "response patId: " + userId);
//
//
//        HashMap params = HashMap<String, String>()
//        params["action"] = "Book Appointment"
//        params["consultantId"] = patientAppointments.getIConsultID();
//        params["clincID"] = patientAppointments.getClinicID();
//        params["date"] = patientAppointments.
//        params["TimeStart"] = timeStart
//        params["TimeEnd"] = timeEnd
//        params["SlotID"] = slotID
//        params["CalendarID"] = calendarID
//        params["patId"] = userId
//        params["Fee"] = fee
//        params["Currency"] = "PKR"
//        params["TransactionID"] =txnRefNo
//        params["notes"] =""
//        params["appId"] ="0"
//        params["rescheduleCheck"] ="false"
//
//        val apiConnection = ApiConnection(object : IWebListener {
//            override fun success(response: String?) {
//
//                Log.d("Call", "response: " + response)
//                if (response != null) {
//                    try {
//                        val jsonObject = JSONObject(response)
//                        val meta = jsonObject.getJSONObject("meta")
//                        val code = meta.getString("code")
//                        if (Integer.valueOf(code) == 200) {
//                            val response = jsonObject.getString("response")
//                            Toast.makeText(this@JazzCashActivity, response, Toast.LENGTH_SHORT).show()
//                            val intent = Intent(this@JazzCashActivity, TabsActivity::class.java)
//                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
//                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//                            startActivity(intent)
//                            finish()
//                        } else if (Integer.valueOf(code) == 504) {
//                            val error = jsonObject.getString("error")
//                            Toast.makeText(applicationContext, error, Toast.LENGTH_SHORT).show()
//                        }
//
//                    } catch (e: JSONException) {
//                        e.printStackTrace()
//                    }
//                }
//            }
//
//            override fun error(response: String) {
//                Log.v("Error Add Appointment", "")
//
//            }
//        }, this@JazzCashActivity, params,
//                Const.BookAppointment)
//        apiConnection.makeStringReq()
//
//
//
//
//
//
//
//
//
//
//
//
//        String userId = FetchData.getData(activity, "userId");
//
//        Map<String, String> params = new HashMap<String, String>();
//        params.put("action", "List Of Patient Appointments");
//        params.put("patId", userId);
////        params.put("patId", "17");
//
//        ApiConnection apiConnection = new ApiConnection(new IWebListener() {
//            @Override
//            public void success(java.lang.String response) {
//
//            }
//
//            @Override
//            public void error(java.lang.String response) {
//
//            }
//        },
//                activity, params, Const.VIEWAPPOINTMENTS);
//        apiConnection.makeStringReq();
//
//
//
//
//    }


}