package com.softech.ccpatient.adapters;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.softech.ccpatient.R;
import com.softech.ccpatient.activities.TabsActivity;
import com.softech.ccpatient.data.models.models.LoadFile;

import java.util.List;


public class FilesAdapter extends RecyclerView.Adapter<FilesAdapter.ViewHolder> {

    private final List<LoadFile> mValues;
    private Activity mContext;

    public FilesAdapter(Activity context, List<LoadFile> loadFileModelList) {
        mValues = loadFileModelList;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_file, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        String fileType = mValues.get(position).getFileType();

        holder.tvDocument.setText(mValues.get(position).getFileName());

        if (fileType.equals("application/msword") || fileType.equals("application/vnd.openxmlformats-officedocument.wordprocessingml.document")) {
            holder.ivDocument.setImageResource(R.drawable.doc);
        } else if (fileType.equals("image/jpeg") || fileType.equals("image/jpg")) {
            holder.ivDocument.setImageResource(R.drawable.jpg);
        } else if (fileType.equals("application/pdf")) {
            holder.ivDocument.setImageResource(R.drawable.pdf);
        } else if (fileType.equals("image/png")) {
            holder.ivDocument.setImageResource(R.drawable.png);
        } else if (fileType.equals("text/plain")) {
            holder.ivDocument.setImageResource(R.drawable.txt);
        } else if (fileType.equals("audio/mp3")) {
            holder.ivDocument.setImageResource(R.drawable.mp3);
        } else if (fileType.equals("video/mp4")) {
            holder.ivDocument.setImageResource(R.drawable.mp4);
        } else {
            holder.ivDocument.setImageResource(R.drawable.empty);
        }


        holder.llItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*SaveData.SaveData(mContext, "webLink", mValues.get(position).getFilePath());
                Intent intent = new Intent(Intent.ACTION_VIEW);
                if (mValues.get(position).getFileType().contains(".xls") || mValues.get(position).getFileType().contains(".xlsx")) {
                    intent.setDataAndType(Uri.parse(mValues.get(position).getFilePath()), "application/vnd.ms-excel");
                } else
                    intent.setDataAndType(Uri.parse(mValues.get(position).getFilePath()), mValues.get(position).getFileType());
                mContext.startActivity(intent);*/

                if (mContext instanceof TabsActivity) {
                    ((TabsActivity)mContext).webviewFrag(mValues.get(position).getFilePath(),mValues.get(position).getFileType(),
                            mValues.get(position).getFile());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout llItem;
        TextView tvDocument;
        ImageView ivDocument;


        public ViewHolder(View view) {
            super(view);
            llItem = view.findViewById(R.id.llItem);
            tvDocument = view.findViewById(R.id.tvDocument);
            ivDocument = view.findViewById(R.id.ivDocument);
        }
    }
}