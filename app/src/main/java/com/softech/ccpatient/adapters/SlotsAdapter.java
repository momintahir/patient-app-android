package com.softech.ccpatient.adapters;

import android.app.Activity;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.softech.ccpatient.activities.BookAppointmentActivity;
import com.softech.ccpatient.R;
import com.softech.ccpatient.data.models.models.SlotsData.Response;
import java.util.List;


public class SlotsAdapter extends RecyclerView.Adapter<SlotsAdapter.ViewHolder> {
    private Activity activity;
    private List<Response> mitem;
    private String consultantId, clincID, date,fee,clinicName;
    private boolean isRescheduled=false;
    private String appointmentId="";
    private String consultantName="";

    public SlotsAdapter(Activity activity,String consultantName, String appointmentId,boolean isRescheduled,String clinicName,List<Response> items, String consultantId, String clincID, String date,String fee) {
        this.activity = activity;
        this.mitem = items;
        this.consultantId = consultantId;
        this.clincID = clincID;
        this.date = date;
        this.fee=fee;
        this.clinicName=clinicName;
        this.isRescheduled=isRescheduled;
        this.appointmentId=appointmentId;
        this.consultantName=consultantName;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_items_slots, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.slotText.setText(mitem.get(position).getSlotTime());
//
//        if(mitem.get(position).getAppID() == -1){
//            if (mitem.get(position).getIsDisabled() == 0) {
//                holder.slotText.setEnabled(true);
//                holder.slotText.setBackgroundResource(R.color.selectGreen);
//            }else{
//                holder.slotText.setEnabled(false);
//                holder.slotText.setBackgroundResource(R.color.lightgrey);
//            }
//        }
//        else{
//            holder.slotText.setEnabled(false);
//            holder.slotText.setBackgroundResource(R.color.lightgrey);
//        }

        String isAllowed=mitem.get(position).isAllowed();
        Integer isDisabled=mitem.get(position).getIsDisabled();
        String isDel=mitem.get(position).isDel();
        Integer appId=mitem.get(position).getAppID();


        assert isAllowed != null;
        assert isDel != null;
        if (isAllowed.equals("1") && isDisabled==0 && isDel.equals("0") && appId==0){
            holder.slotText.setEnabled(true);
            holder.slotText.setBackgroundResource(R.color.selectGreen);
        }
        else {
            holder.slotText.setEnabled(false);
            holder.slotText.setBackgroundResource(R.color.lightgrey);
        }


            holder.slotText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAppointmentActivity(mitem.get(position));
            }
        });

    }

    @Override
    public int getItemCount() {
        return mitem.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public TextView slotText;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            slotText =  view.findViewById(R.id.slotText);
        }
    }

    private void openAppointmentActivity(Response slotsData) {
        Intent intent = new Intent(activity, BookAppointmentActivity.class);
        if (isRescheduled){
            intent.putExtra("consultantId", consultantId);
            intent.putExtra("clincID", clincID);
            intent.putExtra("date", date);
            intent.putExtra("isRescheduled",isRescheduled);
            intent.putExtra("clinicName",clinicName);
            intent.putExtra("TimeStart", slotsData.getSlotTime());
            intent.putExtra("TimeEnd", slotsData.getSlotEndTime());
            intent.putExtra("SlotID", slotsData.getISlotID());
            intent.putExtra("CalendarID", slotsData.getICalendarID());
            intent.putExtra("appointmentId",appointmentId);
        }
        else {
            intent.putExtra("consultantId", consultantId);
            intent.putExtra("clincID", clincID);
            intent.putExtra("date", date);
            intent.putExtra("fee",fee);
            intent.putExtra("clinicName",clinicName);
            intent.putExtra("isRescheduled",isRescheduled);
            intent.putExtra("consultantName",consultantName);
            intent.putExtra("TimeStart", slotsData.getSlotTime());
            intent.putExtra("TimeEnd", slotsData.getSlotEndTime());
            intent.putExtra("SlotID", slotsData.getISlotID());
            intent.putExtra("CalendarID", slotsData.getICalendarID());
        }

        activity.startActivity(intent);
    }
}
