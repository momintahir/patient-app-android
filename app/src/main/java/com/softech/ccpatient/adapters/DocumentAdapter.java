package com.softech.ccpatient.adapters;

import android.app.Activity;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.softech.ccpatient.R;
import com.softech.ccpatient.activities.TabsActivity;
import com.softech.ccpatient.data.models.models.LoadDocuments;

import java.util.List;


public class DocumentAdapter extends RecyclerView.Adapter<DocumentAdapter.ViewHolder> {

    Activity mContext;
    FragmentActivity c;
    private List<LoadDocuments> loadDocumnetModelList;

    public DocumentAdapter(Activity context, List<LoadDocuments> loadDocumnetModelList) {
        this.mContext = context;
        this.loadDocumnetModelList = loadDocumnetModelList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_document, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.titlelabel.setText(loadDocumnetModelList.get(position).getFolderName());
        holder.linearlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               /* FragmentTransaction ft = context.getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.frag, new FileFragment());
                ft.commit();*/

//               TabsActivity tabsActivity = new TabsActivity();
//               tabsActivity.fileFrag();

                if (mContext instanceof TabsActivity) {
                    ((TabsActivity)mContext).fileFrag(loadDocumnetModelList.get(position).getOId());
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return loadDocumnetModelList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView titlelabel;
        public final LinearLayout linearlayout;
        ImageView documentImageView, correctImg, folderImg;

        public ViewHolder(View view) {
            super(view);
            linearlayout =  view.findViewById(R.id.linearlayout);
            titlelabel =  view.findViewById(R.id.documentNameLabel);
            documentImageView = view.findViewById(R.id.docPicture);
            correctImg = view.findViewById(R.id.correctImg);
            folderImg = view.findViewById(R.id.folderImg);
        }
    }
}
