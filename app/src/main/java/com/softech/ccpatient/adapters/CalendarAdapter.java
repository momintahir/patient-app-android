package com.softech.ccpatient.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.softech.ccpatient.activities.BookAppointmentActivity;
import com.softech.ccpatient.R;
import com.softech.ccpatient.data.models.models.CalendarByDate.SlotsDatum;
import com.softech.ccpatient.interfaces.SendCalendarData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by saimshafqat on 09/08/2018.
 */

public class CalendarAdapter extends RecyclerView.Adapter<CalendarAdapter.ViewHolder> {

    private final List mValues;
    private List<Object> sublist = null;
    private List<SlotsDatum> slotsData = new ArrayList<>();
    Activity mContext;
    LayoutInflater mLayoutInflater;
    SendCalendarData sendCalendarData;
    private String selectedDate,hospitalId;

    public CalendarAdapter(Activity context, List items, String selectedDate, String hospitalId) {
        mValues = items;
        if (sublist != null) {
            sublist.clear();
        }
        this.mContext = context;
        this.selectedDate = selectedDate;
        this.hospitalId = hospitalId;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_selectedslots, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        sublist = (List) mValues.get(position);
        slotsData.clear();
        if (sublist.size() == 3) {
            slotsData.add((SlotsDatum) sublist.get(0));
            slotsData.add((SlotsDatum) sublist.get(1));
            slotsData.add((SlotsDatum) sublist.get(2));
            holder.btn1.setText(slotsData.get(0).getSlotTime());
            holder.btn2.setText(slotsData.get(1).getSlotTime());
            holder.btn3.setText(slotsData.get(2).getSlotTime());
            if (Integer.valueOf(slotsData.get(0).getAppID()) == -1) {
                holder.btn1.setEnabled(true);
                holder.btn1.setBackgroundResource(R.color.selectGreen);
            } else {
                holder.btn1.setEnabled(false);
                holder.btn1.setBackgroundResource(R.color.lightgrey);
            }
            if (Integer.valueOf(slotsData.get(1).getAppID()) == -1) {
                holder.btn2.setEnabled(true);
                holder.btn2.setBackgroundResource(R.color.selectGreen);
            } else {
                holder.btn2.setEnabled(false);
                holder.btn2.setBackgroundResource(R.color.lightgrey);
            }
            if (Integer.valueOf(slotsData.get(2).getAppID()) == -1) {
                holder.btn3.setEnabled(true);
                holder.btn3.setBackgroundResource(R.color.selectGreen);
            } else {
                holder.btn3.setEnabled(false);
                holder.btn3.setBackgroundResource(R.color.lightgrey);
            }
        } else if (sublist.size() == 2) {
            slotsData.add((SlotsDatum) sublist.get(0));
            slotsData.add((SlotsDatum) sublist.get(1));
            holder.btn1.setText(slotsData.get(0).getSlotTime());
            holder.btn2.setText(slotsData.get(1).getSlotTime());
        } else if (sublist.size() == 1) {
            slotsData.add((SlotsDatum) sublist.get(0));
            holder.btn1.setText(slotsData.get(0).getSlotTime());
        }
        holder.btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sublist = (List) mValues.get(position);
                SlotsDatum slotsDatum = (SlotsDatum) sublist.get(0);
                openAppointmentActivity(slotsDatum);
//                sendCalendarData.sendCalandarData(slotsDatum);
            }
        });
        holder.btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sublist = (List) mValues.get(position);
                SlotsDatum slotsDatum = (SlotsDatum) sublist.get(1);
                openAppointmentActivity(slotsDatum);
//                sendCalendarData.sendCalandarData(slotsDatum);
            }
        });
        holder.btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sublist = (List) mValues.get(position);
                SlotsDatum slotsDatum = (SlotsDatum) sublist.get(2);

                openAppointmentActivity(slotsDatum);
                //sendCalendarData.sendCalandarData(slotsDatum);
            }
        });
    }


    @Override
    public int getItemCount() {

        return mValues.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public TextView btn1, btn2, btn3;


        ImageView documentImageView;
        String URL;
        String mitem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            btn1 = (TextView) view.findViewById(R.id.btn1);
            btn2 = (TextView) view.findViewById(R.id.btn2);
            btn3 = (TextView) view.findViewById(R.id.btn3);
        }
    }

    private void openAppointmentActivity(SlotsDatum slotsDatum) {
        Intent intent = new Intent(mContext, BookAppointmentActivity.class);
        intent.putExtra("selectedDate", selectedDate);
        intent.putExtra("hospitalId", hospitalId);

        intent.putExtra("slotStartTime", slotsDatum.getSlotTime());
        intent.putExtra("slotEndTime", slotsDatum.getSlotEndTime());
        intent.putExtra("slotID", slotsDatum.getISlotID());
        intent.putExtra("calendarID", slotsDatum.getICalendarID());
        mContext.startActivity(intent);
    }

}
