package com.softech.ccpatient.adapters;

import android.app.Activity;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.softech.ccpatient.R;
import com.softech.ccpatient.data.models.models.LoadDocuments;
import com.softech.ccpatient.utils.OnItemClick;

import java.util.ArrayList;
import java.util.List;



public class UploadDocAdapter extends RecyclerView.Adapter<UploadDocAdapter.ViewHolder> {

    Activity mContext;
    FragmentActivity c;
    private List<LoadDocuments> loadDocumnetModelList;
    private List<ImageView> ivList = new ArrayList<>();
    private OnItemClick mCallback;

    public UploadDocAdapter(Activity context, List<LoadDocuments> loadDocumnetModelList, OnItemClick listener) {
        this.mContext = context;
        this.loadDocumnetModelList = loadDocumnetModelList;
        this.mCallback = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_upload_doc, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        ivList.add(holder.ivTick);

        holder.tvName.setText(loadDocumnetModelList.get(position).getFolderName());
        holder.llItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int i = 0; i < ivList.size(); i++) {
                    ivList.get(i).setBackgroundResource(0);
                }
                holder.ivTick.setBackgroundResource(R.drawable.ic_tick);

                mCallback.onClick(loadDocumnetModelList.get(position).getOId());

            }
        });


    }

    @Override
    public int getItemCount() {
        return loadDocumnetModelList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout llItem;
        TextView tvName;
        ImageView ivTick;

        public ViewHolder(View view) {
            super(view);
            llItem = view.findViewById(R.id.llItem);
            tvName = view.findViewById(R.id.tvName);
            ivTick = view.findViewById(R.id.ivTick);
        }
    }
}
