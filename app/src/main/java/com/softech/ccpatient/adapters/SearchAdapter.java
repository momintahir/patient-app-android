package com.softech.ccpatient.adapters;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.softech.ccpatient.R;
import com.softech.ccpatient.activities.SlotsActivity;
import com.softech.ccpatient.data.models.models.SearchAppointmentNew;
import com.squareup.picasso.Picasso;
import java.util.List;
import java.util.Objects;


public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.MyViewHolder> {

    private List<SearchAppointmentNew> appointmentsModelList;
    private Activity activity;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvPatientName, tvClinicName, tvAddress,tvFee;
        private ImageView ivPatient;
        private LinearLayout llItem;

        private MyViewHolder(View view) {
            super(view);
            llItem = (LinearLayout) view.findViewById(R.id.llItem);
            tvPatientName = (TextView) view.findViewById(R.id.tvPatientName);
            tvClinicName = (TextView) view.findViewById(R.id.tvClinicName);
            tvAddress = (TextView) view.findViewById(R.id.tvAddress);
            ivPatient = (ImageView) view.findViewById(R.id.ivPatient);
            tvFee=view.findViewById(R.id.tvFee);
        }
    }


    public SearchAdapter(List<SearchAppointmentNew> appointmentsModelList, Activity activity) {
        this.appointmentsModelList = appointmentsModelList;
        this.activity = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.design_appointments_search, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        if (position % 2 == 0) {
            holder.llItem.setBackgroundColor(Color.parseColor("#FFFFFF"));
        } else {
            holder.llItem.setBackgroundColor(Color.parseColor("#f6f2f1"));
        }

        final SearchAppointmentNew clinicsModels = appointmentsModelList.get(position);
        holder.tvPatientName.setText(clinicsModels.getConsultantName());
        holder.tvClinicName.setText(clinicsModels.getClinicName());
        holder.tvAddress.setText(clinicsModels.getClinicAddress());
        holder.tvFee.setText("Fee: "+clinicsModels.getOpdFee());


        if (!Objects.requireNonNull(clinicsModels.getImageUrl()).isEmpty()) {
//            Picasso.get()
//                    .load(clinicsModels.getImageUrl())
//                    .placeholder(R.drawable.no_img)
//                    .into(holder.ivPatient);


                     Glide
                    .with(activity)
                    .load(clinicsModels.getImageUrl())
                    .centerCrop()
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .placeholder(R.drawable.no_img)
                    .into(holder.ivPatient);
        }

        holder.llItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, SlotsActivity.class);
                intent.putExtra("ConsultantId",clinicsModels.getConsultantCode());
                intent.putExtra("consultantName",clinicsModels.getConsultantName());
                intent.putExtra("OrganizationId",clinicsModels.getOrganizationId());
                intent.putExtra("clinicName",clinicsModels.getClinicName());
                intent.putExtra("fee",clinicsModels.getOpdFee());
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return appointmentsModelList.size();
    }
}