package com.softech.ccpatient.webrtc.callbacks

import org.json.JSONObject

interface WebSocketCallback {
    fun webSocketCallback(jsonObject: JSONObject)
}

interface RejectCallback {
    fun onReject(jsonObject: JSONObject)
}

interface WebSocketNewCallListener {
    fun newCallListener(jsonObject: JSONObject)
}


interface SettingsListener {
    fun onSettingsListener(jsonObject: JSONObject, type: String)
}

interface ConvertedVideoListener {
    fun onVideoConverted(url: String)
}