package com.softech.ccpatient.webrtc.services

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.softech.ccpatient.webrtc.SocketIO
import com.softech.ccpatient.webrtc.util.AppConstants
import com.softech.ccpatient.webrtc.util.Debugger
import com.softech.ccpatient.webrtc.util.Global

class RejectCallReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        val callerID = intent?.getIntExtra(AppConstants.CALLER_USER_ID, 0)
        val callId = intent?.getStringExtra("callId")
        val isAlreadyConnected = intent?.getBooleanExtra("is_already_connected", false)
        Debugger.e("RejectCallReceiver", "callId:$callerID")
        SocketIO.getInstance().onHangout(callerID.toString(), callId.toString())
        val it = Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS)
        context?.sendBroadcast(it)
        context?.stopService(Intent(context, CallService::class.java))
        if (isAlreadyConnected == false) {
            SocketIO.getInstance().disconnectSocket()
        }
        Global.ringtune?.stop()
    }
}
