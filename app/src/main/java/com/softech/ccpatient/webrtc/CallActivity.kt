package com.softech.ccpatient.webrtc

import android.Manifest
import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.media.AudioManager
import android.media.Ringtone
import android.media.RingtoneManager
import android.media.ToneGenerator
import android.net.Uri
import android.os.*
import android.util.DisplayMetrics
import android.util.Rational
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.softech.ccpatient.R
import com.softech.ccpatient.databinding.CallActivityBinding
import com.softech.ccpatient.webrtc.callbacks.WebSocketCallback
import com.softech.ccpatient.webrtc.callbacks.WebSocketNewCallListener
import com.softech.ccpatient.webrtc.services.CallService
import com.softech.ccpatient.webrtc.util.AppConstants
import com.softech.ccpatient.webrtc.util.Debugger
import com.softech.ccpatient.webrtc.util.GlideDownloader
import com.softech.ccpatient.webrtc.util.Global
import kotlinx.android.synthetic.main.secondcall_notify.*
import org.json.JSONException
import org.json.JSONObject
import org.webrtc.*
import java.util.*
import kotlin.math.roundToInt
import kotlin.system.exitProcess

class CallActivity : AppCompatActivity(), View.OnClickListener, WebSocketCallback,
        WebSocketNewCallListener {
    private var peerConnectionFactory: PeerConnectionFactory? = null
    private var audioConstraints: MediaConstraints? = null
    private lateinit var sdpConstraints: MediaConstraints
    private var videoSource: VideoSource? = null
    private var localVideoTrack: VideoTrack? = null
    private var audioSource: AudioSource? = null
    private var localAudioTrack: AudioTrack? = null
    internal var localPeer: PeerConnection? = null
    private lateinit var rootEglBase: EglBase
    private var peerIceServers: MutableList<PeerConnection.IceServer> = ArrayList()
    private val TAG = this.javaClass.simpleName
    var callerID: Int = -1
    var callId: String? = null

    private lateinit var audioManager: AudioManager
    private var videoCapture: CameraVideoCapturer? = null
    private var calleeName: String? = null
    private var profileImage: String? = null

    private var uri: Uri? = null
    private var ringtune: Ringtone? = null
    private var vibrator: Vibrator? = null
    private var dialTuneHandler: Handler? = null
    private var dialtuneRunnable: Runnable? = null
    private var isVideo = false
    private var isFromPush = false
    lateinit var binding: CallActivityBinding
    private var isSpeakerOn = false
    private var isMicOn = false
    private var isCallAccepted = false
    private var isFullScreenIntent = false
    private var toneGenerator: ToneGenerator? = null
    private var dialog: Dialog? = null
    private var jsonObject: JSONObject? = null

    //    private var secondCallRunning = false
    private var handler: Handler? = null
    private var callTimeRunnable: Runnable? = null

    //Reconnecting call variables
    private var isCallConnected = false
    private var savedBaseTime: Long = 0
    private var isFromReconnecting = false


    override fun onCreate(savedInstanceState: Bundle?) {
        /* window.addFlags(
             WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON or
                     WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD or
                     WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED or
                     WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
         )*/
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.call_activity)
        turnScreenOnAndKeyguardOff()
        initPermissions()
    }

    private fun Activity.turnScreenOnAndKeyguardOff() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
            setShowWhenLocked(true)
            setTurnScreenOn(true)
        } else {
            window.addFlags(
                    WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                            or WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON
            )
        }

        with(getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                requestDismissKeyguard(this@turnScreenOnAndKeyguardOff, null)
            }
        }
    }


    @SuppressLint("CheckResult")
    fun initPermissions() {
        Dexter.withActivity(this).withPermissions(
                Manifest.permission.CAMERA,
                Manifest.permission.RECORD_AUDIO
        ).withListener(object : MultiplePermissionsListener {
            override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                if (report!!.areAllPermissionsGranted()) {
                    startWebrtc()
                } else {
                    Debugger.e("Capturing Image", "onPermissionDenied")
                }
            }

            override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<PermissionRequest>?,
                    token: PermissionToken?
            ) {
                token?.continuePermissionRequest()
            }

        }).check()
    }

    private fun initialization() {
        isVideo = intent.getBooleanExtra(AppConstants.IS_VIDEO_CALL, false)
        isFromPush = intent.getBooleanExtra(AppConstants.IS_FROM_PUSH, false)
        rootEglBase = EglBase.create()
        initViews(isVideo)
        binding.ibHangUp.setOnClickListener(this)
        binding.ibAnswer.setOnClickListener(this)
        binding.ibMute.setOnClickListener(this)
        binding.ibSpeaker.setOnClickListener(this)
        binding.ibCameraSwitch.setOnClickListener(this)
        SocketIO.getInstance().setSocketCallback(this)
        SocketIO.getInstance().setOfferListener(this, false)
        audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            binding.ibMinimize.visibility = View.VISIBLE
            binding.ibMinimize.setOnClickListener(this)
        } else {
            binding.ibMinimize.visibility = View.GONE
        }
    }

    private fun initViews(isVideo: Boolean) {
        if (isVideo) {
            binding.rlCameras.visibility = View.VISIBLE
            binding.tvCall.text = "Video Call"
            binding.ivUserAudio.visibility = View.GONE
            binding.ivUserVideo.visibility = View.VISIBLE
            binding.rlCameras.setOnClickListener(this)
            binding.ibSpeaker.setBackgroundResource(R.drawable.speaker_on)
            isSpeakerOn = true
            binding.localVideoView.init(rootEglBase.eglBaseContext, null)
            binding.remoteVideoView.init(rootEglBase.eglBaseContext, null)
            binding.localVideoView.setZOrderMediaOverlay(true)
            binding.remoteVideoView.setZOrderMediaOverlay(true)
        } else {
            binding.rlCameras.visibility = View.GONE
            binding.tvCall.text = "Audio Call"
            binding.ibSpeaker.setBackgroundResource(R.drawable.speaker_off)
            isSpeakerOn = false
            binding.ivUserAudio.visibility = View.VISIBLE
            binding.ivUserVideo.visibility = View.GONE
        }
    }


    private fun initVideoCapturer(isVideo: Boolean) {
        if (isVideo) {
            val videoCapturerAndroid: VideoCapturer? = if (Camera2Enumerator.isSupported(this)) {
                createCameraCapturer(Camera2Enumerator(this))
            } else {
                createCameraCapturer(Camera1Enumerator(false))
            }
            if (videoCapturerAndroid != null) {
                val surfaceTextureHelper =
                        SurfaceTextureHelper.create("CaptureThread", rootEglBase.eglBaseContext)
                videoSource =
                        peerConnectionFactory?.createVideoSource(videoCapturerAndroid.isScreencast)
                videoCapturerAndroid.initialize(
                        surfaceTextureHelper,
                        applicationContext,
                        videoSource?.capturerObserver
                )
            }
            localVideoTrack = peerConnectionFactory?.createVideoTrack("100", videoSource)
            videoCapturerAndroid?.startCapture(640, 480, 30)
            binding.localVideoView.visibility = View.VISIBLE
            localVideoTrack?.addSink(binding.localVideoView)
            binding.localVideoView.setMirror(true)
            binding.remoteVideoView.setMirror(true)
        }
    }

    fun startWebrtc() {
        initialization()
        val stunIceServer = PeerConnection.IceServer
                .builder("stun:stun.worldnoordev.com:3478")
                .createIceServer()
        peerIceServers.add(stunIceServer)
        val turnIceServer =
                PeerConnection.IceServer.builder("turn:turn.worldnoordev.com:3478?transport=udp")
                        .setUsername("softech")
                        .setPassword("Kalaam2020")
                        .createIceServer()
        peerIceServers.add(turnIceServer)

        val turnIceServer2 =
                PeerConnection.IceServer.builder("turn:turn.worldnoordev.com:3478?transport=tcp")
                        .setUsername("softech")
                        .setPassword("Kalaam2020")
                        .createIceServer()
        peerIceServers.add(turnIceServer2)

        val turnIceServer3 =
                PeerConnection.IceServer.builder("turn:turn.worldnoordev.com:443?transport=tcp")
                        .setUsername("softech")
                        .setPassword("Kalaam2020")
                        .createIceServer()
        peerIceServers.add(turnIceServer3)

        val initializationOptions = PeerConnectionFactory.InitializationOptions.builder(this)
                .createInitializationOptions()
        PeerConnectionFactory.initialize(initializationOptions)

        val options = PeerConnectionFactory.Options()
        val defaultVideoEncoderFactory = DefaultVideoEncoderFactory(
                rootEglBase.eglBaseContext,
                true,
                true
        )
        val defaultVideoDecoderFactory = DefaultVideoDecoderFactory(rootEglBase.eglBaseContext)
        peerConnectionFactory = PeerConnectionFactory.builder()
                .setOptions(options)
                .setVideoEncoderFactory(defaultVideoEncoderFactory)
                .setVideoDecoderFactory(defaultVideoDecoderFactory)
                .createPeerConnectionFactory()

        audioConstraints = MediaConstraints()
        initVideoCapturer(isVideo)

        audioSource = peerConnectionFactory?.createAudioSource(audioConstraints)
        localAudioTrack = peerConnectionFactory?.createAudioTrack("101", audioSource)

        uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE)
        ringtune = RingtoneManager.getRingtone(this, uri)
        handler = Handler()
        callTimeRunnable = Runnable {
            logE("callTimeRunnable called")
            hangup()
        }
        handler?.postDelayed(callTimeRunnable, 90 * 1000)

        val initiator = intent.getBooleanExtra(AppConstants.INITIATOR, false)
        isCallAccepted = intent.getBooleanExtra(AppConstants.IS_CALL_ACCEPTED, false)
        isFullScreenIntent = intent.getBooleanExtra(AppConstants.IS_FULL_SCREEN_INTENT, false)
        if (initiator) {
            callerID = intent.getIntExtra(AppConstants.CALLER_USER_ID, 0)
            calleeName = intent.getStringExtra(AppConstants.CHAT_USER_NAME)
            profileImage = intent.getStringExtra(AppConstants.CHAT_USER_PICTURE)
            binding.ibAnswer.visibility = View.GONE
            binding.tvCallStatus.text = "Calling..."
            if (isVideo) {
                SocketIO.getInstance().onNewCall(callerID.toString(), "1")
            } else {
                SocketIO.getInstance().onNewCall(callerID.toString(), "0")
            }

        } else {
            if (isFullScreenIntent) {
                logE("Full screen intent is true")
                logE("call accepted :$isCallAccepted")
                callerID = intent.getIntExtra(AppConstants.CALLER_USER_ID, 0)
                calleeName = intent.getStringExtra(AppConstants.CHAT_USER_NAME)
                binding.ibAnswer.visibility = View.GONE
                callId = intent.getStringExtra("callId")
                if (isCallAccepted) {
                    binding.ibAnswer.visibility = View.GONE
                    logE("Ready for call here isFullScreenIntent")
                    SocketIO.getInstance().onReadyForCall(
                            callerID.toString(),
                            calleeName.toString(),
                            callId.toString()
                    )
                    stopAndroid10Service()
                } else {
                    binding.ibAnswer.visibility = View.VISIBLE
                }
            } else {
                val json = intent.getStringExtra(AppConstants.JSON)
                val data = JSONObject(json)
                logE("is initiator False :$data")
                ringtune?.play()
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    ringtune?.isLooping = true
                }
                newCallReceived(data)
                binding.tvCallStatus.text = "Incoming..."
            }
        }
        if (isVideo) {
            setCallerProfile(true)
        } else {
            setCallerProfile(false)
        }
    }

    /*override fun onResume() {
        super.onResume()
    }*/

    private fun newCallReceived(data: JSONObject) {
        if (localPeer?.iceConnectionState() == PeerConnection.IceConnectionState.CONNECTED) {
            localPeer?.close()
        }
        logE("newCallReceived")
        val offer = JSONObject(data.getString("offer"))
        callerID = data.getString(AppConstants.CONNECTED_USER_ID).toInt()
        callId = data.getString("callId")
        logE("callerID :$callerID")
        createPeerConnection()
        localPeer?.setRemoteDescription(
                CustomSdpObserver("localSetRemote"),
                SessionDescription(SessionDescription.Type.OFFER, offer.getString("sdp"))
        )
        profileImage = data.getString("photoUrl")
        calleeName = data.getString("name")
    }

    private fun setCallerProfile(isVideo: Boolean) {
        if (isVideo) {
            GlideDownloader.load(
                    this,
                    binding.ivUserVideo,
                    profileImage,
                    R.drawable.dummy_placeholder,
                    R.drawable.dummy_placeholder
            )
        } else {
            GlideDownloader.load(
                    this,
                    binding.ivUserAudio,
                    profileImage,
                    R.drawable.dummy_placeholder,
                    R.drawable.dummy_placeholder
            )
        }
        binding.tvName.text = calleeName.toString()
    }

    private fun startChronometer(base: Long) {
        binding.tvCallStatus.visibility = View.GONE
        binding.chronometer.visibility = View.VISIBLE
        binding.chronometer.base = base
        binding.chronometer.start()
    }

    private fun createPeerConnection() {
        logE("Creating peer connections")
        val rtcConfig = PeerConnection.RTCConfiguration(peerIceServers)
        rtcConfig.tcpCandidatePolicy = PeerConnection.TcpCandidatePolicy.DISABLED
        rtcConfig.bundlePolicy = PeerConnection.BundlePolicy.MAXBUNDLE
        rtcConfig.rtcpMuxPolicy = PeerConnection.RtcpMuxPolicy.REQUIRE
        rtcConfig.continualGatheringPolicy =
                PeerConnection.ContinualGatheringPolicy.GATHER_CONTINUALLY
        rtcConfig.keyType = PeerConnection.KeyType.ECDSA
        localPeer = peerConnectionFactory?.createPeerConnection(
                rtcConfig,
                object : CustomPeerConnectionObserver("localPeerCreation") {
                    override fun onIceCandidate(iceCandidate: IceCandidate) {
                        super.onIceCandidate(iceCandidate)
                        val json = JSONObject()
                        json.put("sdp", iceCandidate.sdp)
                        json.put("sdpMLineIndex", iceCandidate.sdpMLineIndex)
                        json.put("sdpMid", iceCandidate.sdpMid)
                        SocketIO.getInstance().sendIceCandidates(json, callerID.toString())
                    }

                    override fun onAddStream(mediaStream: MediaStream) {
                        super.onAddStream(mediaStream)
                        if (isVideo)
                            gotRemoteStream(mediaStream)
                    }

                    override fun onIceConnectionChange(iceConnectionState: PeerConnection.IceConnectionState) {
                        runOnUiThread {
                            when (iceConnectionState) {
                                PeerConnection.IceConnectionState.CONNECTED -> {
                                    logE("Peer Connection CONNECTED")
                                    if (isFromReconnecting) {
                                        startChronometer(savedBaseTime)
                                        stopDialTune()
                                    } else {
                                        isCallConnected = true
                                        startChronometer(SystemClock.elapsedRealtime())
//                                        binding.ibMute.visibility = View.VISIBLE
//                                        if (!isVideo) {
//                                            binding.ibSpeaker.visibility = View.VISIBLE
//                                        } else {
//                                            binding.ibCameraSwitch.visibility = View.VISIBLE
//                                        }
                                        if (isVideo) {
                                            binding.ibCameraSwitch.visibility = View.VISIBLE
                                        }
                                    }
                                    handler?.removeCallbacks(callTimeRunnable)
                                }
                                PeerConnection.IceConnectionState.DISCONNECTED -> {
                                    logE("Peer Connection DISCONNECTED")
                                    if (isCallConnected) {
                                        startReconnectingTune()
                                        binding.chronometer.visibility = View.GONE
                                        binding.tvCallStatus.visibility = View.VISIBLE
                                        isFromReconnecting = true
                                        binding.chronometer.stop()
                                        savedBaseTime = binding.chronometer.base
                                        binding.tvCallStatus.text = "Reconnecting..."
                                        handler?.postDelayed(callTimeRunnable, 30 * 1000)
                                    }

                                }
                                PeerConnection.IceConnectionState.FAILED -> {
                                    logE("Peer Connection FAILED")
//                                binding.tvCallStatus.text = "failed"

                                }
                                PeerConnection.IceConnectionState.CHECKING -> {
                                    logE("Peer Connection CHECKING")
                                }
                                PeerConnection.IceConnectionState.CLOSED -> {
                                    logE("Peer Connection CLOSED")
                                }
                                PeerConnection.IceConnectionState.NEW -> {
                                    logE("Peer Connection NEW")
                                }
                                PeerConnection.IceConnectionState.COMPLETED -> {
                                    logE("Peer Connection COMPLETED")
                                }

                                else -> {
                                }
                            }
                        }
                    }
                })
        addStreamToLocalPeer()
    }

    private fun gotRemoteStream(stream: MediaStream) {
        val videoTrack = stream.videoTracks[0]
        runOnUiThread {
            try {
                binding.remoteVideoView.visibility = View.VISIBLE
                videoTrack.addSink(binding.remoteVideoView)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }


    private fun turnOnSpeakers() {
        if (!audioManager.isSpeakerphoneOn) {
            logE("TURNING ON SPEAKERS")
            audioManager.mode = AudioManager.MODE_IN_COMMUNICATION
            audioManager.isSpeakerphoneOn = true
        }
    }

    private fun turnOFFSpeakers() {
        if (audioManager.isSpeakerphoneOn) {
            logE("TURNING OFF SPEAKERS")
            audioManager.mode = AudioManager.MODE_NORMAL
            audioManager.isSpeakerphoneOn = false
        }
    }

    private fun muteMicroPhone(mic: Boolean) {
        if (mic) {
            if (!audioManager.isMicrophoneMute)
                audioManager.isMicrophoneMute = true
        } else {
            if (audioManager.isMicrophoneMute)
                audioManager.isMicrophoneMute = false
        }
    }

    private fun addStreamToLocalPeer() {
        val stream = peerConnectionFactory?.createLocalMediaStream("102")
        stream?.addTrack(localAudioTrack)
        if (isVideo)
            stream?.addTrack(localVideoTrack)
        localPeer?.addStream(stream)
    }

    private fun doCall(name: String) {
        sdpConstraints = MediaConstraints()
        sdpConstraints.mandatory.add(
                MediaConstraints.KeyValuePair("OfferToReceiveAudio", "true")
        )
        if (isVideo) {
            sdpConstraints.mandatory.add(
                    MediaConstraints.KeyValuePair("OfferToReceiveVideo", "true")
            )
        }
        localPeer?.createOffer(object : CustomSdpObserver("localCreateOffer") {
            override fun onCreateSuccess(sessionDescription: SessionDescription) {
                super.onCreateSuccess(sessionDescription)
                localPeer?.setLocalDescription(
                        CustomSdpObserver("localSetLocalDesc"),
                        sessionDescription
                )
                val jsonObject = JSONObject()
                jsonObject.put("sdp", sessionDescription.description)
                SocketIO.getInstance()
                        .createOffer(
                                sessionDescription,
                                jsonObject,
                                callerID.toString(),
                                isVideo,
                                name,
                                callId.toString()
                        )

            }
        }, sdpConstraints)
    }

    private fun answerCall() {
        logE("onOfferReceived")
        binding.ibAnswer.visibility = View.GONE
        if (ringtune?.isPlaying == true) {
            ringtune?.stop()
        }
        binding.tvCallStatus.text = "Connecting..."
        doAnswer()
        if (isVideo) {
            updateVideoViews()
            turnOnSpeakers()
        }
    }

    private fun doAnswer() {
        localPeer?.createAnswer(object : CustomSdpObserver("localCreateAnswer") {
            override fun onCreateSuccess(sessionDescription: SessionDescription) {
                super.onCreateSuccess(sessionDescription)
                logE("doAnswer : onCreateSuccess $sessionDescription")
                localPeer?.setLocalDescription(
                        CustomSdpObserver("localSetLocal"),
                        sessionDescription
                )
                val json = JSONObject()
                json.put("sdp", sessionDescription.description)
                SocketIO.getInstance()
                        .doAnswer(sessionDescription, json, callerID.toString(), callId.toString())
            }
        }, MediaConstraints())
    }

    private fun saveIceCandidates(data: JSONObject) {
        try {
            logE("onIceCandidateReceived : $data")
            val candidates = JSONObject(data.getString("candidate"))
            localPeer?.addIceCandidate(
                    IceCandidate(
                            candidates.getString("sdpMid"),
                            candidates.getInt("sdpMLineIndex"),
                            candidates.getString("sdp")
                    )
            )
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun onAnswerSave(data: JSONObject) {
        try {
            logE("onAnswerReceived : $data")
            val answer = JSONObject(data.getString("offer"))
            localPeer?.setRemoteDescription(
                    CustomSdpObserver("localSetRemote"),
                    SessionDescription(
                            SessionDescription.Type.fromCanonicalForm(data.getString("type").toLowerCase()),
                            answer.getString("sdp")
                    )
            )
            updateVideoViews()
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    private fun updateVideoViews() {
        val params = binding.localVideoView.layoutParams
        params.height = dpToPx()
        params.width = dpToPx()
        binding.localVideoView.layoutParams = params
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.ibHangUp -> {
                SocketIO.getInstance().onHangout(callerID.toString(), callId.toString())
                hangup()
            }
            R.id.ibAnswer -> {
                if (isFullScreenIntent) {
                    binding.ibAnswer.visibility = View.GONE
                    binding.tvCallStatus.text = "Connecting..."
                    logE("Answering ready for call")
                    SocketIO.getInstance().onReadyForCall(
                            callerID.toString(),
                            calleeName.toString(),
                            callId.toString()
                    )
                    isCallAccepted = true
                    stopAndroid10Service()
                } else answerCall()
            }
            R.id.rlCameras -> {
                if (isVideo) {
                    if (binding.header.visibility == View.VISIBLE) {
                        binding.rlBottomCalls.visibility = View.GONE
                        binding.header.visibility = View.GONE
                    } else {
                        binding.header.visibility = View.VISIBLE
                        binding.rlBottomCalls.visibility = View.VISIBLE
                    }
                }
            }
            R.id.ibSpeaker -> {
                if (!isSpeakerOn) {
                    binding.ibSpeaker.setBackgroundResource(R.drawable.speaker_on)
                    isSpeakerOn = true
                    turnOnSpeakers()
                } else {
                    binding.ibSpeaker.setBackgroundResource(R.drawable.speaker_off)
                    isSpeakerOn = false
                    turnOFFSpeakers()
                }
            }
            R.id.ibMute -> {
                if (!isMicOn) {
                    binding.ibMute.setBackgroundResource(R.drawable.icon_mute)
                    isMicOn = true
                    muteMicroPhone(true)
                } else {
                    binding.ibMute.setBackgroundResource(R.drawable.icon_unmute)
                    isMicOn = false
                    muteMicroPhone(false)
                }
            }
            R.id.ibCameraSwitch -> {
                switchCamera()
            }
            R.id.ibMinimize -> {
                onBackPressed()
            }
        }
    }

    private fun hangup() {
        try {
            //Shifting call from dialoge to main screen
            if (dialog?.isShowing == true) {
//                secondCallRunning = false
                isCallAccepted = false
                jsonObject?.let {
                    SocketIO.getInstance().onReadyForCall(
                            it.getString(AppConstants.CONNECTED_USER_ID),
                            it.getString("name"),
                            it.getString("callId")
                    )
                }
                dialog?.dismiss()
                binding.tvName.text = calleeName.toString()
                stopChronoMeter()
                binding.chronometer.visibility = View.GONE
                binding.tvCallStatus.visibility = View.VISIBLE
                binding.tvCallStatus.text = "Incoming..."
                binding.ibAnswer.visibility = View.VISIBLE
            } else {
                if (ringtune?.isPlaying == true) {
                    ringtune?.stop()
                }
                peerConnectionFactory?.stopAecDump()
                localPeer?.close()
                localPeer = null
                if (isVideo) {
                    if (videoCapture != null) {
                        videoCapture?.stopCapture()
                        videoCapture?.dispose()
                        videoCapture = null
                    }
                    binding.localVideoView.release()
                    binding.remoteVideoView.release()
                }
                stopDialTune()
                turnOFFSpeakers()
                muteMicroPhone(false)
                Global.isOngoingCall = false
                handler?.removeCallbacks(callTimeRunnable)
                stopAndroid10Service()
                SocketIO.getInstance().setSocketCallback(null)
                logE("Finishing Activity")
                finish()
                overridePendingTransition(R.anim.anim_nothing, R.anim.bottom_down)
                if (isFromPush) {
                    logE("Exiting Process")
                    exitProcess(0)
                } else {
                    SocketIO.getInstance().reAssignOfferListenerToMain()
                }
            }
        } catch (e: Exception) {
            logE("Exception Occurred ${e.message}")
        }
    }

    private fun dpToPx(): Int {
        val displayMetrics = resources.displayMetrics
        return (resources.getDimension(R.dimen._50sdp) * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT)).roundToInt()
    }

    private fun createCameraCapturer(enumerator: CameraEnumerator): VideoCapturer? {
        val deviceNames = enumerator.deviceNames

        logE("Looking for front facing cameras.")
        for (deviceName in deviceNames) {
            if (enumerator.isFrontFacing(deviceName)) {
                videoCapture = enumerator.createCapturer(deviceName, null)
                if (videoCapture != null) {
                    return videoCapture
                }
            }
        }
        logE("Looking for other cameras.")
        for (deviceName in deviceNames) {
            if (!enumerator.isFrontFacing(deviceName)) {
                videoCapture = enumerator.createCapturer(deviceName, null)
                if (videoCapture != null) {
                    return videoCapture
                }
            }
        }
        return null
    }

    private fun switchCamera() {
        if (videoCapture != null) {
            if (videoCapture is CameraVideoCapturer) {
                val cameraVideoCapturer = videoCapture as CameraVideoCapturer
                cameraVideoCapturer.switchCamera(null)
            } else {
                // Will not switch camera, video capturer is not a camera
            }
        }
    }

    private fun startPictureInPictureFeature() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val pictureInPictureParamsBuilder: PictureInPictureParams.Builder =
                    PictureInPictureParams.Builder()
            if (isVideo) {
                val aspectRatio = Rational(binding.rlCameras.width, binding.rlCameras.height)
                pictureInPictureParamsBuilder.setAspectRatio(aspectRatio).build()
                enterPictureInPictureMode(pictureInPictureParamsBuilder.build())
            } else {
                val aspectRatio = Rational(binding.ivUserAudio.width, binding.ivUserAudio.height)
                pictureInPictureParamsBuilder.setAspectRatio(aspectRatio).build()
                enterPictureInPictureMode(pictureInPictureParamsBuilder.build())
            }
        } else {
            val builder1 = AlertDialog.Builder(this)
            builder1.setTitle("End Call")
            builder1.setMessage("Do you really want to end this call?")
            builder1.setCancelable(true)
            builder1.setPositiveButton("Yes") { dialog, id ->
                dialog.cancel()
                SocketIO.getInstance().onHangout(callerID.toString(), callId.toString())
                hangup()
            }
            builder1.setNegativeButton("No") { dialog, id ->
                dialog.cancel()
            }
            builder1.create().show()
        }
    }

    override fun onPictureInPictureModeChanged(
            isInPictureInPictureMode: Boolean,
            newConfig: Configuration?
    ) {
        if (isInPictureInPictureMode) {
            binding.header.visibility = View.GONE
            binding.rlBottomCalls.visibility = View.GONE
            binding.rlBottomOptions.visibility = View.GONE
            binding.ibMinimize.visibility = View.GONE
            if (isVideo) {
                binding.localVideoView.visibility = View.GONE
                binding.remoteVideoView.visibility = View.VISIBLE
            } else {
                binding.ivUserAudio.visibility = View.VISIBLE
            }
        } else {
            binding.ibMinimize.visibility = View.VISIBLE
            binding.header.visibility = View.VISIBLE
            binding.rlBottomCalls.visibility = View.VISIBLE
            binding.rlBottomOptions.visibility = View.VISIBLE
            if (isVideo) {
                binding.localVideoView.visibility = View.VISIBLE
                binding.remoteVideoView.visibility = View.VISIBLE
            } else {
                binding.ivUserAudio.visibility = View.VISIBLE
            }
            logE("onPictureInPictureModeChanged else :$isInPictureInPictureMode")
        }
    }

    override fun onBackPressed() {
        Global.isOngoingCall = true
        Global.calleeName =
                StringBuilder(calleeName.toString()).append(" ").append("is on call").toString()
        Global.callTimer = binding.chronometer.base
        startPictureInPictureFeature()
    }

    private fun logE(msg: String) {
        Debugger.e(TAG, msg)
    }

    override fun webSocketCallback(jsonObject: JSONObject) {
        runOnUiThread {
            when (jsonObject.getString(AppConstants.TYPE)) {
                AppConstants.CANDIDATE -> {
                    logE("iceCandidates Received :$jsonObject")
                    saveIceCandidates(jsonObject)
                }
                AppConstants.ANSWER -> {
                    stopDialTune()
                    logE("answer Received :$jsonObject")
                    onAnswerSave(jsonObject)
                    binding.tvCallStatus.text = "Connecting..."
                }
                AppConstants.REJECT -> {
                    if (callerID == jsonObject.getString(AppConstants.CONNECTED_USER_ID).toInt()) {
                        logE("Reject Received :$jsonObject")
                        stopDialTune()
                        hangup()
                    } else {
                        dialog?.dismiss()
                    }
                }
                AppConstants.READY_FOR_CALL -> {
                    createPeerConnection()
                    callId = jsonObject.getString("callId")
                    doCall(jsonObject.getString("name"))
                    startDialTune()
                    binding.tvCallStatus.text = "Ringing..."
                    if (isVideo) {
                        turnOnSpeakers()
                    }
                }
            }
        }
    }

    private fun secondCallDialogue(jsonObject: JSONObject) {
        dialog = Dialog(this, android.R.style.Theme_Translucent_NoTitleBar)
        dialog?.setCancelable(false)
        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog?.setContentView(R.layout.secondcall_notify)
        dialog?.btnCancelCall?.setOnClickListener {
            dialog?.dismiss()
            vibrator?.cancel()
            SocketIO.getInstance().onHangout(
                    jsonObject.getString(AppConstants.CONNECTED_USER_ID),
                    jsonObject.getString("callId")
            )
        }
        dialog?.btnAcceptReject?.setOnClickListener {
            dialog?.dismiss()
            vibrator?.cancel()
            SocketIO.getInstance().onHangout(callerID.toString(), callId.toString())
//            secondCallRunning = true
            isCallAccepted = true
            isFromReconnecting = false
            logE(" ready for call second call dialopge")
            SocketIO.getInstance().onReadyForCall(
                    jsonObject.getString(AppConstants.CONNECTED_USER_ID),
                    jsonObject.getString("name"),
                    jsonObject.getString("callId")
            )
            stopChronoMeter()
            binding.chronometer.visibility = View.GONE
            binding.tvCallStatus.visibility = View.VISIBLE
            binding.tvCallStatus.text = "Connecting..."
        }
        dialog?.tvTitleCall?.text =
                StringBuilder(jsonObject.getString("name").toString()).append(" ")
                        .append("is calling....").toString()
        dialog?.show()
    }

    private fun stopChronoMeter() {
        binding.chronometer.stop()
        binding.chronometer.base = SystemClock.elapsedRealtime()
    }

    private fun startDialTune() {
        try {
            toneGenerator = ToneGenerator(AudioManager.STREAM_VOICE_CALL, 100)
            dialTuneHandler = Handler(Looper.getMainLooper())
            dialtuneRunnable = object : Runnable {
                override fun run() {
                    logE("toneGenerator is not null")
                    toneGenerator?.startTone(
                            ToneGenerator.TONE_CDMA_NETWORK_USA_RINGBACK, 1500
                    )
                    dialTuneHandler?.postDelayed(this, 4000)
                }
            }
            dialTuneHandler?.post(dialtuneRunnable)
        } catch (e: Exception) {
            logE("Exception while playing dial Tune :${e.message}")
        }
    }

    private fun startReconnectingTune() {
        try {
            toneGenerator = ToneGenerator(AudioManager.STREAM_VOICE_CALL, 100)
            dialTuneHandler = Handler(Looper.getMainLooper())
            dialtuneRunnable = object : Runnable {
                override fun run() {
                    logE("toneGenerator is not null")
                    toneGenerator?.startTone(
                            ToneGenerator.TONE_CDMA_NETWORK_USA_RINGBACK, 500
                    )
                    dialTuneHandler?.postDelayed(this, 1000)
                }
            }
            dialTuneHandler?.post(dialtuneRunnable)
        } catch (e: Exception) {
            logE("Exception while playing dial Tune :${e.message}")
        }
    }

    private fun stopDialTune() {
        toneGenerator?.stopTone()
        dialTuneHandler?.removeCallbacks(dialtuneRunnable)
    }

    private fun vibratePhone() {
        vibrator = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        if (Build.VERSION.SDK_INT >= 26) {
            vibrator?.vibrate(
                    VibrationEffect.createOneShot(
                            1000,
                            VibrationEffect.DEFAULT_AMPLITUDE
                    )
            )
        } else {
            vibrator?.vibrate(2000)
        }
    }

    private fun stopAndroid10Service() {
        if (Global.isMyServiceRunning(this, CallService::class.java)) {
            val it = Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS)
            sendBroadcast(it)
            stopService(Intent(this, CallService::class.java))
            Global.ringtune?.stop()
        }
    }

    override fun newCallListener(jsonObject: JSONObject) {
        runOnUiThread {
            when (jsonObject.getString(AppConstants.TYPE)) {
                AppConstants.NEW_CALL -> {
                    this.jsonObject = jsonObject
                    logE("offer received $jsonObject")
                    vibratePhone()
                    secondCallDialogue(jsonObject)
                }
                AppConstants.OFFER -> {
                    logE("offer received in listener $jsonObject")
                    newCallReceived(jsonObject)
                    setCallerProfile(this.isVideo)
                    if (isCallAccepted) {

                        answerCall()
                        isCallAccepted = false
                    }
                }
            }
        }
    }

}
