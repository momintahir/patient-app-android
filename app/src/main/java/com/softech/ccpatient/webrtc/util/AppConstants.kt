package com.softech.ccpatient.webrtc.util

object AppConstants {


    const val CALLER_USER_ID = "caller_user_id"
    const val INITIATOR = "caller_initiator"
    const val IS_FROM_PUSH = "is_call_boolean"
    const val IS_CALL_ACCEPTED = "is_call_accepted"
    const val IS_FULL_SCREEN_INTENT = "is_full_screen_intent"
    const val CHAT_USER_NAME = "chat_user_name"
    const val CHAT_USER_PICTURE = "chat_user_picture"
    const val CONNECTED_USER_ID = "connectedUserId"
    const val JSON = "webrtc_json"
    const val IS_VIDEO_CALL = "isVideoCall"
    const val PUSH_TOKEN = "push_token"


    //WebRtc Actions
    const val CANDIDATE = "candidate"
    const val NEW_CALL = "newcall"
    const val READY_FOR_CALL = "readyforcall"
    const val ANSWER = "answer"
    const val OFFER = "offer"
    const val TYPE = "type"
    const val REJECT = "reject"
    const val UPDATE_PUSH_TOKEN = "updatepushtoken"
    const val IS_NEW_TOKEN = "is_new_token"
}
