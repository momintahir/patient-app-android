package com.softech.ccpatient.webrtc.services

import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.os.Handler
import android.os.IBinder
import androidx.core.app.NotificationCompat
import com.softech.ccpatient.R
import com.softech.ccpatient.webrtc.CallActivity
import com.softech.ccpatient.webrtc.util.AppConstants
import com.softech.ccpatient.webrtc.util.Debugger
import com.softech.ccpatient.webrtc.util.Global


class CallService : Service() {
    private var connectedUserId = ""
    private var name = ""
    private var callId = ""
    private var isVideo: Boolean? = null
    private var handler: Handler? = null
    private var callTimeRunnable: Runnable? = null
    private var isAlreadyConnected: Boolean? = null

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        connectedUserId = intent?.getStringExtra(AppConstants.CALLER_USER_ID).toString()
        name = intent?.getStringExtra(AppConstants.CHAT_USER_NAME).toString()
        callId = intent?.getStringExtra("callId").toString()
        isVideo = intent?.getBooleanExtra(AppConstants.IS_VIDEO_CALL, false)
        isAlreadyConnected = intent?.getBooleanExtra("is_already_connected", false)
        startActivity()
        return START_STICKY
    }

    private fun startActivity() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            try {
                handler = Handler()
                callTimeRunnable = Runnable {
                    Debugger.e("service", "startActivity service called")
                    Global.ringtune?.stop()
                    stopSelf()
                    handler?.removeCallbacks(callTimeRunnable)
                }
                handler?.postDelayed(callTimeRunnable, 90 * 1000)
                val sound: Uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE)
                Global.ringtune = RingtoneManager.getRingtone(this, sound)
                Global.ringtune?.play()
                Global.ringtune?.isLooping = true
                val notificationManager =
                        getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager?
                val notificationId = (System.currentTimeMillis() / 1000L).toInt()

                val channelId: String = ("voip_iD")
                val channelName: String = ("Voip Calls")
                assert(notificationManager != null)
                var mChannel =
                        notificationManager?.getNotificationChannel(channelId)
                if (mChannel == null) {
                    mChannel = NotificationChannel(
                            channelId,
                            channelName,
                            NotificationManager.IMPORTANCE_HIGH
                    )
                    notificationManager?.createNotificationChannel(mChannel)
                }
                val builder = NotificationCompat.Builder(this, channelId)
                val description: String = if (isVideo == true) {
                    "Incoming video call from $name"
                } else {
                    "Incoming audio call from $name"
                }

                builder.setSmallIcon(Global.getNotificationIcon())
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(description)
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setCategory(NotificationCompat.CATEGORY_CALL)
                        .setColor(Color.parseColor("#179a63"))
                        .setFullScreenIntent(
                                fullScreenIntent(
                                        this,
                                        isVideo!!,
                                        connectedUserId.toInt(),
                                        name,
                                        callId,
                                        notificationId
                                ), true
                        )
                        .addAction(
                                R.drawable.call_cancel,
                                "Reject",
                                rejectCallIntent(this, connectedUserId.toInt(), callId, notificationId)
                        )
                        .addAction(
                                R.drawable.call_receive,
                                "Answer",
                                acceptCallIntent(
                                        this,
                                        isVideo!!,
                                        connectedUserId.toInt(),
                                        name,
                                        callId,
                                        notificationId
                                )
                        )
                        .setAutoCancel(true)
                        .setOngoing(true)
                val notification: Notification = builder.build()
                notificationManager?.notify(notificationId, notification)
                startForeground(notificationId, notification)
            } catch (e: Exception) {
                Debugger.e("service", "exception :${e.message}")
            }
        }
    }

    private fun acceptCallIntent(
            context: Context,
            isVideo: Boolean,
            callerID: Int,
            name: String,
            callId: String,
            notificationId: Int

    ): PendingIntent? {
        val intent = Intent(context, CallActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.putExtra(AppConstants.IS_FROM_PUSH, true)
        if (isAlreadyConnected == true)
            intent.putExtra(AppConstants.IS_FROM_PUSH, false)
        else intent.putExtra(AppConstants.IS_FROM_PUSH, true)

        intent.putExtra(AppConstants.IS_CALL_ACCEPTED, true)
        intent.putExtra(AppConstants.IS_FULL_SCREEN_INTENT, true)
        intent.putExtra(AppConstants.IS_VIDEO_CALL, isVideo)
        intent.putExtra(AppConstants.CALLER_USER_ID, callerID)
        intent.putExtra(AppConstants.CHAT_USER_NAME, name)
        intent.putExtra("notificationId", notificationId)
        intent.putExtra("callId", callId)

        return PendingIntent.getActivity(
                context,
                notificationId + 1,
                intent,
                PendingIntent.FLAG_ONE_SHOT
        )
    }

    private fun rejectCallIntent(
            context: Context,
            callerID: Int,
            callId: String,
            notificationId: Int

    ): PendingIntent? {
        val intent = Intent(context, RejectCallReceiver::class.java)
        intent.putExtra(AppConstants.CALLER_USER_ID, callerID)
        intent.putExtra("callId", callId)
        intent.putExtra("is_already_connected", isAlreadyConnected)
        intent.putExtra("notificationId", notificationId)

        return PendingIntent.getBroadcast(
                context,
                notificationId + 2,
                intent,
                0
        )
    }

    private fun fullScreenIntent(
            context: Context,
            isVideo: Boolean,
            callerID: Int,
            name: String,
            callId: String,
            notificationId: Int

    ): PendingIntent? {
        val intent = Intent(context, CallActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.putExtra(AppConstants.IS_FROM_PUSH, true)
        if (isAlreadyConnected == true)
            intent.putExtra(AppConstants.IS_FROM_PUSH, false)
        else intent.putExtra(AppConstants.IS_FROM_PUSH, true)

        intent.putExtra(AppConstants.IS_CALL_ACCEPTED, false)
        intent.putExtra(AppConstants.IS_FULL_SCREEN_INTENT, true)
        intent.putExtra(AppConstants.IS_VIDEO_CALL, isVideo)
        intent.putExtra(AppConstants.CALLER_USER_ID, callerID)
        intent.putExtra(AppConstants.CHAT_USER_NAME, name)
        intent.putExtra("notificationId", notificationId)
        intent.putExtra("callId", callId)

        return PendingIntent.getActivity(
                context,
                notificationId + 3,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT
        )
    }
}
