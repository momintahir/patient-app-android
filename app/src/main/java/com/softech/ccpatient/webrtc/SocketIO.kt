package com.softech.ccpatient.webrtc

import android.content.Context
import android.content.Intent
import android.os.Build
import com.github.nkzawa.socketio.client.IO
import com.github.nkzawa.socketio.client.Socket
import com.google.gson.JsonObject
import com.softech.ccpatient.webrtc.callbacks.RejectCallback
import com.softech.ccpatient.webrtc.callbacks.WebSocketCallback
import com.softech.ccpatient.webrtc.callbacks.WebSocketNewCallListener
import com.softech.ccpatient.webrtc.services.CallService
import com.softech.ccpatient.webrtc.util.AppConstants
import com.softech.ccpatient.webrtc.util.Debugger
import org.json.JSONObject
import org.webrtc.SessionDescription


class SocketIO private constructor() {
    private val TAG = this.javaClass.simpleName
    var socket: Socket? = null

    //Calling params
    private var webSocketCallback: WebSocketCallback? = null
    private var rejectCallback: RejectCallback? = null
    private var webSocketNewCallListener: WebSocketNewCallListener? = null
    private var dummyWebSocketNewCallListener: WebSocketNewCallListener? = null
    private var isFromPush = false
    private var connectedUserId = ""
    private var name = ""
    private var callId = ""
    private var isVideo: Boolean = false
    private var isAlreadyConnected: Boolean = false

    companion object {
        private var instance: SocketIO? = null
        var context: Context? = null

        @Synchronized
        fun getInstance(): SocketIO {
            if (instance == null) {
                instance = SocketIO()
            }
            return instance as SocketIO
        }

        @Synchronized
        fun getInstance(context: Context): SocketIO {
            Companion.context = context
            if (instance == null) {
                instance = SocketIO()
            }
            return instance as SocketIO
        }

    }

    fun connectSocket(userId: String, userName: String) {
        val opts = IO.Options()
        opts.forceNew = true
        opts.query = "user_id=$userId&user_name=$userName&device_type=android&user_type=patient"
        socket = IO.socket("http://184.169.185.200:9090/", opts)
        socket?.connect()
        Debugger.e("Socket", "user_id=$userId")
        Debugger.e("Socket", "user_name=$userName")
    }

    fun connectListeners() {
        socket?.on(Socket.EVENT_CONNECT) {
            Debugger.e(TAG, "==============CONNECTED==============")
            if (isFromPush) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    isFromPush = false
                    val intent = Intent(context, CallService::class.java)
                    intent.putExtra(AppConstants.IS_VIDEO_CALL, isVideo)
                    intent.putExtra(AppConstants.CALLER_USER_ID, connectedUserId)
                    intent.putExtra(AppConstants.CHAT_USER_NAME, name)
                    intent.putExtra("callId", callId)
                    intent.putExtra("is_already_connected", isAlreadyConnected)
                    context?.startForegroundService(intent)
                } else {
                    Debugger.e(TAG, "ready for call in socket connection")
                    onReadyForCall(connectedUserId, name, callId)
                }
            }
        }?.on(Socket.EVENT_DISCONNECT) {
            Debugger.e(TAG, "===============OFF===============")

        }?.on(AppConstants.NEW_CALL) {
            val json = it[0] as JSONObject
            webSocketNewCallListener?.newCallListener(json)

        }?.on(AppConstants.OFFER) {
            val json = it[0] as JSONObject
            if (isFromPush) {
                isFromPush = false
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
                    startCallActivity(json, json.getBoolean("isVideo"))
                }
            } else {
                logE("Sending offer to listener")
                webSocketNewCallListener?.newCallListener(json)
            }

        }?.on(AppConstants.CANDIDATE) {
            val json = it[0] as JSONObject
            webSocketCallback?.webSocketCallback(json)

        }?.on(AppConstants.ANSWER) {
            val json = it[0] as JSONObject
            webSocketCallback?.webSocketCallback(json)

        }?.on(AppConstants.REJECT) {
            val json = it[0] as JSONObject
            webSocketCallback?.webSocketCallback(json)
            rejectCallback?.onReject(json)

        }?.on(AppConstants.READY_FOR_CALL) {
            val json = it[0] as JSONObject
            webSocketCallback?.webSocketCallback(json)
        }
    }

    private fun startCallActivity(json: JSONObject, isVideo: Boolean) {
        logE("isVideo Call :$isVideo")
        val intent = Intent(context, CallActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.putExtra(AppConstants.JSON, json.toString())
        intent.putExtra(AppConstants.IS_FROM_PUSH, true)
        intent.putExtra(AppConstants.IS_VIDEO_CALL, isVideo)
        context?.startActivity(intent)
    }

    fun disconnectSocket() {
        socket?.disconnect()
        socket?.off(AppConstants.NEW_CALL)
        socket?.off(AppConstants.OFFER)
        socket?.off(AppConstants.CANDIDATE)
        socket?.off(AppConstants.ANSWER)
        socket?.off(AppConstants.REJECT)
        socket?.off(AppConstants.READY_FOR_CALL)
        socket?.close()
        instance = null
        Debugger.e("SocketIO", "Socket disconnectSocket")
    }

    // Call Functions

    fun setSocketCallback(webSocketCallback: WebSocketCallback?) {
        this.webSocketCallback = webSocketCallback
    }

    fun setOfferListener(
            webSocketNewCallListener: WebSocketNewCallListener,
            isMainOfferCallback: Boolean
    ) {
        if (isMainOfferCallback) {
            this.webSocketNewCallListener = webSocketNewCallListener
        } else {
            dummyWebSocketNewCallListener = this.webSocketNewCallListener
            this.webSocketNewCallListener = webSocketNewCallListener
        }
    }

    fun reAssignOfferListenerToMain() {
        this.webSocketNewCallListener = dummyWebSocketNewCallListener
    }


    fun setPushData(
            connectedUserId: String,
            isFromPush: Boolean,
            name: String,
            callId: String,
            isVideo: String?,
            isAlreadyConnected: Boolean
    ) {
        this.connectedUserId = connectedUserId
        this.isFromPush = isFromPush
        this.name = name
        this.callId = callId
        this.isVideo = isVideo != "0"
        this.isAlreadyConnected = isAlreadyConnected
//        this.isVideo = isVideo!!
        logE("isFromPush :$isFromPush")
    }


    fun onReadyForCall(connectedUserId: String, name: String, callId: String) {
        val jsonObject = JsonObject()
        jsonObject.addProperty("type", AppConstants.READY_FOR_CALL)
        jsonObject.addProperty("connectedUserId", connectedUserId)
        jsonObject.addProperty("name", name)
        jsonObject.addProperty("callId", callId)
        logE("readyForCall sent $jsonObject")
        socket?.emit(AppConstants.READY_FOR_CALL, jsonObject)
    }


    fun sendIceCandidates(iceCandidate: JSONObject, callerID: String) {
        val jsonObject = JSONObject()
        jsonObject.put("type", AppConstants.CANDIDATE)
        jsonObject.put("candidate", iceCandidate)
        jsonObject.put("connectedUserId", callerID)
        logE("send IceCandidate $jsonObject")
        socket?.emit(AppConstants.CANDIDATE, jsonObject.toString())
    }

    fun createOffer(
            sessionDescription: SessionDescription,
            offer: JSONObject,
            callerID: String,
            isVideo: Boolean,
            name: String,
            callId: String
    ) {
        val obj = JSONObject()
        obj.put("type", sessionDescription.type.canonicalForm())
        obj.put("offer", offer)
        obj.put("connectedUserId", callerID)
        obj.put("isVideo", isVideo)
        obj.put("name", name)
        obj.put("callId", callId)
        logE("createOffer $obj")
        socket?.emit(AppConstants.OFFER, obj.toString())
    }

    fun doAnswer(
            sessionDescription: SessionDescription,
            offer: JSONObject,
            callerID: String,
            callId: String
    ) {
        val obj = JSONObject()
        obj.put("type", sessionDescription.type.canonicalForm())
        obj.put("offer", offer)
        obj.put("connectedUserId", callerID)
        obj.put("callId", callId)
        logE("doAnswer :$obj")
        socket?.emit(AppConstants.ANSWER, obj.toString())
    }

    fun onHangout(id: String, callId: String) {
        val obj = JsonObject()
        obj.addProperty("type", AppConstants.REJECT)
        obj.addProperty("connectedUserId", id)
        obj.addProperty("callId", callId)
        socket?.emit(AppConstants.REJECT, obj)
    }

    fun didEnterBackground(id: String) {
        val obj = JsonObject()
        obj.addProperty("type", "didEnterBackground")
        obj.addProperty("userId", id)
        socket?.emit("didEnterBackground", obj)
    }

    fun didEnterForeground(id: String) {
        val obj = JsonObject()
        obj.addProperty("type", "didEnterForeground")
        obj.addProperty("userId", id)
        socket?.emit("didEnterForeground", obj)
    }

    fun onNewCall(id: String, isVideo: String) {
        val obj = JsonObject()
        obj.addProperty("type", AppConstants.NEW_CALL)
        obj.addProperty("connectedUserId", id)
        obj.addProperty("isVideo", isVideo)
        logE("onNewCall sent $obj")
        socket?.emit(AppConstants.NEW_CALL, obj)
    }

    fun updatePushToken(token: String) {
        val obj = JsonObject()
        obj.addProperty("push_token", token)
        logE("updatePushToken sent $obj")
        socket?.emit(AppConstants.UPDATE_PUSH_TOKEN, obj)
    }

    private fun logE(msg: String) {
        Debugger.e(TAG, msg)
    }
}