package com.softech.ccpatient.interfaces;

/**
 * Created by Robin.Yaqoob on 12-Oct-17.
 */

public interface IWebListener {

    void success(String response);
    void error(String response);

}
