package com.softech.ccpatient.interfaces;

import com.softech.ccpatient.data.models.models.CalendarByDate.SlotsDatum;

/**
 * Created by saimshafqat on 09/08/2018.
 */

public interface SendCalendarData
{
    void sendCalandarData(  SlotsDatum slotsDatum);
}
