
package com.softech.ccpatient.data.models.models.CalendarByDate;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DayInterval {

    @SerializedName("iSlotID")
    @Expose
    private Integer iSlotID;
    @SerializedName("vDay")
    @Expose
    private String vDay;
    @SerializedName("vStartTime")
    @Expose
    private String vStartTime;
    @SerializedName("vEndTime")
    @Expose
    private String vEndTime;
    @SerializedName("StartDate")
    @Expose
    private String startDate;
    @SerializedName("EndDate")
    @Expose
    private String endDate;
    @SerializedName("iSlotDuration")
    @Expose
    private Integer iSlotDuration;
    @SerializedName("iCalendarID")
    @Expose
    private Integer iCalendarID;
    @SerializedName("vPatientName")
    @Expose
    private Object vPatientName;
    @SerializedName("SlotsData")
    @Expose
    private List<SlotsDatum> slotsData = null;

    public Integer getISlotID() {
        return iSlotID;
    }

    public void setISlotID(Integer iSlotID) {
        this.iSlotID = iSlotID;
    }

    public String getVDay() {
        return vDay;
    }

    public void setVDay(String vDay) {
        this.vDay = vDay;
    }

    public String getVStartTime() {
        return vStartTime;
    }

    public void setVStartTime(String vStartTime) {
        this.vStartTime = vStartTime;
    }

    public String getVEndTime() {
        return vEndTime;
    }

    public void setVEndTime(String vEndTime) {
        this.vEndTime = vEndTime;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Integer getISlotDuration() {
        return iSlotDuration;
    }

    public void setISlotDuration(Integer iSlotDuration) {
        this.iSlotDuration = iSlotDuration;
    }

    public Integer getICalendarID() {
        return iCalendarID;
    }

    public void setICalendarID(Integer iCalendarID) {
        this.iCalendarID = iCalendarID;
    }

    public Object getVPatientName() {
        return vPatientName;
    }

    public void setVPatientName(Object vPatientName) {
        this.vPatientName = vPatientName;
    }

    public List<SlotsDatum> getSlotsData() {
        return slotsData;
    }

    public void setSlotsData(List<SlotsDatum> slotsData) {
        this.slotsData = slotsData;
    }

}
