package com.softech.ccpatient.data.models.models

import com.google.gson.annotations.SerializedName

class LoadFile {

    @SerializedName("OID")
    val oId: String? = null
    @SerializedName("PATIENTCHARTFOLDER")
    val patientChartFolder: String? = null
    @SerializedName("FILENAME")
    val fileName: String? = null
    @SerializedName("FILETYPE")
    val fileType: String? = null
    @SerializedName("FILEPATH")
    val filePath: String? = null
    @SerializedName("FILEDATA")
    val fileData: String? = null
    @SerializedName("FILETHUMB")
    val fileThumb: String? = null
    @SerializedName("FILEREMARKS")
    val fileRemarks: String? = null
    @SerializedName("FILESIZE")
    val fileSize: String? = null
    @SerializedName("INSERTUSER")
    val insertUser: String? = null
    @SerializedName("Total")
    val total: String? = null

    @SerializedName("FILE")
    val file: String? = null

    override fun toString(): String {
        return "LoadFile(oId=$oId, patientChartFolder=$patientChartFolder, fileName=$fileName, fileType=$fileType, filePath=$filePath, fileData=$fileData, fileThumb=$fileThumb, fileRemarks=$fileRemarks, fileSize=$fileSize, insertUser=$insertUser, total=$total)"
    }


}