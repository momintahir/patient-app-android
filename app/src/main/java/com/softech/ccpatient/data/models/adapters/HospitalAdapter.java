package com.softech.ccpatient.data.models.adapters;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.softech.ccpatient.R;
import com.softech.ccpatient.activities.WardsActivity;

import java.util.List;

/**
 * Created by Robin.Yaqoob on 06-Sep-17.
 */

public class HospitalAdapter extends RecyclerView.Adapter<HospitalAdapter.MyViewHolder> {

    private List<String> moviesList;
    private Activity activity;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewHospitalTitle,textViewHospitalAddress;

        public MyViewHolder(View view) {
            super(view);
            textViewHospitalTitle = (TextView) view.findViewById(R.id.textViewHospitalTitle);
            textViewHospitalAddress   = (TextView) view.findViewById(R.id.textViewHospitalAddress);
        }
    }


    public HospitalAdapter(List<String> moviesList, Activity activity) {
        this.moviesList = moviesList;
        this.activity = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.design_hospitals, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        if (position%2 == 0) {
            holder.textViewHospitalTitle.setBackgroundColor(activity.getResources().getColor(R.color.hospitalbluetop));
            holder.textViewHospitalAddress.setBackgroundColor(activity.getResources().getColor(R.color.hospitalbluebottom));
        } else {
            holder.textViewHospitalTitle.setBackgroundColor(activity.getResources().getColor(R.color.hospitalredtop));
            holder.textViewHospitalAddress.setBackgroundColor(activity.getResources().getColor(R.color.hospitalredbottom));
        }
        Typeface font = Typeface.createFromAsset(activity.getAssets(), "fonts/DOSISBOLD.TTF");
        holder. textViewHospitalTitle.setTypeface(font);
        holder. textViewHospitalAddress.setTypeface(font);
        holder.textViewHospitalTitle.setText(moviesList.get(position));
        holder.textViewHospitalTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(activity, WardsActivity.class);
                activity.startActivity(intent);

            }
        });


    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}