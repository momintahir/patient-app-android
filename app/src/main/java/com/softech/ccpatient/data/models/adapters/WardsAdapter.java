package com.softech.ccpatient.data.models.adapters;
import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.softech.ccpatient.R;

import java.util.List;

/**
 * Created by Robin.Yaqoob on 06-Sep-17.
 */

public class WardsAdapter extends RecyclerView.Adapter<WardsAdapter.MyViewHolder> {

    private List<String> moviesList;
    private Activity activity;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewHospitalTitle,textViewHospitalAddress;

        public MyViewHolder(View view) {
            super(view);
            textViewHospitalTitle = (TextView) view.findViewById(R.id.textViewHospitalTitle);
            textViewHospitalAddress= (TextView) view.findViewById(R.id.textViewHospitalAddress);
        }
    }


    public WardsAdapter(List<String> moviesList, Activity activity) {
        this.moviesList = moviesList;
        this.activity = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.design_wards, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {


        if (position%2 == 0) {
            holder.textViewHospitalTitle.setBackgroundColor(activity.getResources().getColor(R.color.hospitalbluetop));
            holder.textViewHospitalAddress.setBackgroundColor(activity.getResources().getColor(R.color.hospitalbluebottom));
        } else {
            holder.textViewHospitalTitle.setBackgroundColor(activity.getResources().getColor(R.color.hospitalredtop));
            holder.textViewHospitalAddress.setBackgroundColor(activity.getResources().getColor(R.color.hospitalredbottom));
        }

        holder.textViewHospitalTitle.setText(moviesList.get(position));
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}