package com.softech.ccpatient.data.models.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Robin.Yaqoob on 29-Jan-18.
 */

public class SelectClinicModel {

    @SerializedName("OID")
    private String OID;
    @SerializedName("ClinicName")

    private String ClinicName;


    public String getOID() {
        return OID;
    }

    public String getClinicName() {
        return ClinicName;
    }


    @Override
    public String toString() {
        return ClinicName ;
    }
}
