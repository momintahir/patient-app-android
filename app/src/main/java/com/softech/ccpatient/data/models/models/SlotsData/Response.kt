package com.softech.ccpatient.data.models.models.SlotsData

import com.google.gson.annotations.SerializedName

class Response {

    @SerializedName("AppID")
    val appID: Int? = null
    @SerializedName("iPatID")
    val iPatID: Int? = null
    @SerializedName("vPatientName")
    val vPatientName: String? = null
    @SerializedName("EventType")
    val eventType: Int? = null
    @SerializedName("vStartTime")
    val vStartTime: String? = null
    @SerializedName("slotTime")
    val slotTime: String? = null
    @SerializedName("slotEndTime")
    val slotEndTime: String? = null
    @SerializedName("slotGroup")
    val slotGroup: String? = null
    @SerializedName("iSlotID")
    val iSlotID: String? = null
    @SerializedName("iSlotStatus")
    val iSlotStatus: String? = null
    @SerializedName("isAllowed")
    val isAllowed: String? = null
    @SerializedName("iCalendarID")
    val iCalendarID: String? = null
    @SerializedName("isDel")
    val isDel: String? = null
    @SerializedName("IsDisabled")
    val IsDisabled: Int? = null

}
