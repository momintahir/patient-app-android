package com.softech.ccpatient.data.models.models

import com.google.gson.annotations.SerializedName

data class PatientProfile(

        @SerializedName("PatName")
        var PatName:String,
        @SerializedName("PatDOB")
        var PatDOB:String,
        @SerializedName("PatGender")
        var PatGender:String,
        @SerializedName("PatAddress")
        var PatAddress:String,
        @SerializedName("PatCity")
        var PatCity:String,

        @SerializedName("PatCountry")
        var PatCountry:String,
        @SerializedName("PatCountryName")
        var PatCountryName:String,
        @SerializedName("PatCellNumber")
        var PatCellNumber:String,
        @SerializedName("PatMRno")
        var PatMRno:String
)