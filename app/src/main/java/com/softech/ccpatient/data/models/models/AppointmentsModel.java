package com.softech.ccpatient.data.models.models;

import com.google.gson.annotations.SerializedName;


public class AppointmentsModel {

    @SerializedName("iAppID")
    private String iAppID;
    @SerializedName("iPatID")
    private String iPatID;
    @SerializedName("vAppTimeFrom")
    private String AppTimeFrom;
    @SerializedName("ConsultantID")
    private String ConsultantID;
    @SerializedName("AppStatus")
    private String AppStatus;
    @SerializedName("ClinicID")
    private String ClinicID;
    @SerializedName("ClinicName")
    private String ClinicName;
    @SerializedName("DateStr")
    private String DateStr;
    @SerializedName("PatientName")
    private String PatientName;
    @SerializedName("PatientBillNotPaid")
    private String PatientBillNotPaid;

    public String getiAppID() {
        return iAppID;
    }

    public String getiPatID() {
        return iPatID;
    }

    public String getAppTimeFrom() {
        return AppTimeFrom;
    }

    public String getConsultantID() {
        return ConsultantID;
    }

    public String getAppStatus() {
        return AppStatus;
    }

    public String getClinicID() {
        return ClinicID;
    }

    public String getClinicName() {
        return ClinicName;
    }

    public String getDateStr() {
        return DateStr;
    }

    public String getPatientName() {
        return PatientName;
    }

    public String getPatientBillNotPaid() {
        return PatientBillNotPaid;
    }
}
