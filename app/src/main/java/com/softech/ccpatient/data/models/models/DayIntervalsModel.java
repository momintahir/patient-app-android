package com.softech.ccpatient.data.models.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Robin.Yaqoob on 15-Jan-18.
 */

public class DayIntervalsModel {

    @SerializedName("iSlotID")
    private String iSlotID;
    @SerializedName("vDay")
    private String vDay;
    @SerializedName("vStartTime")
    private String vStartTime;
    @SerializedName("vEndTime")
    private String vEndTime;
    @SerializedName("StartDate")
    private String StartDate;
    @SerializedName("EndDate")
    private String EndDate;
    @SerializedName("iSlotDuration")
    private String iSlotDuration;
    @SerializedName("iCalendarID")
    private String iCalendarID;

    public String getiSlotID() {
        return iSlotID;
    }

    public String getvDay() {
        return vDay;
    }

    public String getvStartTime() {
        return vStartTime;
    }

    public String getvEndTime() {
        return vEndTime;
    }

    public String getStartDate() {
        return StartDate;
    }

    public String getEndDate() {
        return EndDate;
    }

    public String getiSlotDuration() {
        return iSlotDuration;
    }

    public String getiCalendarID() {
        return iCalendarID;
    }
}
