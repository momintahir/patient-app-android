package com.softech.ccpatient.data.models.models

import com.google.gson.annotations.SerializedName

class SearchAppointmentNew {
    @SerializedName("ConsultantCode")
    val consultantCode: String? = null
    @SerializedName("ConsultantName")
    val consultantName: String? = null
    @SerializedName("ClinicName")
    val clinicName: String? = null
    @SerializedName("PHONE_NO")
    val phoneNo: String? = null
    @SerializedName("Speciality")
    val speciality: String? = null
    @SerializedName("OrganizationId")
    val organizationId: String? = null

    @SerializedName("IHospitalID")
    val hospitalID: String? = null
    @SerializedName("DrEmail")
    val drEmail: String? = null
    @SerializedName("ClinicAddress")
    val clinicAddress: String? = null
    @SerializedName("FileName")
    val fileName: String? = null
    @SerializedName("ImageUrl")
    val imageUrl: String? = null

    @SerializedName("Fee")
    val opdFee: String? = null

}
