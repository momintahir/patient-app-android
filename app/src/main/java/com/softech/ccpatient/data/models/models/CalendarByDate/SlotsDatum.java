
package com.softech.ccpatient.data.models.models.CalendarByDate;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SlotsDatum {

    @SerializedName("AppID")
    @Expose
    private String appID;
    @SerializedName("iPatID")
    @Expose
    private String iPatID;
    @SerializedName("vPatientName")
    @Expose
    private Object vPatientName;
    @SerializedName("EventType")
    @Expose
    private String eventType;
    @SerializedName("vStartTime")
    @Expose
    private String vStartTime;
    @SerializedName("vEndTime")
    @Expose
    private Object vEndTime;
    @SerializedName("vCell")
    @Expose
    private Object vCell;
    @SerializedName("vNotes")
    @Expose
    private Object vNotes;
    @SerializedName("vMrNo")
    @Expose
    private Object vMrNo;
    @SerializedName("vAge")
    @Expose
    private Object vAge;
    @SerializedName("slotTime")
    @Expose
    private String slotTime;
    @SerializedName("slotEndTime")
    @Expose
    private String slotEndTime;
    @SerializedName("slotGroup")
    @Expose
    private String slotGroup;
    @SerializedName("iSlotID")
    @Expose
    private String iSlotID;
    @SerializedName("iSlotStatus")
    @Expose
    private String iSlotStatus;
    @SerializedName("isAllowed")
    @Expose
    private String isAllowed;
    @SerializedName("iCalendarID")
    @Expose
    private String iCalendarID;
    @SerializedName("isWaiting")
    @Expose
    private String isWaiting;
    @SerializedName("slot")
    @Expose
    private String slot;
    @SerializedName("vDate")
    @Expose
    private String vDate;
    @SerializedName("dAppDate")
    @Expose
    private Object dAppDate;
    @SerializedName("isDel")
    @Expose
    private String isDel;
    @SerializedName("iTWS")
    @Expose
    private String iTWS;
    @SerializedName("vUserName")
    @Expose
    private Object vUserName;
    @SerializedName("dInsertDT")
    @Expose
    private Object dInsertDT;
    @SerializedName("IsDisabled")
    @Expose
    private String isDisabled;
    @SerializedName("vDay")
    @Expose
    private String vDay;

    public String getAppID() {
        return appID;
    }

    public void setAppID(String appID) {
        this.appID = appID;
    }

    public String getIPatID() {
        return iPatID;
    }

    public void setIPatID(String iPatID) {
        this.iPatID = iPatID;
    }

    public Object getVPatientName() {
        return vPatientName;
    }

    public void setVPatientName(Object vPatientName) {
        this.vPatientName = vPatientName;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getVStartTime() {
        return vStartTime;
    }

    public void setVStartTime(String vStartTime) {
        this.vStartTime = vStartTime;
    }

    public Object getVEndTime() {
        return vEndTime;
    }

    public void setVEndTime(Object vEndTime) {
        this.vEndTime = vEndTime;
    }

    public Object getVCell() {
        return vCell;
    }

    public void setVCell(Object vCell) {
        this.vCell = vCell;
    }

    public Object getVNotes() {
        return vNotes;
    }

    public void setVNotes(Object vNotes) {
        this.vNotes = vNotes;
    }

    public Object getVMrNo() {
        return vMrNo;
    }

    public void setVMrNo(Object vMrNo) {
        this.vMrNo = vMrNo;
    }

    public Object getVAge() {
        return vAge;
    }

    public void setVAge(Object vAge) {
        this.vAge = vAge;
    }

    public String getSlotTime() {
        return slotTime;
    }

    public void setSlotTime(String slotTime) {
        this.slotTime = slotTime;
    }

    public String getSlotEndTime() {
        return slotEndTime;
    }

    public void setSlotEndTime(String slotEndTime) {
        this.slotEndTime = slotEndTime;
    }

    public String getSlotGroup() {
        return slotGroup;
    }

    public void setSlotGroup(String slotGroup) {
        this.slotGroup = slotGroup;
    }

    public String getISlotID() {
        return iSlotID;
    }

    public void setISlotID(String iSlotID) {
        this.iSlotID = iSlotID;
    }

    public String getISlotStatus() {
        return iSlotStatus;
    }

    public void setISlotStatus(String iSlotStatus) {
        this.iSlotStatus = iSlotStatus;
    }

    public String getIsAllowed() {
        return isAllowed;
    }

    public void setIsAllowed(String isAllowed) {
        this.isAllowed = isAllowed;
    }

    public String getICalendarID() {
        return iCalendarID;
    }

    public void setICalendarID(String iCalendarID) {
        this.iCalendarID = iCalendarID;
    }

    public String getIsWaiting() {
        return isWaiting;
    }

    public void setIsWaiting(String isWaiting) {
        this.isWaiting = isWaiting;
    }

    public String getSlot() {
        return slot;
    }

    public void setSlot(String slot) {
        this.slot = slot;
    }

    public String getVDate() {
        return vDate;
    }

    public void setVDate(String vDate) {
        this.vDate = vDate;
    }

    public Object getDAppDate() {
        return dAppDate;
    }

    public void setDAppDate(Object dAppDate) {
        this.dAppDate = dAppDate;
    }

    public String getIsDel() {
        return isDel;
    }

    public void setIsDel(String isDel) {
        this.isDel = isDel;
    }

    public String getITWS() {
        return iTWS;
    }

    public void setITWS(String iTWS) {
        this.iTWS = iTWS;
    }

    public Object getVUserName() {
        return vUserName;
    }

    public void setVUserName(Object vUserName) {
        this.vUserName = vUserName;
    }

    public Object getDInsertDT() {
        return dInsertDT;
    }

    public void setDInsertDT(Object dInsertDT) {
        this.dInsertDT = dInsertDT;
    }

    public String getIsDisabled() {
        return isDisabled;
    }

    public void setIsDisabled(String isDisabled) {
        this.isDisabled = isDisabled;
    }

    public String getVDay() {
        return vDay;
    }

    public void setVDay(String vDay) {
        this.vDay = vDay;
    }



    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Response createFromParcel(Parcel in) {
            return new Response(in);
        }

        public Response[] newArray(int size) {
            return new Response[size];
        }
    };




}
