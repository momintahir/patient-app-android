package com.softech.ccpatient.data.models.models

import com.google.gson.annotations.SerializedName

class PatientAppointments {

    @SerializedName("iAppID")
    val iAppID: String? = null
    @SerializedName("AppDate")
    val appDate: String? = null
    @SerializedName("AppStatus")
    val appStatus: String? = null
    @SerializedName("vAppTimeFrom")
    val vAppTimeFrom: String? = null
    @SerializedName("vNotes")
    val vNotes: String? = null
    @SerializedName("iConsultID")
    val iConsultID: String? = null
    @SerializedName("Consultant")
    val consultant: String? = null
    @SerializedName("Clinic")
    val clinic: String? = null
    @SerializedName("DateStr")
    val dateStr: String? = null
    @SerializedName("ClinicID")
    val clinicID: String? = null
    @SerializedName("FileName")
    val fileName: String? = null
    @SerializedName("ImageUrl")
    val imageUrl: String? = null

//  For Past Appointment
    @SerializedName("counterDate")
    val counterDate: String? = null
    @SerializedName("PrescriptionLink")
    val prescriptionLink: String? = null


}
