package com.softech.ccpatient.data.models;

/**
 * Created by Robin.Yaqoob on 06-Sep-17.
 */

public class HospitalsList {

    private String name;


    public HospitalsList(String name) {
        this.name = name;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
