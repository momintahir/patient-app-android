package com.softech.ccpatient.data.models.models

import com.google.gson.annotations.SerializedName

class LoadDocuments {

    @SerializedName("OID")
    val oId: String? = null
    @SerializedName("FOLDERNAME")
    val folderName: String? = null
    @SerializedName("FOLDERCOLOR")
    val folderColor: String? = null
    @SerializedName("IPATID")
    val iPatID: String? = null
    @SerializedName("B_SYSTEM")
    val bSystem: String? = null
    @SerializedName("CREATEDBY")
    val createdBy: String? = null
    @SerializedName("Total")
    val total: String? = null

    override fun toString(): String {
        return "LoadDocuments(oId=$oId, folderName=$folderName, folderColor=$folderColor, iPatID=$iPatID, bSystem=$bSystem, createdBy=$createdBy, total=$total)"
    }


}