
package com.softech.ccpatient.data.models.models.PatientList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Response {

    @SerializedName("AppDate")
    @Expose
    private String appDate;
    @SerializedName("vAppTimeFrom")
    @Expose
    private String vAppTimeFrom;
    @SerializedName("ConId")
    @Expose
    private Integer conId;
    @SerializedName("OrgId")
    @Expose
    private Integer orgId;
    @SerializedName("Patient")
    @Expose
    private String patient;
    @SerializedName("AppId")
    @Expose
    private Integer appId;
    @SerializedName("PatId")
    @Expose
    private Integer patId;
    @SerializedName("MrNo")
    @Expose
    private String mrNo;
    @SerializedName("Mobile")
    @Expose
    private String mobile;
    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("LastName")
    @Expose
    private String lastName;
    @SerializedName("Gender")
    @Expose
    private String gender;
    @SerializedName("City")
    @Expose
    private String city;
    @SerializedName("Avatar")
    @Expose
    private String avatar;
    @SerializedName("PicAddress")
    @Expose
    private String picAddress;

    public String getAppDate() {
        return appDate;
    }

    public void setAppDate(String appDate) {
        this.appDate = appDate;
    }

    public String getVAppTimeFrom() {
        return vAppTimeFrom;
    }

    public void setVAppTimeFrom(String vAppTimeFrom) {
        this.vAppTimeFrom = vAppTimeFrom;
    }

    public Integer getConId() {
        return conId;
    }

    public void setConId(Integer conId) {
        this.conId = conId;
    }

    public Integer getOrgId() {
        return orgId;
    }

    public void setOrgId(Integer orgId) {
        this.orgId = orgId;
    }

    public String getPatient() {
        return patient;
    }

    public void setPatient(String patient) {
        this.patient = patient;
    }

    public Integer getAppId() {
        return appId;
    }

    public void setAppId(Integer appId) {
        this.appId = appId;
    }

    public Integer getPatId() {
        return patId;
    }

    public void setPatId(Integer patId) {
        this.patId = patId;
    }

    public String getMrNo() {
        return mrNo;
    }

    public void setMrNo(String mrNo) {
        this.mrNo = mrNo;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getPicAddress() {
        return picAddress;
    }

    public void setPicAddress(String picAddress) {
        this.picAddress = picAddress;
    }

}
