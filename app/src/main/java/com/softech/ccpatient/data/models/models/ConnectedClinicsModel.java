package com.softech.ccpatient.data.models.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Robin.Yaqoob on 14-Dec-17.
 */

public class ConnectedClinicsModel {

    @SerializedName("OID")
    private String OID;
    @SerializedName("ClinicName")
    private String ClinicName;
    @SerializedName("IsCalendarExist")
    private String CalendarExist;

//    public ConnectedClinicsModel(String OID, String clinicName) {
//        this.OID = OID;
//        ClinicName = clinicName;
//    }

    public String getOID() {
        return OID;
    }

    public String getClinicName() {
        return ClinicName;
    }

    public String getCalendarExist() {
        return CalendarExist;
    }
}
