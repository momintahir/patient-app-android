
package com.softech.ccpatient.data.models.models.CalendarByDate;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Response implements Parcelable {

    @SerializedName("ID")
    @Expose
    private Object iD;
    @SerializedName("CalendarID")
    @Expose
    private String calendarID;
    @SerializedName("ClinicName")
    @Expose
    private String clinicName;
    @SerializedName("CalendarName")
    @Expose
    private String calendarName;
    @SerializedName("StartDate")
    @Expose
    private String startDate;
    @SerializedName("EndDate")
    @Expose
    private String endDate;
    @SerializedName("ConsultId")
    @Expose
    private String consultId;
    @SerializedName("HospitalId")
    @Expose
    private String hospitalId;
    @SerializedName("Status")
    @Expose
    private Integer status;
    @SerializedName("ConsultantName")
    @Expose
    private String consultantName;
    @SerializedName("DayIntervals")
    @Expose
    private List<DayInterval> dayIntervals = null;

    public Object getID() {
        return iD;
    }

    public void setID(Object iD) {
        this.iD = iD;
    }

    public String getCalendarID() {
        return calendarID;
    }

    public void setCalendarID(String calendarID) {
        this.calendarID = calendarID;
    }

    public String getClinicName() {
        return clinicName;
    }

    public void setClinicName(String clinicName) {
        this.clinicName = clinicName;
    }

    public String getCalendarName() {
        return calendarName;
    }

    public void setCalendarName(String calendarName) {
        this.calendarName = calendarName;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getConsultId() {
        return consultId;
    }

    public void setConsultId(String consultId) {
        this.consultId = consultId;
    }

    public String getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(String hospitalId) {
        this.hospitalId = hospitalId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getConsultantName() {
        return consultantName;
    }

    public void setConsultantName(String consultantName) {
        this.consultantName = consultantName;
    }

    public List<DayInterval> getDayIntervals() {
        return dayIntervals;
    }

    public void setDayIntervals(List<DayInterval> dayIntervals) {
        this.dayIntervals = dayIntervals;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Response createFromParcel(Parcel in) {
            return new Response(in);
        }

        public Response[] newArray(int size) {
            return new Response[size];
        }
    };

    public Response(Parcel in) {
        this.iD = in.readString();
        this.calendarID = in.readString();
        this.clinicName = in.readString();
        this.startDate = in.readString();
        this.endDate = in.readString();
        this.consultId = in.readString();
        this.hospitalId = in.readString();
        this.status = in.readInt();
        this.consultantName = in.readString();

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(String.valueOf(iD));
        dest.writeString(calendarID);
        dest.writeString(clinicName);
        dest.writeString(startDate);
        dest.writeString(endDate);
        dest.writeString(consultId);
        dest.writeString(hospitalId);
        dest.writeString(hospitalId);
        dest.writeString(String.valueOf(status));
        dest.writeString(consultantName);
//        dest.writeString(dayIntervals);

    }
}
