package com.softech.ccpatient.data.models.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Robin.Yaqoob on 30-Nov-17.
 */

public class ClinicsModels {

    @SerializedName("OID")
    private String OID;
    @SerializedName("ClinicName")
    private String ClinicName;
    @SerializedName("Speciality")
    private String Speciality;
    @SerializedName("Address")
    private String Address;
    @SerializedName("City")
    private String City;
    @SerializedName("Phone")
    private String Phone;

    public String getOID() {
        return OID;
    }

    public String getClinicName() {
        return ClinicName;
    }

    public String getSpeciality() {
        return Speciality;
    }

    public String getAddress() {
        return Address;
    }

    public String getCity() {
        return City;
    }

    public String getPhone() {
        return Phone;
    }
}




