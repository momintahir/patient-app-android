
package com.softech.ccpatient.data.models.models.DocumentFolder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Response {

    @SerializedName("OID")
    @Expose
    private Integer oID;
    @SerializedName("FOLDERNAME")
    @Expose
    private String fOLDERNAME;
    @SerializedName("FOLDERCOLOR")
    @Expose
    private String fOLDERCOLOR;
    @SerializedName("IPATID")
    @Expose
    private Integer iPATID;
    @SerializedName("B_SYSTEM")
    @Expose
    private Integer bSYSTEM;
    @SerializedName("CREATEDBY")
    @Expose
    private Integer cREATEDBY;
    @SerializedName("Total")
    @Expose
    private Integer total;

    public Integer getOID() {
        return oID;
    }

    public void setOID(Integer oID) {
        this.oID = oID;
    }

    public String getFOLDERNAME() {
        return fOLDERNAME;
    }

    public void setFOLDERNAME(String fOLDERNAME) {
        this.fOLDERNAME = fOLDERNAME;
    }

    public String getFOLDERCOLOR() {
        return fOLDERCOLOR;
    }

    public void setFOLDERCOLOR(String fOLDERCOLOR) {
        this.fOLDERCOLOR = fOLDERCOLOR;
    }

    public Integer getIPATID() {
        return iPATID;
    }

    public void setIPATID(Integer iPATID) {
        this.iPATID = iPATID;
    }

    public Integer getBSYSTEM() {
        return bSYSTEM;
    }

    public void setBSYSTEM(Integer bSYSTEM) {
        this.bSYSTEM = bSYSTEM;
    }

    public Integer getCREATEDBY() {
        return cREATEDBY;
    }

    public void setCREATEDBY(Integer cREATEDBY) {
        this.cREATEDBY = cREATEDBY;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

}
