package com.softech.ccpatient.data.models.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Robin.Yaqoob on 27-Nov-17.
 */

public class SpecialitiesModel {

    @SerializedName("OID")
    private String OID;
    @SerializedName("SpecialityDesc")
    private String SpecialityDesc;

    public SpecialitiesModel(String OID, String specialityDesc) {
        this.OID = OID;
        SpecialityDesc = specialityDesc;
    }

    public String getOID() {
        return OID;
    }

    public String getSpecialityDesc() {
        return SpecialityDesc;
    }

    @Override
    public String toString() {
        return SpecialityDesc;
    }

}
