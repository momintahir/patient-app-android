package com.softech.ccpatient.data.models.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Robin.Yaqoob on 18-Dec-17.
 */

public class SearchAppointmentModel {


    @SerializedName("AppDate")
    private String AppDate;
    @SerializedName("vAppTimeFrom")
    private String vAppTimeFrom;
    @SerializedName("Patient")
    private String Patient;
    @SerializedName("AppId")
    private String AppId;
    @SerializedName("PatId")
    private String PatId;
    @SerializedName("MrNo")
    private String MrNo;
    @SerializedName("Mobile")
    private String Mobile;
    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("LastName")
    @Expose
    private String lastName;
    @SerializedName("Gender")
    @Expose
    private String gender;
    @SerializedName("City")
    @Expose
    private String city;
    @SerializedName("Avatar")
    @Expose
    private String avatar;
    @SerializedName("PicAddress")
    @Expose
    private String picAddress;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getPicAddress() {
        return picAddress;
    }

    public void setPicAddress(String picAddress) {
        this.picAddress = picAddress;
    }

    public String getAppDate() {
        return AppDate;
    }

    public String getvAppTimeFrom() {
        return vAppTimeFrom;
    }

    public String getPatient() {
        return Patient;
    }

    public String getAppId() {
        return AppId;
    }

    public String getPatId() {
        return PatId;
    }

    public String getMrNo() {
        return MrNo;
    }

    public String getMobile() {
        return Mobile;
    }
}
