package com.softech.ccpatient.app;

import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.multidex.MultiDex;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.softech.ccpatient.sharedpreferences.FetchData;
import com.softech.ccpatient.utils.LruBitmapCache;
import com.softech.ccpatient.webrtc.SocketIO;
import com.stripe.android.PaymentConfiguration;

//import android.support.multidexdex.MultiDex;


public class AppController extends Application implements LifecycleObserver {

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static final String TAG = AppController.class
            .getSimpleName();

    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    private static AppController mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        PaymentConfiguration.init(
                getApplicationContext(),
                "pk_test_8tozyiXNwGQSYV2VUZ9VvJ4100mLNxIoWK"
        );



    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }


    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public void onAppBackgrounded() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            SocketIO.Companion.getInstance().didEnterBackground(FetchData.getData(this, "userId"));
        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    public void onAppForegrounded() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            SocketIO.Companion.getInstance().didEnterForeground(FetchData.getData(this, "userId"));
        }

    }
}
