package com.softech.ccpatient.utils;

/**
 * Created by Robin.Yaqoob on 16-Nov-17.
 */

public interface CallBacks {

    void yes();
    void no();

}
