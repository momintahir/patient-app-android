package com.softech.ccpatient.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import android.media.MediaCodec;
import android.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AESEncryption {
    public static String encrypt(String clearText, String secretKey, String initVector) {
        try {
            byte[] bytePass = secretKey.getBytes("utf-8");
            byte[] byteV = initVector.getBytes("utf-8");
            byte[] byteKey = Arrays.copyOf(bytePass, 32);
            byte[] byteIV = Arrays.copyOf(byteV, 16);
// System.out.println(DatatypeConverter.printHexBinary(byteKey));
// System.out.println(DatatypeConverter.printHexBinary(byteIV));
            SecretKeySpec skeySpec = new SecretKeySpec(byteKey, "AES");
            IvParameterSpec ivSpec = new IvParameterSpec(byteIV);
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivSpec);
            byte[] byteText = clearText.getBytes("utf-8");
            byte[] buf = cipher.doFinal(byteText);
            byte[] byteBase64 = Base64.encode(buf,Base64.DEFAULT);
            //  byte[] byteBase64 = Base64.getEncoder().encode(buf);
            String data = new String(byteBase64);
            return data;
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }

    public static void encryptFile(int encryptionMode, File inputFile, String secretKey, String initVector, File outputFile) throws MediaCodec.CryptoException {
        try {
            byte[] bytePass = secretKey.getBytes("utf-8");
            byte[] byteV = initVector.getBytes("utf-8");
            byte[] byteKey = Arrays.copyOf(bytePass, 32);
            byte[] byteIV = Arrays.copyOf(byteV, 16);
// System.out.println(DatatypeConverter.printHexBinary(byteKey));
// System.out.println(DatatypeConverter.printHexBinary(byteIV));
            SecretKeySpec skeySpec = new SecretKeySpec(byteKey, "AES");
            IvParameterSpec ivSpec = new IvParameterSpec(byteIV);
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
            cipher.init(encryptionMode, skeySpec, ivSpec);
            FileInputStream inputStream = new FileInputStream(inputFile);
            byte[] inputBytes = new byte[(int) inputFile.length()];
            inputStream.read(inputBytes);
            byte[] outputBytes = cipher.doFinal(inputBytes);
            FileOutputStream outputStream = new FileOutputStream(outputFile);
            outputStream.write(outputBytes);
            inputStream.close();
            outputStream.close();
        } catch (NoSuchPaddingException | NoSuchAlgorithmException
                | InvalidKeyException | BadPaddingException
                | IllegalBlockSizeException | IOException ex) {
            throw new MediaCodec.CryptoException(100, ex.toString());

        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }
    }

    public static String decrypt(String data, String secretKey, String initVector) {
        try {
            //byte[] byteData = Base64.getDecoder().decode(data.getBytes("utf-8"));
            byte[] byteData = Base64.decode(data,Base64.DEFAULT);
            byte[] bytePass = secretKey.getBytes("utf-8");
            byte[] byteV = initVector.getBytes("utf-8");
            byte[] byteKey = Arrays.copyOf(bytePass, 32);
            byte[] byteIV = Arrays.copyOf(byteV, 16);
            SecretKeySpec skeySpec = new SecretKeySpec(byteKey, "AES");
            IvParameterSpec ivSpec = new IvParameterSpec(byteIV);
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivSpec);
            byte[] byteText = cipher.doFinal(byteData);
            String clearText = new String(byteText);
            return clearText;
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }
}