package com.softech.ccpatient.utils;


import com.softech.ccpatient.BuildConfig;

public class Const {

    //public static final String BASE_URL = "https://www.healthcarepakistan.pk/ClinicConnectServiceStage/";
   // public static final String BASE_URL ="  https://www.clinic.nexthealth.pk/ClinicConnectService/";
    // public static final String BASE_URL = "https://www.healthcarepakistan.pk/ClinicConnectService/";
    //public static final String BASE_URL = "http://192.168.0.176/iPhoneServices/";
   // public static final String BASE_URL ="https://www.healthcarepakistan.pk/ClinicConnectServiceStageSecure/";

    public static final String googleDirectionBase_url = "https://maps.googleapis.com/maps/api/";
    public static final String BASE_URL = BuildConfig.HOST;
    public static final String LOGIN = "SignIn/SignInPatientMobile";
    public static final String FORGOTPASSWORD = "SignUp/ForgetPasswordRequest";
    public static final String SIGNUP = "SignUp/PatientSignUp";
    public static final String ADD_PATIENT_PAYMENT_SOURCE = "SignUp/AddPatientPaymentSource";
    public static final String CHANGEPASSWORD = "Consultant/UpdatePatientPassword";
    public static final String GETSPECIALITIES = "SignUp/GetSpecialities";
    public static final String ADDNEWCLINIC = "SignUp/AddClinic";
    public static final String SEND_ONLINE_STATUS = "Consultant/SendOnlineStatus";
    public static final String GETPROFILE = "Consultant/GetPatientProfile";
    public static final String UPDATEPROFILE = "Consultant/UpdatePatientProfile";
    public static final String UPDATEPROFILEIMAGE = "Consultant/UpdatePatientProfilePicture";
    public static final String VIEWAPPOINTMENTS = "Appointment/MyAppointments";
    public static final String VIEWPASTAPPOINTMENTS = "Appointment/PastAppointments";
    public static final String LOGOUT = "SignIn/SignOutMobile";

    public static final String ACTION_GETPATIENTPROFILE = "Consultant/GetPatientProfile";
    public static final String Documents = "Consultant/LoadFolders";
    public static final String ConsultantCalendars = "Consultant/GetCurrentDateCalendarSlots";
    public static final String PatientCalendar = "Appointment/GetCalendarSlots";
    public static final String BookAppointment = "Appointment/BookAppointment";


    public static final String LoadFoldersForPatient = "Consultant/LoadFoldersForPatientApp";
    public static final String LoadSelectedFolderForPatien = "Consultant/LoadSelectedFolderForPatientApp";

    public static final String SelectedDocuments = "Consultant/LoadSelectedFolder";
    public static final String CLINCSLIST = "SignUp/ViewClinics";
    public static final String CLINICREQUEST = "Consultant/ConnectClinicRequest";
    public static final String SEARCH = "Consultant/SearchPatientsOnly";
    public static final String SEARCHPatientByClinic = "Consultant/PatientsListByClinicId";
//    public static final String SEARCHConsultantByNameAndCity = "Appointment/GetConsultantByNameAndCity";
    public static final String SEARCHConsultantByNameAndCity = "Appointment/GetConsultantByNameAndCity";

    public static final String ADDCALANDER = "Consultant/ConsultantCalendar";
    public static final String VIEWCALENDER = "Consultant/ViewConsultantCalendar";
    public static final String VIEWAPPOINTMENTDETAIL = "Consultant/ViewConsultantCalendar";
    public static final String VIEW_CLINICS = "Consultant/SelectClinic";

    public static final String SELECT_CLINIC = "Consultant/SelectClinic";
    public static final String UPDATETOKEN = "Consultant/UpdateDeviceId";
    public static final String SEARCH_PATIENT = "Consultant/SearchPatient";
    public static final String UPLOAD_DOCUMENT = "Consultant/UploadDocumentFromPatientApp";
    public static final String ACTION_BOOK_APPOINTMENT = "Appointment/AddAppointment";
    public static final String ACTION_DELETE_APPOINTMENT = "Appointment/DeleteAppointmentByPatient";


    public static final String FOLDER_ID = "folderId";
    public static final String FILE_PATH= "filePath";

    public static final int PICK_IMAGE= 1001;
    public static final int PICK_AUDIO= 1002;
    public static final int PICK_VIDEO= 1003;
    public static final int PICK_DOC= 1004;
    public static final String Encryption_Key = "S*FTECHCL!N!CC*NNECTCRYPT*GRAPHY";
    public static final String Encryption_IV = "1234567890123456";
}
