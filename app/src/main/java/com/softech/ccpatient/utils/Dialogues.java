package com.softech.ccpatient.utils;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Build;
import androidx.appcompat.app.AlertDialog;

/**
 * Created by Robin.Yaqoob on 08-Dec-17.
 */

public class Dialogues {

    public static  void showOkDialogue(Activity activity, String message)
    {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(activity);
//        builder1.setTitle("Title");
        builder1.setMessage(message);
        builder1.setCancelable(true);
        builder1.setPositiveButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    public static void yesnoDialogue(Activity activity, String message, final CallBacks callBacks)
    {


        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(activity);
        } else {
            builder = new AlertDialog.Builder(activity);
        }
        builder .setMessage(message)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        callBacks.yes();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        callBacks.no();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();

    }

}
