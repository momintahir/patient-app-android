package com.softech.ccpatient.utils;

import android.os.Handler;

import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat;

public class ProgressBarAnimation {

    AnimatedVectorDrawableCompat avdProgress;
    Handler handler;
    Runnable run;

    public ProgressBarAnimation(AnimatedVectorDrawableCompat avdProgress) {
        this.avdProgress = avdProgress;
    }

    public void repeatAnimation() {
        handler = new Handler();
        run = new Runnable() {
            @Override
            public void run() {

                avdProgress.start();
                //repeat this task devery 1s
                handler.postDelayed(this,500);
            }
        };
        handler.post(run);
    }

    public void stopProgressAnimation(){

        handler.removeCallbacks(run);
    }

}
