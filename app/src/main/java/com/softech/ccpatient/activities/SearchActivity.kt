package com.softech.ccpatient.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import android.widget.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.softech.ccpatient.R
import com.softech.ccpatient.adapters.SearchAdapter
import com.softech.ccpatient.apiconnections.ApiConnection
import com.softech.ccpatient.data.models.models.CliniclistModel
import com.softech.ccpatient.data.models.models.SearchAppointmentNew
import com.softech.ccpatient.data.models.models.SpecialitiesModel
import com.softech.ccpatient.interfaces.IWebListener
import com.softech.ccpatient.sharedpreferences.FetchData
import com.softech.ccpatient.sharedpreferences.SaveData
import com.softech.ccpatient.utils.Const
import com.softech.ccpatient.utils.Globals
import com.softech.ccpatient.utils.Logs
import kotlinx.android.synthetic.main.activity_search.*
import org.json.JSONException
import org.json.JSONObject
import java.util.ArrayList
import java.util.HashMap

class SearchActivity : AppCompatActivity() {

    var clinicID = ""

    var appointmentsModelArrayList = ArrayList<SearchAppointmentNew>()
    private var mAdapter: SearchAdapter? = null
    var specialitiesModelList= ArrayList<SpecialitiesModel>()
    var speciality=""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)



        getSpecialities()
        getAllConsultants()
        val gson = Gson()
        val type = object : TypeToken<ArrayList<CliniclistModel>>() {

        }.type
        val ClinicList = FetchData.getData(this@SearchActivity, "cliniclist")
        val connectedClinicsModelArrayList = gson.fromJson<ArrayList<CliniclistModel>>(ClinicList.toString(), type)

        val adapter = ArrayAdapter(this@SearchActivity, R.layout.spinner, connectedClinicsModelArrayList)
        adapter.setDropDownViewResource(R.layout.spinner)


        spinnerSpeciality.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {

                speciality= specialitiesModelList[pos].specialityDesc
                if (speciality == "all"){
                    speciality="\"\"";
                }

            }

            override fun onNothingSelected(parent: AdapterView<out Adapter>?) {

            }
        }



        ivBack.setOnClickListener {
            finish()
        }
        btnSearch.setOnClickListener {
//            if (etSearch.getText().toString().isNotEmpty()) {

                appointmentsModelArrayList.clear()
                mAdapter?.notifyDataSetChanged()
                getConsultantByNameAndCity(etSearch.getText().toString())

                Globals.hideKeyboard(this@SearchActivity)
//            } else {
//                Toast.makeText(this@SearchActivity, "Please enter consultant name", Toast.LENGTH_SHORT).show()
//            }
        }

    }



    private fun getConsultantByNameAndCity(text: String) {
        val params = HashMap<String, String>()
//        params["action"] = "Login"
        params["consultantName"] = text
        params["consultantCity"] = "\"\""
        params["speciality"] = speciality
        params["country"] = "\"\""

        val apiConnection = ApiConnection(object : IWebListener {
            override fun success(response: String?) {

                Log.e("response","response: "+response)
                if (response != null) {
                    try {
                        val jsonObject = JSONObject(response)
                        val meta = jsonObject.getJSONObject("meta")
                        val code = meta.getString("code")
                        if (Integer.valueOf(code) == 200) {

                            val jsonArray = jsonObject.getJSONArray("response")
                            val gson = Gson()
                            val type = object : TypeToken<ArrayList<SearchAppointmentNew>>() {
                            }.type
                            appointmentsModelArrayList = gson.fromJson(jsonArray.toString(), type)

                            mAdapter = SearchAdapter(appointmentsModelArrayList, this@SearchActivity)
                            val mLayoutManager = LinearLayoutManager(this@SearchActivity)
                            rvSearch.setLayoutManager(mLayoutManager)
                            rvSearch.setItemAnimator(DefaultItemAnimator())
                            rvSearch.setAdapter(mAdapter)
                            rvSearch.addItemDecoration(DividerItemDecoration(this@SearchActivity, DividerItemDecoration.VERTICAL))
                            SaveData.SaveData(this@SearchActivity, "searchData", jsonArray.toString())
                        } else if (Integer.valueOf(code) == 504) {
                            appointmentsModelArrayList.clear()
                            val message = meta.getString("message")
                            Toast.makeText(this@SearchActivity, message, Toast.LENGTH_LONG).show()
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    Logs.showLog("Login", response)
                }
            }

            override fun error(response: String) {
                Log.d(response, response)
            }
        }, this@SearchActivity, params,
                Const.SEARCHConsultantByNameAndCity)
        apiConnection.makeStringReq()

    }


    fun getSpecialities() {
        val params: Map<String, String> = HashMap()
        val apiConnection = ApiConnection(object : IWebListener {
            override fun success(response: String?) {
                if (response != null) {
                    try {
                        val jsonObject = JSONObject(response)
                        val meta = jsonObject.getJSONObject("meta")
                        val code = meta.getString("code")
                        if (Integer.valueOf(code) == 200) {
                            val jsonResponse = jsonObject.getJSONArray("response")
                            val gson = Gson()
                            val type = object : TypeToken<ArrayList<SpecialitiesModel?>?>() {}.type
                            specialitiesModelList = gson.fromJson<ArrayList<SpecialitiesModel>>(jsonResponse.toString(), type)
                            specialitiesModelList.add(0,SpecialitiesModel("0","all"))

                            Logs.showLog("Login", response)
                            val adapter: ArrayAdapter<SpecialitiesModel> = ArrayAdapter<SpecialitiesModel>(applicationContext, R.layout.spinner, specialitiesModelList)
                            adapter.setDropDownViewResource(R.layout.spinner)
                            spinnerSpeciality.adapter = adapter
                            spinnerSpeciality.prompt = "Select Site"
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                    Logs.showLog("Login", response)
                }
            }

            override fun error(response: String?) {
            }
        }, this@SearchActivity, params, Const.GETSPECIALITIES)
        apiConnection.makeStringReq()
    }

    private fun getAllConsultants() {
        val params = HashMap<String, String>()
//        params["action"] = "Login"
        params["consultantName"] = "\"\""
        params["consultantCity"] = "\"\""
        params["speciality"] = "\"\""
        params["country"] = "\"\""

        val apiConnection = ApiConnection(object : IWebListener {
            override fun success(response: String?) {

                Log.e("response","response: "+response)
                if (response != null) {
                    try {
                        val jsonObject = JSONObject(response)
                        val meta = jsonObject.getJSONObject("meta")
                        val code = meta.getString("code")
                        if (Integer.valueOf(code) == 200) {

                            val jsonArray = jsonObject.getJSONArray("response")
                            val gson = Gson()
                            val type = object : TypeToken<ArrayList<SearchAppointmentNew>>() {
                            }.type
                            appointmentsModelArrayList = gson.fromJson(jsonArray.toString(), type)

                            mAdapter = SearchAdapter(appointmentsModelArrayList, this@SearchActivity)
                            val mLayoutManager = LinearLayoutManager(this@SearchActivity)
                            rvSearch.setLayoutManager(mLayoutManager)
                            rvSearch.setItemAnimator(DefaultItemAnimator())
                            rvSearch.setAdapter(mAdapter)
                            rvSearch.addItemDecoration(DividerItemDecoration(this@SearchActivity, DividerItemDecoration.VERTICAL))
                            SaveData.SaveData(this@SearchActivity, "searchData", jsonArray.toString())
                        } else if (Integer.valueOf(code) == 504) {
                            appointmentsModelArrayList.clear()
                            val message = meta.getString("message")
                            Toast.makeText(this@SearchActivity, message, Toast.LENGTH_LONG).show()
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    Logs.showLog("Login", response)
                }
            }

            override fun error(response: String) {
                Log.d(response, response)
            }
        }, this@SearchActivity, params,
                Const.SEARCHConsultantByNameAndCity)
        apiConnection.makeStringReq()

    }



}
