package com.softech.ccpatient.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.softech.ccpatient.R;
import com.softech.ccpatient.adapters.ClinicsAdapter;
import com.softech.ccpatient.apiconnections.ApiConnection;
import com.softech.ccpatient.data.models.models.ClinicsModels;
import com.softech.ccpatient.interfaces.Callbacks;
import com.softech.ccpatient.interfaces.IWebListener;
import com.softech.ccpatient.sharedpreferences.FetchData;
import com.softech.ccpatient.utils.Const;
import com.softech.ccpatient.utils.Dialogues;
import com.softech.ccpatient.utils.Logs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ClinicListActivity extends AppCompatActivity implements IWebListener {

    @BindView(R.id.recyclViewClinics)
    RecyclerView recyclViewClinics;
    ArrayList<ClinicsModels> clinicsModelsArrayList = new ArrayList<ClinicsModels>();
    private ClinicsAdapter mAdapter;
    @BindView(R.id.editTextSearch)
    EditText editTextSearch;
    private String clinicId;
    @BindView(R.id.buttonSave)
    Button buttonSave;
    @BindView(R.id.buttonClinicNotFound)
    Button buttonClinicNotFound;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clinic_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Choose a clinic");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClinicListActivity.this.finish();
            }
        });

        ButterKnife.bind(this);

        getClinics();

        buttonClinicNotFound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ClinicListActivity.this, AddNewClinicActivity.class));

            }
        });
        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // filter your list from your input
                filter(s.toString());
                //you can use runnable postDelayed like 500 ms to delay search text
            }
        });


        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addClinic();

            }
        });

    }

    void filter(String text) {
        ArrayList<ClinicsModels> temp = new ArrayList();
        for (ClinicsModels d : clinicsModelsArrayList) {
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if (d.getClinicName().contains(text)) {
                temp.add(d);
            }
        }
        //update recyclerview
        mAdapter.updateList(temp);
    }


    private void addClinic() {
        String consultantID = FetchData.getData(ClinicListActivity.this, "userId");
        String access_token = FetchData.getData(ClinicListActivity.this, "access_token");

        Map<String, String> params = new HashMap<String, String>();
        params.put("action", "Connect Clinic Request");
        params.put("consultantId", consultantID);
        params.put("access_token", access_token);
        params.put("clinicId", clinicId);


        ApiConnection apiConnection = new ApiConnection(new IWebListener() {
            @Override
            public void success(String response) {

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject meta = jsonObject.getJSONObject("meta");
                        String code = meta.getString("code");
                        if (Integer.valueOf(code) == 200) {

                            String responseString = jsonObject.getString("response");
                            //  Toast.makeText(ClinicListActivity.this, responseString, Toast.LENGTH_LONG).show();
                            Dialogues.showOkDialogue(ClinicListActivity.this, responseString);

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Logs.showLog("Login", response);
                }
                Log.d("Clinic Request", response);
            }

            @Override
            public void error(String response) {

            }
        },
                ClinicListActivity.this, params, Const.CLINICREQUEST);
        apiConnection.makeStringReq();

    }

    private void getClinics() {
        String consultantID = FetchData.getData(ClinicListActivity.this, "userId");
        String access_token = FetchData.getData(ClinicListActivity.this, "access_token");

        Map<String, String> params = new HashMap<String, String>();
        params.put("action", "List Of Consultant Appointments");
        params.put("consultantId", consultantID);
        params.put("access_token", access_token);

        ApiConnection apiConnection = new ApiConnection(ClinicListActivity.this,
                ClinicListActivity.this, params, Const.CLINCSLIST);
        apiConnection.makeStringReq();

    }


    @Override
    public void success(String response) {
        if (response != null) {

            try {

                JSONObject jsonObject = new JSONObject(response);
                JSONObject meta = jsonObject.getJSONObject("meta");
                String code = meta.getString("code");
                if (Integer.valueOf(code) == 200) {

                    JSONObject responseObj = jsonObject.getJSONObject("response");
                    JSONArray Clinics = responseObj.getJSONArray("Clinics");

                    Gson gson = new Gson();
                    Type type = new TypeToken<ArrayList<ClinicsModels>>() {
                    }.getType();


                    clinicsModelsArrayList = gson.fromJson(Clinics.toString(), type);

                    mAdapter = new ClinicsAdapter(clinicsModelsArrayList, new Callbacks() {
                        @Override
                        public void data(String data) {

                            clinicId = data;
                        }
                    });
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                    recyclViewClinics.setLayoutManager(mLayoutManager);
                    recyclViewClinics.setItemAnimator(new DefaultItemAnimator());
                    recyclViewClinics.setAdapter(mAdapter);
                    // mAdapter.notifyDataSetChanged();
                    recyclViewClinics.addItemDecoration(new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL));

                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

            Logs.showLog("Login", response);
        }
    }

    @Override
    public void error(String response) {

    }


}
