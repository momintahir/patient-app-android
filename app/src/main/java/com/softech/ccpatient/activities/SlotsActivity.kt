package com.softech.ccpatient.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import android.util.Log
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.ArrayAdapter
import android.widget.Toast

import com.google.gson.Gson
import com.softech.ccpatient.R
import com.softech.ccpatient.adapters.SlotsAdapter
import com.softech.ccpatient.apiconnections.ApiConnection
import com.softech.ccpatient.data.models.models.SlotsData.SlotsData
import com.softech.ccpatient.interfaces.IWebListener
import com.softech.ccpatient.sharedpreferences.FetchData
import com.softech.ccpatient.utils.Const

import org.json.JSONException
import org.json.JSONObject

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.HashMap

import kotlinx.android.synthetic.main.activity_slots.*
import kotlinx.android.synthetic.main.content_date_selection.*

class SlotsActivity : AppCompatActivity() {

    var slotsData: SlotsData? = null
    var currentDated: Date? = null
    var slotsAdapter: SlotsAdapter? = null
    var adapter: ArrayAdapter<String>? = null
    var consultantId = ""
    var organizationId = ""
    var currentDate = ""
    var clinicName=""
    var fee=""
    var appointmentId=""
    var consultantName=""
    var isRescheduled=false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_slots)

        val slidedown = AnimationUtils.loadAnimation(this, R.anim.slide_down)
        val slideup = AnimationUtils.loadAnimation(this, R.anim.slide_up)

        ivBack.setOnClickListener {
            finish()
        }
        val b = intent.extras
        if (intent.extras != null) {
            isRescheduled= b?.getBoolean("isRescheduled",false)!!
            if (isRescheduled){
                consultantId = b?.getString("ConsultantId").toString()
                organizationId = b?.getString("OrganizationId").toString()
                clinicName=b?.getString("clinicName").toString()
                appointmentId=b?.getString("appointmentId").toString()

            }
            else{
                consultantId = b?.getString("ConsultantId").toString()
                consultantName = b?.getString("consultantName").toString()
                organizationId = b?.getString("OrganizationId").toString()
                clinicName=b?.getString("clinicName").toString()
                fee=b?.getString("fee").toString()
            }

        }

        tvDateLabel.text = getCurrentDate(Calendar.getInstance().time)

        llDate.setOnClickListener {
            if (rvCalander.visibility == View.GONE) {
                rvCalander.visibility = View.VISIBLE
                simpleCalandar.startAnimation(slidedown)
            } else {
                rvCalander.visibility = View.GONE
                slidingLayout.startAnimation(slideup)
            }
        }

        ivLeftArrow.setOnClickListener {
            currentDate = FetchData.getPreviousOrNextDate(currentDate, -1)
            var sdf = SimpleDateFormat("MMM dd, yyyy")
            val newDate = sdf.format(FetchData.dateFromString(currentDate))
            tvDateLabel.text = newDate
            val date=FetchData.dateFromString(currentDate)
            sdf= SimpleDateFormat("EEEE")
            tvDay.text = sdf.format(date)
            try {
                simpleCalandar.setDate(SimpleDateFormat("dd/MM/yyyy").parse(currentDate).time, true, true)
                callingDaysSlotService()
            } catch (e: ParseException) {
                e.printStackTrace()
            }
        }
        ivRightArrow.setOnClickListener {
            currentDate = FetchData.getPreviousOrNextDate(currentDate, 1)
            var sdf = SimpleDateFormat("MMM dd, yyyy")
            val newDate = sdf.format(FetchData.dateFromString(currentDate))
            tvDateLabel.text = newDate
            val date=FetchData.dateFromString(currentDate)
            sdf= SimpleDateFormat("EEEE")
            tvDay.text = sdf.format(date)
            try {
                simpleCalandar.setDate(SimpleDateFormat("dd/MM/yyyy").parse(currentDate).time, true, true)
                callingDaysSlotService()
            } catch (e: ParseException) {
                e.printStackTrace()
            }
        }
        simpleCalandar.setOnDateChangeListener { view, year, month, dayOfMonth ->
            currentDate = String.format("%d/%d/%d", dayOfMonth, month + 1, year)
            val sdf = SimpleDateFormat("dd/MM/yyyy")
            val sdf1 = SimpleDateFormat("MMM dd, yyyy")
            currentDate = sdf.format(FetchData.dateFromString(currentDate))
            val newDate = sdf1.format(FetchData.dateFromString(currentDate))
            tvDateLabel.text = newDate
            callingDaysSlotService()


        }

        callingDaysSlotService()
    }

    fun getCurrentDate(date: Date): String {
        println("Current time => $date")
        val df = SimpleDateFormat("dd/MM/yyyy")
        var df1 = SimpleDateFormat("MMM dd, yyyy")
        val newDate = df1.format(date)
        currentDate = df.format(date)
        currentDated = FetchData.dateFromString(currentDate)
        df1= SimpleDateFormat("EEEE")
        tvDay.text = df1.format(date)
        return newDate
    }

    private fun callingDaysSlotService() {
        val params = HashMap<String, String>()
        params["action"] = "Consultant - selected date slots"
        params["vDate"] = currentDate
        params["vConsultantCode"] = consultantId
        params["OrgId"] = organizationId

        val apiConnection = ApiConnection(object : IWebListener {
            override fun success(response: String?) {

                Log.e("response", "response: " + response)
                if (response != null) {

                    try {
                        val jsonObject = JSONObject(response)
                        val meta = jsonObject.getJSONObject("meta")
                        val code = meta.getString("code")
                        if (Integer.valueOf(code) == 200) {
                            slotsData = Gson().fromJson(response, SlotsData::class.java)
                            val mLayoutManager = GridLayoutManager(this@SlotsActivity, 3)
                            rvSlots.itemAnimator = DefaultItemAnimator()

                            rvCalander.visibility = View.GONE
                            if(slotsData!!.response.size>0){
                                rvSlots.visibility = View.VISIBLE
                            }else{
                                rvSlots.visibility = View.GONE
                            }

                            slotsAdapter = SlotsAdapter(this@SlotsActivity,consultantName,appointmentId, isRescheduled,clinicName, slotsData!!.response, consultantId, organizationId, currentDate,fee)
                            rvSlots.layoutManager = mLayoutManager
                            rvSlots.adapter = slotsAdapter

                        } else if (Integer.valueOf(code) == 504) {
                            val message = meta.getString("message")
                            if (slotsAdapter != null)
                                slotsAdapter!!.notifyDataSetChanged()
                            Toast.makeText(this@SlotsActivity, message, Toast.LENGTH_LONG).show()

                            rvCalander.visibility = View.GONE
                            rvSlots.visibility = View.GONE

                        } else if (Integer.valueOf(code) == 499) {
                            val message = meta.getString("message")
                            if (slotsData != null) {
                                slotsData!!.response.clear()
                                if (slotsAdapter != null)
                                    slotsAdapter!!.notifyDataSetChanged()
                            }
                            Toast.makeText(this@SlotsActivity, message, Toast.LENGTH_LONG).show()
                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                }
            }

            override fun error(response: String) {
                Log.d(response, response)
            }
        }, this@SlotsActivity, params,
                Const.PatientCalendar)
        apiConnection.makeStringReq()

    }

}

