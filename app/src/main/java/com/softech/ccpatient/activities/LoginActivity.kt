package com.softech.ccpatient.activities

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.text.InputType
import android.text.SpannableString
import android.text.Spanned
import android.text.method.HideReturnsTransformationMethod
import android.text.method.LinkMovementMethod
import android.text.method.PasswordTransformationMethod
import android.text.style.ClickableSpan
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.iid.FirebaseInstanceId
import com.softech.ccpatient.R
import com.softech.ccpatient.apiconnections.ApiConnection
import com.softech.ccpatient.interfaces.IWebListener
import com.softech.ccpatient.sharedpreferences.FetchData
import com.softech.ccpatient.sharedpreferences.SaveData
import com.softech.ccpatient.utils.AESEncryption
import com.softech.ccpatient.utils.Const
import com.softech.ccpatient.webrtc.util.AppConstants
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONException
import org.json.JSONObject
import java.util.*
import java.util.regex.Pattern

@Suppress("NAME_SHADOWING")
class LoginActivity : AppCompatActivity(), IWebListener {

    private var fcmToken = ""
    private var isPasswordVisible = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        //  etUserName.setText("htkhan@hotmail.com");
//        etUserName.setText("user@user.com");
//        etPassword.setText("1234567");

//                etUserName.setText("saim.shafqat93@gmail.com");
//                etPassword.setText("13579");

//        etUserName.setText("saad@softech.com")
//        etPassword.setText("123")


        etPassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.eye_cancel, 0);


        etPassword.setOnTouchListener(View.OnTouchListener { v, event ->
            val DRAWABLE_RIGHT = 2;
            val DRAWABLE_BOTTOM = 3;

            if (event.action == MotionEvent.ACTION_UP) {
                if (event.rawX >= (etPassword.right - etPassword.compoundDrawables[DRAWABLE_RIGHT].bounds.width())) {
                    // your action here
                    Log.d("Rann", "Asd")
                    togglePassVisability()
                    return@OnTouchListener true;
                }
            }
            return@OnTouchListener false;
        })


        tvTerms.setOnClickListener {
            val intent = Intent(this@LoginActivity, WebviewActivity::class.java)
            intent.putExtra("url", "https://www.healthcarepakistan.pk/terms-clinic.php")
            startActivity(intent)
        }

        tvCondition.setOnClickListener {
            val intent = Intent(this@LoginActivity, WebviewActivity::class.java)
            intent.putExtra("url", "https://www.healthcarepakistan.pk/terms-clinic.php")
            startActivity(intent)
        }
        tvPolicy.setOnClickListener {
            val intent = Intent(this@LoginActivity, WebviewActivity::class.java)
            intent.putExtra("url", "https://www.healthcarepakistan.pk/privacy-clinic.php")
            startActivity(intent)
        }


        val exists = FetchData.getData(this@LoginActivity, "exists")
        if (exists == "true") {
            val userId = FetchData.getData(this@LoginActivity, "userId")
            val Name = FetchData.getData(this@LoginActivity, "Name")
            val IsDoctor = FetchData.getData(this@LoginActivity, "IsDoctor")
            val access_token = FetchData.getData(this@LoginActivity, "access_token")

            if (userId.length > 0) {


                val intent = Intent(this@LoginActivity, TabsActivity::class.java)
                intent.putExtra("userId", userId)
                intent.putExtra("firstName", Name)
                intent.putExtra("IsDoctor", IsDoctor)
                intent.putExtra("access_token", access_token)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
                this.finish()
            }
        }

        tvForgotPassword.setOnClickListener {
            startActivity(Intent(this@LoginActivity, ForgetPasswordActivity::class.java))
        }

        btnLogin.setOnClickListener {
            fcmToken = FirebaseInstanceId.getInstance().token!!

            if (etPassword.text.toString().isNotEmpty() && etUserName.text.toString().isNotEmpty()) {
                if (isValidEmaillId(etUserName.text.toString())) {
                    val params = HashMap<String, String>()
                    params["Email"] = etUserName.text.toString()
                    params["password"] = etPassword.text.toString()
                    params["Device_type"] = "android"
                    params["consultantDeviceId"] = fcmToken!!
//                    val apiConnection = ApiConnection(this@LoginActivity, this@LoginActivity, params, Const.LOGIN)
//                    apiConnection.makeStringReq()
                    val jsonObject = JSONObject(params as Map<*, *>)
                    try {
                        val encrypt = AESEncryption.encrypt(jsonObject.toString(), Const.Encryption_Key, Const.Encryption_IV)
                        val finalParams = HashMap<String, String>()
                        finalParams["encryptObj"] = encrypt
                        val apiConnection = ApiConnection(this@LoginActivity, this@LoginActivity, finalParams, Const.LOGIN)
                        apiConnection.makeStringReq()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                } else {
                    Toast.makeText(this@LoginActivity, "Please add correct Email address.", Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(this@LoginActivity, "Please fill username and password.", Toast.LENGTH_SHORT).show()
            }
        }

        val termsOfServicesClick = object : ClickableSpan() {
            override fun onClick(view: View) {
                startActivity(Intent(this@LoginActivity, SignUpActivity::class.java))
            }
        }

        val privacyPolicyClick = object : ClickableSpan() {
            override fun onClick(view: View) {
                // Toast.makeText(getApplicationContext(), "Privacy Policy Clicked", Toast.LENGTH_SHORT).show();
            }
        }
        makeLinks(tvSignUp, arrayOf("Sign Up"), arrayOf(termsOfServicesClick, privacyPolicyClick))
    }

    private fun togglePassVisability() {
        if (isPasswordVisible) {
            var pass = etPassword.text.toString();
            etPassword.transformationMethod = PasswordTransformationMethod.getInstance();
            etPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD)
            etPassword.setText(pass);
            etPassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.eye_cancel, 0);

            etPassword.setSelection(pass.length);
        } else {
            var pass = etPassword.text.toString();
            etPassword.transformationMethod = HideReturnsTransformationMethod.getInstance();
            etPassword.inputType = InputType.TYPE_CLASS_TEXT;
            etPassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.eye, 0);

            etPassword.setText(pass);
            etPassword.setSelection(pass.length);
        }
        isPasswordVisible = !isPasswordVisible;
    }


    private fun isValidEmaillId(email: String): Boolean {

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches()
    }

    fun makeLinks(textView: TextView, links: Array<String>, clickableSpans: Array<ClickableSpan>) {
        val spannableString = SpannableString(textView.text)
        for (i in links.indices) {
            val clickableSpan = clickableSpans[i]
            val link = links[i]

            val startIndexOfLink = textView.text.toString().indexOf(link)
            spannableString.setSpan(clickableSpan, startIndexOfLink, startIndexOfLink + link.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        }
        textView.movementMethod = LinkMovementMethod.getInstance()
        textView.setText(spannableString, TextView.BufferType.SPANNABLE)
    }

    override fun success(response: String?) {

        if (response != null) {
            try {
                val jsonObject = JSONObject(response)
                val meta = jsonObject.getJSONObject("meta")
                val code = meta.getString("code")
                if (Integer.valueOf(code) == 200) {

//                    val decryptedString = AESEncryption.decrypt(jsonObject.getString("response"), Const.Encryption_Key, Const.Encryption_IV)
//                    val jsonResponse = JSONObject(decryptedString)

                    val jsonResponse = jsonObject.getJSONObject("response")
                    val userId = jsonResponse.getString("userId")
                    val mrn = jsonResponse.getString("MRN")
                    val firstName = jsonResponse.getString("FirstName")
                    val lastName = jsonResponse.getString("LastName")
                    val dob = jsonResponse.getString("DOB")
                    val gender = jsonResponse.getString("Gender")
                    val address = jsonResponse.getString("Address")
                    val city = jsonResponse.getString("City")
                    val country = jsonResponse.getString("Country")
                    val emailAddress = jsonResponse.getString("EmailAddress")
                    val cellNumber = jsonResponse.getString("CellNumber")
                    val twoFACode = jsonResponse.getString("TwoFACode")
                    Log.e("LoginResponse", "Response :$jsonResponse")





                    val sp = applicationContext.getSharedPreferences("appData", 0)
                    val editor: SharedPreferences.Editor
                    editor = sp.edit()
                    editor.putString("userId", userId)
                    editor.putString("mrn", mrn)
                    editor.putString("firstName", firstName)
                    editor.putString("lastName", lastName)
                    editor.putString("dob", dob)
                    editor.putString("gender", gender)
                    editor.putString("address", address)
                    editor.putString("city", city)
                    editor.putString("country", country)
                    editor.putString("emailAddress", emailAddress)
                    editor.putString("cellNumber", cellNumber)
                    editor.putString("twoFACode", twoFACode)
                    editor.putString(AppConstants.PUSH_TOKEN, fcmToken)
                    editor.apply()
                    editor.apply()


                    if (cellNumber.equals("")) {
                        val intent = Intent(this@LoginActivity, ProfileActivity::class.java)
                        intent.putExtra("firstName", firstName)
                        intent.putExtra("lastName", lastName)
                        intent.putExtra("email", emailAddress)
                        startActivity(intent)
                    } else {
                        val intent = Intent(this@LoginActivity, TabsActivity::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                        SaveData.SaveData(this@LoginActivity, "exists", "true")
                        startActivity(intent)
                        this.finish()
                    }


                } else if (Integer.valueOf(code) == 504) {
//                    val jsonResponse = jsonObject.getJSONObject("response")
//                    val Name = jsonResponse.getString("Name")
//                    val intent1 = IHntent(this@LoginActivity, WelcomeActivity::class.java)
//                    intent1.putExtra("name", Name)
//                    intent1.putExtra("welcome", "Thank you for sending request to the Clinic")
//                    intent1.putExtra("team", "Your request for the clinic request has not approved yet. Please wait.")
//                    startActivity(intent1)
//                    this@LoginActivity.finish()
                } else if (Integer.valueOf(code) == 402) {
                    Toast.makeText(this, "Wrong username or password.", Toast.LENGTH_SHORT).show()
                } else if (Integer.valueOf(code) == 404) {
                    Toast.makeText(this, jsonObject.getString("error"), Toast.LENGTH_SHORT).show()
                }

            } catch (e: JSONException) {
                e.printStackTrace()
            }

//            Logs.showLog("Login", response)
        }

    }

    override fun error(response: String?) {
        if (response != null) {
//            Logs.showLog("Login", response)
        }
    }

}
