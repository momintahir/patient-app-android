package com.softech.ccpatient.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.softech.ccpatient.R;
import com.softech.ccpatient.apiconnections.ApiConnection;
import com.softech.ccpatient.interfaces.IWebListener;
import com.softech.ccpatient.sharedpreferences.FetchData;
import com.softech.ccpatient.utils.Const;
import com.softech.ccpatient.utils.Logs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChangePasswordActivity extends AppCompatActivity implements IWebListener {

    @BindView(R.id.editTextCurrentPassword)
    EditText editTextCurrentPassword;
    @BindView(R.id.editTextNewPassword)
    EditText editTextNewPassword;
    @BindView(R.id.editTextConfirmPassword)
    EditText editTextConfirmPassword;

    @BindView(R.id.buttonChangePassword)
    Button buttonChangePassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Change Password");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangePasswordActivity.this.finish();
            }
        });

        ButterKnife.bind(this);


        buttonChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(editTextCurrentPassword.getText().toString().length()>0 &&
                        editTextNewPassword.getText().toString().length()>6 &&
                        editTextConfirmPassword.getText().toString().length()>6 ) {
                    if(editTextNewPassword.getText().toString().equals(editTextConfirmPassword.getText().toString())){

                    changePassword();

                    }
                    else{
                        Toast.makeText(ChangePasswordActivity.this, "New Password Doesn't match.", Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    Toast.makeText(ChangePasswordActivity.this, "Please fill all of the fields", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }


    private void changePassword() {

        SharedPreferences shared = getSharedPreferences("appData", MODE_PRIVATE);
        String email = shared.getString("emailAddress", "");

        String consultantID = FetchData.getData(ChangePasswordActivity.this, "userId");
        String access_token = FetchData.getData(ChangePasswordActivity.this, "access_token");

        String newPassword=editTextNewPassword.getText().toString();
        Map<String, String> params = new HashMap<String, String>();
        params.put("Email", email);
        params.put("newPassword", newPassword);


        ApiConnection apiConnection = new ApiConnection(ChangePasswordActivity.this, ChangePasswordActivity.this, params,
                Const.CHANGEPASSWORD);
        apiConnection.makeStringReq();
    }

    @Override
    public void success(String response) {
        if(response !=null) {

            try {

                JSONObject jsonObject = new JSONObject(response);
                JSONObject meta = jsonObject.getJSONObject("meta");
                String code = meta.getString("code");
                if(Integer.valueOf(code)== 200)
                {
                    Toast.makeText(this, "You have changed your password.", Toast.LENGTH_SHORT).show();
                    ChangePasswordActivity.this.finish();
                }



            } catch (JSONException e) {
                e.printStackTrace();
            }

            Logs.showLog("Login", response);
        }
    }

    @Override
    public void error(String response) {
        Toast.makeText(this, "You have changed your password.", Toast.LENGTH_SHORT).show();

    }
}
