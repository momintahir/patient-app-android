package com.softech.ccpatient.activities;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.softech.ccpatient.apiconnections.ApiConnectionBackground1;
import com.softech.ccpatient.interfaces.IWebListener;
import com.softech.ccpatient.sharedpreferences.FetchData;
import com.softech.ccpatient.utils.Const;
import com.softech.ccpatient.utils.Logs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Robin.Yaqoob on 09-Aug-17.
 */

public class MyInstanceIDListenerService extends FirebaseMessagingService {



    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is also called
     * when the InstanceID token is initially generated, so this is where
     * you retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onNewToken(String s)  {
        super.onNewToken(s);
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("RTS Customer", "Refreshed token: " + refreshedToken);
        getProfile( refreshedToken);
        // TODO: Implement this method to send any registration to your app's servers.
     }
    private void getProfile(String deviceToken) {
        Map<String, String> params = new HashMap<String, String>();
        String consultantID = FetchData.getData(this, "userId");


        params.put("action", "Consultant Profile");
        params.put("consultantId", consultantID);
        params.put("consultantDeviceId", deviceToken);
        params.put("consultantDeviceType", "Android");


        ApiConnectionBackground1 apiConnection = new ApiConnectionBackground1(new IWebListener() {
            @Override
            public void success(String response) {

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject meta = jsonObject.getJSONObject("meta");
                        String code = meta.getString("code");
                        if (Integer.valueOf(code) == 200) {

                        }


                    } catch(JSONException e){
                        e.printStackTrace();
                    }

                    Logs.showLog("Login", response);
                }
            }

            @Override
            public void error (String response){

            }
        },

                this,params, Const.UPDATETOKEN);
        apiConnection.makeStringReq();

    }
}
