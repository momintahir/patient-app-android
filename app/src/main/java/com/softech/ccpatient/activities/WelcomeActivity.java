package com.softech.ccpatient.activities;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.softech.ccpatient.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WelcomeActivity extends AppCompatActivity {

    @BindView(R.id.textViewDrWelcome)
    TextView textViewDrWelcome;
    @BindView(R.id.textViewThankYou)
    TextView textViewThankYou;
    @BindView(R.id.textViewTechnicalTeam)
    TextView textViewTechnicalTeam;
    @BindView(R.id.buttonOk)
    Button buttonOk;

    String name, welcome, team;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Welcome to Clinic Connect");

        Bundle intent = getIntent().getExtras();
        if (intent != null) {

            welcome = intent.getString("welcome", "");
            name = intent.getString("name", "");
            team = intent.getString("team", "");
        }

        textViewDrWelcome.setText("Hi Dr. " + name);
        textViewThankYou.setText(welcome);
        textViewTechnicalTeam.setText(team);

        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                WelcomeActivity.this.finish();

            }
        });
    }

}
