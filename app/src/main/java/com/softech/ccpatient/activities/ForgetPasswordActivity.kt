package com.softech.ccpatient.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.text.TextUtils
import android.util.Patterns
import android.view.View
import android.widget.Toast
import com.softech.ccpatient.R

import com.softech.ccpatient.apiconnections.ApiConnection
import com.softech.ccpatient.interfaces.IWebListener
import com.softech.ccpatient.utils.Const
import com.softech.ccpatient.utils.Dialogues
import com.softech.ccpatient.utils.Logs

import org.json.JSONException
import org.json.JSONObject

import java.util.HashMap
import java.util.regex.Pattern

import kotlinx.android.synthetic.main.activity_forget_password.*

class ForgetPasswordActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forget_password)
        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)

        supportActionBar!!.title = "Forgot Password"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        toolbar.setNavigationOnClickListener { this@ForgetPasswordActivity.finish() }


        btnForgotPass.setOnClickListener {
            if (etEmail.text.toString().isNotEmpty()) {
                val emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
                val email = etEmail.text.toString().trim { it <= ' ' }
                //val isValidEmaillId = isValidEmaillId(email)
                if (email.matches(emailPattern.toRegex())) {
                    forgetPassword()
                } else {
                    Toast.makeText(this@ForgetPasswordActivity, "Please add correct Email address.", Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(this@ForgetPasswordActivity, "Please add Email address.", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun isValidEmaillId(email: String): Boolean {

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches()
    }

    private fun forgetPassword() {
        val params = HashMap<String, String>()
        params["action"] = "Forget Password"
        params["email"] = etEmail.text.toString()

        val apiConnection = ApiConnection(object : IWebListener {
            override fun success(response: String?) {
                if (response != null) {

                    try {
                        val jsonObject = JSONObject(response)
                        val meta = jsonObject.getJSONObject("meta")
                        val code = meta.getString("code")
                        if (Integer.valueOf(code) == 200) {
                            Dialogues.showOkDialogue(this@ForgetPasswordActivity, "We have sent you an email in response to your request to reset your password. Please check your inbox.")
                        }
                        if (Integer.valueOf(code) == 504) {
                            Dialogues.showOkDialogue(this@ForgetPasswordActivity,
                                    "Email not Exist.")
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    Logs.showLog("Login", response)
                }
            }

            override fun error(response: String) {

            }
        }, this@ForgetPasswordActivity, params,
                Const.FORGOTPASSWORD)
        apiConnection.makeStringReq()
    }

    companion object {
        fun isValidEmail(target: CharSequence): Boolean {
            return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches()
        }
    }
}
