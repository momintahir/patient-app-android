package com.softech.ccpatient.activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.softech.ccpatient.R;
import com.softech.ccpatient.apiconnections.ApiConnection;
import com.softech.ccpatient.interfaces.IWebListener;
import com.softech.ccpatient.sharedpreferences.FetchData;
import com.softech.ccpatient.sharedpreferences.SaveData;
import com.softech.ccpatient.utils.AESEncryption;
import com.softech.ccpatient.utils.Const;
import com.softech.ccpatient.utils.Logs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileActivity extends AppCompatActivity implements IWebListener {

    @BindView(R.id.fieldFirstName)
    EditText editTextFirstName;
    @BindView(R.id.fieldLastName)
    EditText editTextLastName;
//    @BindView(R.id.editTextEmail)
//    EditText editTextEmail;
    @BindView(R.id.editTextPhoneNumber)
    EditText editTextPhoneNumber;
    @BindView(R.id.textViewDOB)
    EditText textViewDOB;
    @BindView(R.id.editTextAddress)
    EditText editTextAddress;
    @BindView(R.id.editTextCity)
    EditText editTextCity;
    @BindView(R.id.editTextCountry)
    EditText editTextCountry;
//    @BindView(R.id.editTextQualification)
//    EditText editTextQualification;
    @BindView(R.id.buttonProfileSignup)
    Button buttonSignUp;
//    @BindView(R.id.spinnerSpeciality)
//    Spinner spinnerSpeciality;
    String FirstName, LastName, Email;
    final Calendar myCalendar = Calendar.getInstance();
    final Calendar myCalendar1 = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Complete Your Profile");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileActivity.this.finish();
            }
        });

        ButterKnife.bind(this);

        Bundle intent = getIntent().getExtras();
        if (intent != null) {

            FirstName = intent.getString("firstName", "");
            LastName = intent.getString("lastName", "");
            Email = intent.getString("email", "");
//            Password = intent.getString("password","");
            editTextFirstName.setText(FirstName);
            editTextLastName.setText(LastName);
//            editTextEmail.setText(Email);

        }

        final String userId= FetchData.getData(this, "userId");
        //getSpecialities();


        textViewDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DatePickerDialog dialog =  new DatePickerDialog(ProfileActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                dialog.show();
            }
        });


        buttonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String phoneNumber=editTextPhoneNumber.getText().toString();
                if (phoneNumber.length() > 0  &&
//                        spinnerSpeciality.getSelectedItem().toString().length() > 0 &&
//                        editTextQualification.getText().toString().length() > 0 &&
                        editTextAddress.getText().toString().length() > 0 &&
                        editTextCity.getText().toString().length() > 0 &&
                        !textViewDOB.getText().toString().isEmpty())
                {
                    if( phoneNumber.length() == 16){
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("action", "Create New User");
                        //    params.put("ImageFile", "");
                        params.put("vFirstName", editTextFirstName.getText().toString());
                        params.put("vLastName", editTextLastName.getText().toString());
                        params.put("dDOB", textViewDOB.getText().toString());
                        params.put("IPatID", userId);
                        params.put("vCellNumber", phoneNumber);
                        params.put("vAddress", editTextAddress.getText().toString());
                        params.put("vCity", editTextCity.getText().toString());
                        params.put("vCountry", "PK");
//                    params.put("email", editTextEmail.getText().toString());
//                    params.put("password", Password);
//                    params.put("qualification", editTextQualification.getText().toString());

                        JSONObject jsonObject = new JSONObject(params);
                        try {
                            //   SecretKeySpec newKey = new SecretKeySpec("S*FTECHCL!N!CC*NNECTCRYPT*GRAPHY".getBytes(), "AES");
                            String encrypt = AESEncryption.encrypt(jsonObject.toString(), Const.Encryption_Key, Const.Encryption_IV);
                            Log.d("dsasda", encrypt);
                            Map<String, String> finalParams = new HashMap<String, String>();
                            finalParams.put("encryptObj", encrypt);
                            ApiConnection apiConnection = new ApiConnection(ProfileActivity.this, ProfileActivity.this, finalParams, Const.UPDATEPROFILE);
                            apiConnection.makeStringReq();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    else
                    {
                        Toast.makeText(ProfileActivity.this, "Please enter number in correct format", Toast.LENGTH_SHORT).show();
                    }

                }
                else {

                    Toast.makeText(ProfileActivity.this, "Please fill in all fields", Toast.LENGTH_SHORT).show();
                }
            }
        });

        editTextPhoneNumber.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(i ==11)
                {
                    String input = editTextPhoneNumber.getText().toString();
                    final String number = input.replaceFirst("(\\d{2})(\\d{3})(\\d+)", "(+$1)$2-$3");//(+92)XXX-XXXXXXX
                    System.out.println(number);
                    editTextPhoneNumber.setText(number);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }

    };

    private void updateLabel() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        textViewDOB.setText(sdf.format(myCalendar.getTime()));
    }

    private void getSpecialities() {
        Map<String, String> params = new HashMap<String, String>();

        ApiConnection apiConnection = new ApiConnection(new IWebListener() {
            @Override
            public void success(String response) {

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject meta = jsonObject.getJSONObject("meta");
                        String code = meta.getString("code");
                        if (Integer.valueOf(code) == 200) {
                            JSONArray jsonResponse = jsonObject.getJSONArray("response");
                            Gson gson = new Gson();
//                            Type type = new TypeToken<ArrayList<SpecialitiesModel>>() {
//                            }.getType();
//                            List<SpecialitiesModel> specialitiesModelList = gson.fromJson(jsonResponse.toString(), type);
//                            Logs.showLog("Login", response);
//
//                            ArrayAdapter<SpecialitiesModel> adapter =
//                                    new ArrayAdapter<SpecialitiesModel>(ProfileActivity.this, R.layout.spinner, specialitiesModelList);
//                            adapter.setDropDownViewResource(R.layout.spinner);
//                            spinnerSpeciality.setAdapter(adapter);
//                            spinnerSpeciality.setPrompt("Select Site");

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Logs.showLog("Login", response);
                }
            }

            @Override
            public void error(String response) {

            }
        }, ProfileActivity.this, params, Const.GETSPECIALITIES);
        apiConnection.makeStringReq();

    }


    @Override
    public void success(String response) {
        if (response != null) {

            try {

                JSONObject jsonObject = new JSONObject(response);
                JSONObject meta = jsonObject.getJSONObject("meta");
                String code = meta.getString("code");
                if (Integer.valueOf(code) == 200) {
                    SharedPreferences sp = getApplicationContext()
                            .getSharedPreferences("appData", 0);
                    SharedPreferences.Editor editor;
                    editor = sp.edit();
                    editor.putString("cellNumber", editTextPhoneNumber.getText().toString());
                    editor.apply();
                    SaveData.SaveData(ProfileActivity.this, "exists", "true");
                    Intent intent = new Intent(ProfileActivity.this, TabsActivity.class);
                  //  intent.putExtra("email", Email);
                   // intent.putExtra("password", Password);
                  //  intent.putExtra("name", FirstName + " " + LastName);
                   // intent.putExtra("welcome", "Thank you for registering with Clinic Connect");
                   // intent.putExtra("team", "Our Technical team will contact you within 24 hours to confirm your details.");

                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                    startActivity(intent);

                    ProfileActivity.this.finish();


                } else if (Integer.valueOf(code) == 404) {


                    String message = meta.getString("message");

                    Toast.makeText(ProfileActivity.this, message, Toast.LENGTH_LONG).show();
                    //  recyclViewSearch.setVisibility(View.GONE);
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

            Logs.showLog("Login", response);
        }
    }

    @Override
    public void error(String response) {

        Toast.makeText(this, response, Toast.LENGTH_SHORT).show();
    }
}
