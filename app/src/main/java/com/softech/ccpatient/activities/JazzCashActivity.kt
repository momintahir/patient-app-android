package com.softech.ccpatient.activities

import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.os.StrictMode
import android.util.Log
import android.webkit.WebResourceRequest
import android.webkit.WebResourceResponse
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.softech.ccpatient.R
import com.softech.ccpatient.apiconnections.ApiConnection
import com.softech.ccpatient.interfaces.IWebListener
import com.softech.ccpatient.sharedpreferences.FetchData
import com.softech.ccpatient.sharedpreferences.SaveData
import com.softech.ccpatient.utils.Const
import kotlinx.android.synthetic.main.activity_jazz_cash.*
import okhttp3.Headers
import okhttp3.OkHttpClient
import okhttp3.Request
import org.apache.http.util.EncodingUtils
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.util.*
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec


class JazzCashActivity : AppCompatActivity() {
    var paramsArray: Array<HashMap<String, String>>? = null
    var encryptedString: String = ""
    var dateString: String = ""
    var expireDateString: String = ""
    var txnRefNo: String = ""
    var userid: String = ""
    var doctorId: String = ""
    var practiceId: String = ""
    var appointementTime: String = ""
    var fee = ""
    var flag: Boolean = false;
    var consultantId: String = ""
    var clincID: String = ""
    var date: String = ""
    var timeStart: String = ""
    var timeEnd: String = ""
    var slotID: String = ""
    var calendarID: String = ""
    var feeJazz = ""
    var feeJazzInt: Int = 0
    var counter = 0
    var clinicName = ""
    var consultantName = ""
    var firstName=""
    var lastName=""
    var myDate=""
    var myTimeStart=""
    var shouldAllowBack=false
    private var pDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_jazz_cash)
//        setSupportActionBar(jazzToolBar)

        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        val prefs = getSharedPreferences("appData", 0);
        firstName = prefs.getString ("firstName", "").toString();
        lastName = prefs.getString ("lastName", "").toString();

        val intent = intent.extras
        if (intent != null) {
            consultantId = intent.getString("consultantId", "")
            clincID = intent.getString("clincID", "")
            date = intent.getString("date", "")
            myDate = date
            date = date.replace(("[^\\w\\d ]").toRegex(), "")
            timeStart = intent.getString("TimeStart").toString()
            myTimeStart = timeStart
            timeStart = timeStart.replace(("[^\\w\\d ]").toRegex(), "")
            timeEnd = intent.getString("TimeEnd").toString()
            slotID = intent.getString("SlotID", "")
            calendarID = intent.getString("CalendarID", "")
            fee = intent.getString("fee", "")
            consultantName = intent.getString("consultantName", "")
            consultantName = consultantName.replace(("[^\\w\\d ]").toRegex(), "")
            clinicName = intent.getString("clinicName", "")
            clinicName = clinicName.replace(("[^\\w\\d ]").toRegex(), "")
            feeJazzInt = Integer.parseInt(fee);

            feeJazzInt *= 100
            feeJazz = feeJazzInt.toString()
//            patId = intent.getString("patId", "")


        }

        practiceId = FetchData.getData(this@JazzCashActivity, "practice_Id")!!
        userid = FetchData.getData(this@JazzCashActivity, "user_id")!!
        dateString = getCurrentTimeForPaymentForm(0)
        expireDateString = getCurrentTimeForPaymentForm(2)
        txnRefNo = "TXN" + getCurrentTimeForPaymentForm(0)
//            SaveData.saveBoolean(this@JazzCashActivity,"isPaymentDone",true)
        SaveData.SaveData(this@JazzCashActivity, "txnRefNo", txnRefNo)
        generateEncryptedKeyFromParams()
        postRequestViewWebView()


//        supportActionBar?.title = "Jazz Cash"
//        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
//        supportActionBar!!.setDisplayShowHomeEnabled(true)


//        ivBack.setOnClickListener {
//            finish()
//        }

    }

    fun getCurrentTimeForPaymentForm(daysAdded: Int): String {
        val tz = TimeZone.getTimeZone("GMT+05")
        val c = Calendar.getInstance(tz)
        c.add(Calendar.HOUR, daysAdded)
        val time = String.format("%02d", c.get(Calendar.YEAR)) + "" +
                String.format("%02d", c.get(Calendar.MONTH) + 1) + "" +
                String.format("%02d", c.get(Calendar.DATE)) + "" +
                String.format("%02d", c.get(Calendar.HOUR_OF_DAY)) + "" +
                String.format("%02d", c.get(Calendar.MINUTE)) + "" +
                String.format("%02d", c.get(Calendar.SECOND))
        return time
    }

    fun postRequestViewWebView() {

//        val finalUrlString = "pp_Amount=" + cashAmountField_activity.text + "00" +
//                "&pp_BillReference=OrderID&pp_Description=abc" +
//                "&pp_DiscountedAmount=&pp_DiscountBank=&" +
//                "pp_Language=EN&pp_MerchantID=MC9082&pp_Password=y09svu31t4" +
//                "&pp_SubMerchantID=&pp_TxnCurrency=PKR&pp_TxnDateTime=" + dateString +
//                "&pp_TxnExpiryDateTime=" + expireDateString +
//                "&pp_TxnRefNo=" + txnRefNo +
//                "&pp_TxnType=&pp_Version=1.1&pp_SecureHash=" + encryptedString +
//                "&ppmpf_5=platform_android" +
//                "&ppmpf_4="+userid+
//                "&ppmpf_3="+practiceId+
//                "&pp_ReturnURL=https://www.healthcarepakistan.pk/Patient_trainingSecure/Appointment/PaymentResponse"


        val finalUrlString = "pp_Version=" + "1.1" +
                "&pp_Amount=" + feeJazz +
                "&pp_TxnType=" +
                "&pp_Language=EN" +
                "&pp_MerchantID=00168541" +
                "&pp_SubMerchantID=" +
                "&pp_Password=e16t803g0u" +
                "&pp_BankID=&" +
                "pp_ProductID=" +
                "&pp_TxnRefNo=" + txnRefNo +
                "&pp_TxnCurrency=PKR" +
                "&pp_TxnDateTime=" + dateString +
                "&pp_BillReference=billRef" +
                "&pp_Description=Appointment booking with Dr Consultant"+" "+consultantName + "at" +" Clinic"+clinicName+" on" +" "+date +" at"+" "+timeStart +
//                "&pp_Description=Appointment booking with" +
                "&pp_TxnExpiryDateTime=" + expireDateString +
//                "&pp_ReturnURL=https://www.healthcarepakistan.pk/Patient/home/index" +
                "&pp_ReturnURL=https://www.healthcarepakistan.pk/Patient/Appointment/PaymentResponse" +
                "&pp_SecureHash=" + encryptedString +
                "&ppmpf_1=android" +
                "&ppmpf_2=2" +
                "&ppmpf_3=Patient:" + firstName+" "+lastName +
                "&ppmpf_4=Consultant" + consultantName +
//                "&ppmpf_4=Consultant"+
                "&ppmpf_5=Clinic:"+ clinicName

        val post = EncodingUtils.getBytes(finalUrlString, "BASE64")

        postWebView_activity.settings.javaScriptEnabled = true // enable javascript
        postWebView_activity.settings.allowContentAccess = true;
        postWebView_activity.setNetworkAvailable(true);
        postWebView_activity.settings.javaScriptCanOpenWindowsAutomatically = true;




        postWebView_activity.webViewClient = object : WebViewClient() {

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                Log.d("MyLogOnPage" + "Started", url.toString());

                pDialog = ProgressDialog(this@JazzCashActivity)
                pDialog!!.setMessage("Loading...")
                pDialog!!.setCancelable(false)
                showProgressDialog()

            }

            @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                Log.d("MyLogOnPage" + "shouldOverrideUrlLoading", request!!.url.toString());
                return super.shouldOverrideUrlLoading(view, request)

            }


            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                Log.d("MyLogOnPage" + "Finished", url.toString());


                hideProgressDialog()

//                if (url!!.contains("https://secure7.arcot.com/acspage/")){
//                    counter++
//                    if (counter==2){
//                        showProgressDialog()
//                    }
//                }
                if (url!!.contains("AndroidResponse")) {
                    if (url.contains("responseCode=000") ||
                            url.contains("responseCode=121") ||
                            url.contains("responseCode=200")) {
                            shouldAllowBack=false
                        //success
                        addAppointmentService()
                    } else if (url.contains("responseCode=124") ||
                            url.contains("responseCode=210")) {
                        //voucher
                    } else if (url.contains("responseCode=110")) {

                        //appointment booked
                        shouldAllowBack=false
//                        addAppointmentService()
                    } else {
                        //failure
                    }
                }
            }
        }

//        postWebView_activity.postUrl("https://sandbox.jazzcash.com.pk/CustomerPortal/transactionmanagement/merchantform", post)
        postWebView_activity.postUrl("https://payments.jazzcash.com.pk/CustomerPortal/transactionmanagement/merchantform", post)
        Log.d("asd", "asd")


    }

    fun hmacSHA256(key: ByteArray, data: String): ByteArray = try {
        val algorithm = "HmacSHA256"
        Mac.getInstance(algorithm).run {
            init(SecretKeySpec(key, algorithm))
            doFinal(data.toByteArray(charset("UTF8")))
        }
    } catch (e: Exception) {
        throw RuntimeException("Could not run HMAC SHA256", e)
    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun handleIntercept(request: WebResourceRequest): WebResourceResponse? {
        var okHttpClient = OkHttpClient()
        var call = if (
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            okHttpClient.newCall(Request.Builder()
                    .url(request.url.toString())
                    .method(request.method, null)
                    .headers(Headers.of(request.requestHeaders))
                    .build()
            )
        } else {
            TODO("VERSION.SDK_INT < LOLLIPOP")
        };
        try {
            var response = call.execute();
            response.headers();// get response header here
            return WebResourceResponse(
                    response.header("content-type", "text/plain"), // You can set something other as default content-type
                    response.header("content-encoding", "utf-8"),  //you can set another encoding as default
                    response.body()!!.byteStream())

        } catch (e: IOException) {
            e.printStackTrace()
            return null
        }
    }

    private fun generateEncryptedKeyFromParams() {
        val params = java.util.HashMap<String, String>()
//        params["pp_Amount]"] = "" + cashAmountField_activity.text + "00"
//        params["pp_BillReference]"] = "OrderID"
//        params["pp_Description"] = appointementTime
//        params["pp_DiscountedAmount"] = ""
//        params["pp_DiscountBank"] = ""
//        params["pp_Language"] = "EN"
//        params["pp_MerchantID"] = "MC0478" //Use ur own
//        params["pp_Password"] = "3x239tg0d0"// user ur pass
//        params["pp_ReturnURL"] = "https://www.healthcarepakistan.pk/Patient_trainingSecure/Appointment/PaymentResponse" //use ur return url
//        params["pp_SubMerchantID"] = ""
//        params["pp_TxnCurrency"] = "PKR"
//        params["pp_TxnDateTime"] = dateString
//        params["pp_TxnExpiryDateTime"] = expireDateString
//        params["pp_TxnRefNo"] = txnRefNo
//        params["pp_TxnType"] = ""
//          params["ppmpf_5"] = "platform_android"
//        params["ppmpf_4"] = userid
//        params["ppmpf_3"] = practiceId


        params["pp_Version]"] = "1.1"
        params["pp_TxnType]"] = ""
        params["pp_Language"] = "EN"
        params["pp_MerchantID"] = "00168541"
        params["pp_SubMerchantID"] = ""
        params["pp_Password"] = "e16t803g0u"
        params["pp_BankID"] = ""
        params["pp_ProductID"] = ""
        params["pp_TxnRefNo"] = txnRefNo
        params["pp_Amount"] = feeJazz
        params["pp_TxnCurrency"] = "PKR"
        params["pp_TxnDateTime"] = dateString
        params["pp_BillReference"] = "billRef"
        params["pp_Description"] = "Appointment booking with Dr Consultant" + " "+consultantName + "at" +" Clinic"+clinicName+" on" +" "+date +" at"+" "+timeStart
//        params["pp_Description"] = "Appointment booking with"
        params["pp_TxnExpiryDateTime"] = expireDateString
//        params["pp_ReturnURL"] = "https://www.healthcarepakistan.pk/Patient/home/index"
        params["pp_ReturnURL"] = "https://www.healthcarepakistan.pk/Patient/Appointment/PaymentResponse"
        params["ppmpf_1"] = "android"
        params["ppmpf_2"] = "2"
        params["ppmpf_3"] = "Patient:$firstName $lastName"
        params["ppmpf_4"] = "Consultant$consultantName"
//        params["ppmpf_4"] = "Consultant"
        params["ppmpf_5"] = "Clinic:$clinicName"


        val sortedParams = params.toSortedMap()
//
//        var html = "<html><body onload=\"document.forms[0].submit()\">" +
//                "<form name=\"jsform\" method=\"post\" action=\"https://sandbox.jazzcash.com.pk/CustomerPortal/transactionmanagement/merchantform\">"
//        sortedParams.forEach { (key, value) ->
//            html += "<input type=\"hidden\" name=\"$key\" value=\"$value\" >\n"
//        }
//        html += "</input></form></body></html>"


//        Log.d("html", html)


        val hashValuesArray = ArrayList(sortedParams.values)
        var hashkey = "b31u998z9w"
        for (i in hashValuesArray) {
            if (i != "") {
                hashkey = "$hashkey&$i"

            }
        }
//           val encryptedString=encryptString(hashkey)
        val encrptedBytes = hmacSHA256("b31u998z9w".toByteArray(), hashkey)
        encryptedString = encrptedBytes.fold("", { str, it -> str + "%02x".format(it) })
        Log.d("concatinated string", hashkey)
        Log.d("encrypted String", encryptedString)
    }

    override fun onBackPressed() {
//        this.finish()
        if (shouldAllowBack) {
            super.onBackPressed();
        }
    }

    private fun addAppointmentService() {
        val userId = FetchData.getData(this@JazzCashActivity, "userId")

        Log.d("Call", "response consultantId: " + consultantId)
        Log.d("Call", "response clincID: " + clincID)
        Log.d("Call", "response date: " + date)
        Log.d("Call", "response TimeStart: " + timeStart)
        Log.d("Call", "response TimeEnd: " + timeEnd)
        Log.d("Call", "response SlotID: " + slotID)
        Log.d("Call", "response CalendarID: " + calendarID)
        Log.d("Call", "response patId: " + userId)


        val params = HashMap<String, String>()
        params["action"] = "Book Appointment"
        params["consultantId"] = consultantId
        params["clincID"] = clincID
        params["date"] = myDate
        params["TimeStart"] = myTimeStart
        params["TimeEnd"] = timeEnd
        params["SlotID"] = slotID
        params["CalendarID"] = calendarID
        params["patId"] = userId
        params["Fee"] = fee
        params["Currency"] = "PKR"
        params["TransactionID"] = txnRefNo
        params["notes"] = ""
        params["appId"] = "0"
        params["rescheduleCheck"] = "false"

        val apiConnection = ApiConnection(object : IWebListener {
            override fun success(response: String?) {

                Log.d("Call", "response: " + response)
                if (response != null) {
                    try {
                        shouldAllowBack=true
                        val jsonObject = JSONObject(response)
                        val meta = jsonObject.getJSONObject("meta")
                        val code = meta.getString("code")
                        if (Integer.valueOf(code) == 200) {
                            val response = jsonObject.getString("response")
                            Toast.makeText(this@JazzCashActivity, response, Toast.LENGTH_SHORT).show()
                            val intent = Intent(this@JazzCashActivity, TabsActivity::class.java)
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            startActivity(intent)
                            finish()
                        } else if (Integer.valueOf(code) == 504) {
                            val error = jsonObject.getString("error")
                            shouldAllowBack=true
                            Toast.makeText(applicationContext, error, Toast.LENGTH_SHORT).show()
                        }

                    } catch (e: JSONException) {
                        shouldAllowBack=true
                        e.printStackTrace()
                    }
                }
            }

            override fun error(response: String) {
                Log.v("Error Add Appointment", "")
                shouldAllowBack=true

            }
        }, this@JazzCashActivity, params,
                Const.BookAppointment)
        apiConnection.makeStringReq()
    }

    private fun showProgressDialog() {
        if (!pDialog!!.isShowing) pDialog!!.show()
    }

    private fun hideProgressDialog() {
        if (pDialog!!.isShowing) pDialog!!.dismiss()
    }
}
