/*
package com.softech.clinicconnect;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.softech.clinicconnect.adapters.DocumentAdapter;
import com.softech.clinicconnect.adapters.SearchAdapter;
import com.softech.clinicconnect.apiconnections.ApiConnection;
import com.softech.clinicconnect.data.models.models.DocumentFolder.Document;
import com.softech.clinicconnect.data.models.models.SearchAppointmentModel;
import com.softech.clinicconnect.fragments.DocumentFragment;
import com.softech.clinicconnect.interfaces.IWebListener;
import com.softech.clinicconnect.sharedpreferences.FetchData;
import com.softech.clinicconnect.sharedpreferences.SaveData;
import com.softech.clinicconnect.utils.Const;
import com.softech.clinicconnect.utils.Logs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DocumentActivity extends AppCompatActivity implements IWebListener {
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;
    @BindView(R.id.calendarBtn)
    ImageView calendarBtn;

    @BindView(R.id.toolbar)
    android.support.v7.widget.Toolbar toolbar;

    FragmentManager fragmentManager;
    Document document;
    DocumentAdapter documentAdapter;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_document);
        ButterKnife.bind(this, this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerView.setHasFixedSize(true);
        callDocumentService();
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Documents");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DocumentActivity.this.finish();
            }
        });
        calendarBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(DocumentActivity.this, UploadDocumentActivity.class));
            }
        });

    }
        public void callDocumentService()
        {
        String consultantID = FetchData.getData(this, "userId");
        String access_token = FetchData.getData(this, "access_token");
        String patientID = FetchData.getData(this, "patientID");

        Map<String, String> params = new HashMap<String, String>();
        params.put("action", "Load Folders");
        params.put("consultantId", consultantID);
        params.put("access_token", access_token);
        params.put("patientId",patientID);
        ApiConnection apiConnection = new ApiConnection(this,
                this,params, Const.Documents);
        apiConnection.makeStringReq();
    }

    @Override
    public void success(String response)
    {
        if(response !=null) {

            try {

                JSONObject jsonObject = new JSONObject(response);
                JSONObject meta = jsonObject.getJSONObject("meta");
                String code = meta.getString("code");
                if(Integer.valueOf(code)== 200)
                {
                    String message = meta.getString("message");
                    document = new Gson().fromJson(response, Document.class);
//                    linearLayout1.setVisibility(View.VISIBLE);
                    documentAdapter = new DocumentAdapter(this,document.getResponse(),false);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
                    recyclerView.setLayoutManager(mLayoutManager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(documentAdapter);
                    // mAdapter.notifyDataSetChanged();
                    recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

                }
                else if(Integer.valueOf(code)== 504)
                {
                    document.getResponse().clear();
                    String message = meta.getString("message");

                    Toast.makeText(this, message, Toast.LENGTH_LONG).show();
                    //  recyclViewSearch.setVisibility(View.GONE);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            Logs.showLog("Login", response);
        }
    }
    @Override
    public void error(String response) {

    }
}
*/
