package com.softech.ccpatient.activities;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.softech.ccpatient.R;
import com.softech.ccpatient.sharedpreferences.FetchData;
import com.softech.ccpatient.webrtc.SocketIO;
import com.softech.ccpatient.webrtc.util.AppConstants;

import java.util.Objects;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO(developer): Handle FCM messages here.
        Log.e(TAG, "Firebase push: " + remoteMessage.getData());
        Log.e(TAG, "Firebase push: " + remoteMessage.getNotification());
        if (Objects.equals(remoteMessage.getData().get("nType"), "call")) {
            boolean isAlreadyConnected = false;
            if (SocketIO.Companion.getInstance().getSocket() != null) {
                if (SocketIO.Companion.getInstance(this).getSocket().connected()) {
                    isAlreadyConnected = true;
                }
            }
            SocketIO.Companion.getInstance().connectSocket(FetchData.getData(this, "userId"),
                    FetchData.getData(this, "firstName") + " " + FetchData.getData(this, "lastName"));
            SocketIO.Companion.getInstance(this).connectListeners();
            SocketIO.Companion.getInstance().setPushData(
                    Objects.requireNonNull(remoteMessage.getData().get("connectedUserId")),
                    true, Objects.requireNonNull(remoteMessage.getData().get("callerName")),
                    Objects.requireNonNull(remoteMessage.getData().get("callId")), remoteMessage.getData().get("isVideo"), isAlreadyConnected
            );
        }
        else if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            String title= remoteMessage.getData().get("title");
            String body= remoteMessage.getData().get("body");
//            sendNotification(String.valueOf(remoteMessage.getData()));
            Intent intent = new Intent(this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                    PendingIntent.FLAG_ONE_SHOT);
//            showSmallNotification("ClinicConnect-Consultant App", String.valueOf(remoteMessage.getNotification().getBody()), pendingIntent);
            notification(title,body,pendingIntent);
        }


        else {
            if (remoteMessage.getNotification() != null) {

                Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());

                sendNotification(String.valueOf(remoteMessage.getNotification().getBody()));
            }
        }
    }

    public void notification(String title,String body,PendingIntent pendingIntent){
        Uri sound = Uri. parse (ContentResolver. SCHEME_ANDROID_RESOURCE + "://" + getPackageName() + "/raw/quite_impressed.mp3" ) ;
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder( this,
                "1" )
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle( title )
                .setContentIntent(pendingIntent)
                .setContentText( body);
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context. NOTIFICATION_SERVICE ) ;
        if (android.os.Build.VERSION. SDK_INT >= android.os.Build.VERSION_CODES. O ) {
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setContentType(AudioAttributes. CONTENT_TYPE_SONIFICATION )
                    .setUsage(AudioAttributes. USAGE_ALARM )
                    .build() ;
            int importance = NotificationManager. IMPORTANCE_HIGH ;
            NotificationChannel notificationChannel = new
                    NotificationChannel( "1" , "NOTIFICATION_CHANNEL_NAME" , importance) ;
            notificationChannel.enableLights( true ) ;
            notificationChannel.setLightColor(Color. RED ) ;
            notificationChannel.enableVibration( true ) ;
            notificationChannel.setVibrationPattern( new long []{ 100 , 200 , 300 , 400 , 500 , 400 , 300 , 200 , 400 }) ;
//            notificationChannel.setSound(sound , audioAttributes) ;
            mBuilder.setChannelId( "1" ) ;
            assert mNotificationManager != null;
            mNotificationManager.createNotificationChannel(notificationChannel) ;
        }
        assert mNotificationManager != null;
        mNotificationManager.notify(( int ) System. currentTimeMillis (), mBuilder.build()) ;
    }


    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        Log.e(TAG, "onNewToken: " + s);
        SharedPreferences sp = getSharedPreferences("appData", 0);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(AppConstants.IS_NEW_TOKEN, true);
        editor.putString(AppConstants.PUSH_TOKEN, s);
        editor.apply();
    }


    private void sendNotification(String messageBody) {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(messageBody)

                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setPriority(Notification.PRIORITY_HIGH)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());


    }


}
