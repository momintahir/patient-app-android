package com.softech.ccpatient.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.widget.RelativeLayout;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;
import com.softech.ccpatient.R;
import com.softech.ccpatient.apiconnections.Api;
import com.softech.ccpatient.data.models.models.PlaceDirectionModel;
import com.softech.ccpatient.databinding.ActivityMapsBinding;
import com.softech.ccpatient.fragments.FindDoctorFragment;
import com.softech.ccpatient.fragments.FoundDoctorFragment;
import com.softech.ccpatient.utils.ConnectionDetector;
import com.softech.ccpatient.utils.Const;
import com.softech.ccpatient.utils.HSnackBar;
import com.softech.ccpatient.utils.Logs;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback
        ,GoogleApiClient.OnConnectionFailedListener
        ,GoogleApiClient.ConnectionCallbacks
        ,LocationListener
        , FoundDoctorFragment.UpdateMap {

    private GoogleMap mMap;
    public RelativeLayout bottom_fragment;
    private FindDoctorFragment findDoctorFragment;
    private ActivityMapsBinding binding;

    private double lat = 0.0, lng = 0.0;
    private String user_address="";
    private LatLng latLng;

    public Location location = null;
    private final String gps_perm = Manifest.permission.ACCESS_FINE_LOCATION;
    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    private final int gps_req = 22;
    private FragmentManager fragmentManager;
    private SupportMapFragment mapFragment;
    private boolean updated;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this,R.layout.activity_maps);

        initMap();
        initialize();
        launchFragment();


    }

    @Override
    protected void onStart() {
        super.onStart();

        CheckPermission();

    }

    private void initMap(){

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

    }

    private void initialize(){
        findDoctorFragment = new FindDoctorFragment();
        fragmentManager = getSupportFragmentManager();
    }

    private void launchFragment(){

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.bottom_fragment, findDoctorFragment );
        ft.commit();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        findDoctorFragment.stop();
        mMap = googleMap;
        latLng = new LatLng(lat, lng);

        MarkerOptions  markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.blue_marker));

        markerOptions.getPosition();
        mMap.addMarker(markerOptions);

        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera( CameraUpdateFactory.zoomTo( 15.0f ) );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1255)
        {
            if (resultCode == RESULT_OK)
            {
                //progBar.setVisibility(View.GONE);
                startLocationUpdate();

            }
            else if(resultCode == 0)
            {
                intLocationRequest();
                // progBar.setVisibility(View.VISIBLE);

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        List<Fragment> fragmentList = fragmentManager.getFragments();
        for (Fragment fragment : fragmentList) {
            fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        if (requestCode == gps_req && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            initGoogleApiClient();
        }
    }

    private void CheckPermission()
    {
        if (ActivityCompat.checkSelfPermission(this, gps_perm) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{gps_perm}, gps_req);
        } else {

            if (ConnectionDetector.getInstance(this).isConnectingToInternet()) {
                initGoogleApiClient();
            }else
            {
                try {
                    HSnackBar.showMsg(findViewById(android.R.id.content), "No Internet Connection.");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void initGoogleApiClient()
    {
        googleApiClient = new GoogleApiClient.Builder(this)
                .addOnConnectionFailedListener(this)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }

    private void intLocationRequest()
    {
        locationRequest = new LocationRequest();
        locationRequest.setFastestInterval(500*60);
        locationRequest.setInterval(500*60);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {

                Status status = result.getStatus();
                switch (status.getStatusCode())
                {
                    case LocationSettingsStatusCodes.SUCCESS:
                        startLocationUpdate();
                        //checkBlock();

                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(MapsActivity.this,1255);
                        }catch (Exception e)
                        {
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.e("not available","not available");
                        break;
                }
            }
        });

    }

    @SuppressLint("MissingPermission")
    private void startLocationUpdate()
    {
        locationCallback = new LocationCallback()
        {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                onLocationChanged(locationResult.getLastLocation());
            }
        };
        LocationServices.getFusedLocationProviderClient(this)
                .requestLocationUpdates(locationRequest,locationCallback, Looper.myLooper());
    }

    @Override
    public void onConnected(@Nullable Bundle bundle)
    {
        intLocationRequest();

    }

    @Override
    public void onConnectionSuspended(int i) {
        if (googleApiClient!=null)
        {
            googleApiClient.connect();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        LocationListener(location);
    }

    private void LocationListener(final Location location)
    {

        if (location!=null)
        {

            MapsActivity.this.location = location;
            lat = location.getLatitude();
            lng = location.getLongitude();
            mapFragment.getMapAsync(this);

//            user_address = getUserAddress(lat,lng);
//            user_city = user_address;

        }

    }

    @Override
    public void update(boolean status) {
        updated = status;

        if(updated){
            showDoctorLocation();
        }
        else{

        }

    }

    private void showDoctorLocation() {

        LatLng doctorLocation = new LatLng(31.4672701, 74.2637157);
        MarkerOptions  markerOptions = new MarkerOptions();
        markerOptions.position(doctorLocation);
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.car_icon));
        mMap.addMarker(markerOptions);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(doctorLocation));

        directionApi();
    }

    private void directionApi() {

        String originLoc = latLng.latitude+","+latLng.longitude+"", destLoc = "31.4672701,74.2637157";

        Api.getService(Const.googleDirectionBase_url)
                .getDirection(originLoc, destLoc, "driving", getString(R.string.google_maps_key))
                .enqueue(new Callback<PlaceDirectionModel>() {
                    @Override
                    public void onResponse(Call<PlaceDirectionModel> call, Response<PlaceDirectionModel> response) {
                        if (response.body() != null && response.body().getRoutes().size() > 0) {

                            drawRoute(PolyUtil.decode(response.body().getRoutes().get(0).getOverview_polyline().getPoints()));
//                            distance += response.body().getRoutes().get(0).getLegs().get(0).getDistance().getValue();
//                            time += response.body().getRoutes().get(0).getLegs().get(0).getDuration().getValue();

//                                tv_distance.setText(String.format("%s %s", String.valueOf(Math.round(distance / 1000.0)), getString(R.string.km)));
//                                tv_time.setText(String.format("%s %s", String.valueOf(Math.round(time / 60.0)), getString(R.string.min2)));
//                                dialog.dismiss();

                        } else {
                            //dialog.dismiss();
                            //Toast.makeText(activity, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                            Logs.showLog("response", response.toString());
                        }
                    }

                    @Override
                    public void onFailure(Call<PlaceDirectionModel> call, Throwable t) {

                        t.getMessage();
                    }
                });
    }

    private void drawRoute(List<LatLng> latLngList) {
        PolylineOptions options = new PolylineOptions();
        options.geodesic(true);
        options.color(ContextCompat.getColor(this, R.color.colorAccent));
        options.width(8.0f);
        options.addAll(latLngList);
        mMap.addPolyline(options);
    }

}
