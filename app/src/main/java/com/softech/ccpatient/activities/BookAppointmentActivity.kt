package com.softech.ccpatient.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.softech.ccpatient.R
import com.softech.ccpatient.apiconnections.ApiConnection
import com.softech.ccpatient.data.models.models.PatientList.Response
import com.softech.ccpatient.interfaces.IWebListener
import com.softech.ccpatient.sharedpreferences.FetchData
import com.softech.ccpatient.utils.Const
import kotlinx.android.synthetic.main.activity_book_appointments.*
import kotlinx.android.synthetic.main.content_book_appointment.*
import org.json.JSONException
import org.json.JSONObject
import java.util.*

@Suppress("NAME_SHADOWING")
class BookAppointmentActivity : AppCompatActivity() {

    internal var response: Response? = null
    var consultantId: String = ""
    var clincID: String = ""
    var date: String = ""
    var timeStart: String = ""
    var timeEnd: String = ""
    var slotID: String = ""
    var calendarID: String = ""
    var fee=""
    var clinicName=""
    var appointmentId=""
    var consultantName=""
    var isRecheduled=false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_book_appointments)

        ivBack.setOnClickListener {
            finish()
        }

        val cellNumber = FetchData.getData(this, "cellNumber")
        val firstName = FetchData.getData(this, "firstName")
        val lastName = FetchData.getData(this, "lastName")
        val gender = FetchData.getData(this, "gender")
        tvPhone.setText(cellNumber)
        tvFirstName.setText(firstName)
        tvLastName.setText(lastName)

//        if (gender.equals("1"))
//            tvGender.setText("Male")
//        else
//            tvGender.setText("Female")

        val dropdown: Spinner = findViewById(R.id.spinner)
        val items = arrayOf("Male", "Female")
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, items)
        dropdown.adapter = adapter

        val intent = intent.extras
        if (intent != null) {
            isRecheduled = intent.getBoolean("isRescheduled")
            if (isRecheduled){
                consultantId = intent.getString("consultantId", "")
                clincID = intent.getString("clincID", "")
                date = intent.getString("date", "")
                timeStart = intent.getString("TimeStart").toString()
                timeEnd = intent.getString("TimeEnd").toString()
                slotID = intent.getString("SlotID", "")
                calendarID = intent.getString("CalendarID", "")
                clinicName = intent.getString("clinicName", "")
                appointmentId = intent.getString("appointmentId", "")
            }
            else{
                consultantId = intent.getString("consultantId", "")
                clincID = intent.getString("clincID", "")
                date = intent.getString("date", "")
                timeStart = intent.getString("TimeStart").toString()
                consultantName = intent.getString("consultantName").toString()
                timeEnd = intent.getString("TimeEnd").toString()
                slotID = intent.getString("SlotID", "")
                calendarID = intent.getString("CalendarID", "")
                clinicName = intent.getString("clinicName", "")
                fee=intent.getString("fee","")
                tvFee.text = "Doc Fee: $fee"
            }

//            patId = intent.getString("patId", "")
        }



        btnSubmit.setOnClickListener() {
//            if (tvFirstName!!.text.isNotEmpty() &&
//                    tvLastName!!.text.isNotEmpty() &&
//                    tvPhone!!.text.toString().isNotEmpty()) {
//                addAppointmentService()
//            } else {
//                Toast.makeText(this, "Please Fill All Fields", Toast.LENGTH_LONG).show()
//            }



            if (isRecheduled){
                recheduleAppointment()

            }
            else{
                val intent = Intent(applicationContext, JazzCashActivity::class.java)
                intent.putExtra("consultantId", consultantId)
                intent.putExtra("clincID", clincID)
                intent.putExtra("date", date)
                intent.putExtra("fee", fee)
                intent.putExtra("TimeStart", timeStart)
                intent.putExtra("consultantName", consultantName)
                intent.putExtra("TimeEnd", timeEnd)
                intent.putExtra("SlotID", slotID)
                intent.putExtra("CalendarID",calendarID)
                intent.putExtra("clinicName",clinicName)
                startActivity(intent)
            }



        }
//        searchBtn.setOnClickListener() {
//            if (editTextPhone.text.isNotEmpty()) {
//                getpatientList()
//            } else {
//                Toast.makeText(applicationContext, "Please enter valid phone number to search.", Toast.LENGTH_SHORT).show()
//
//            }
//
//        }
    }

    /*private fun getpatientList() {
        val params = HashMap<String, String>()
        params["action"] = "Search Patient Result"
        params["consultantId"] = consultantID
        params["access_token"] = access_token
        params["contactNo"] = editTextPhone.text.toString()
        val apiConnection = ApiConnection(object : IWebListener {
            override fun success(response: String?) {

                if (response != null) {
                    try {
                        val jsonObject = JSONObject(response)
                        val meta = jsonObject.getJSONObject("meta")
                        val code = meta.getString("code")
                        if (Integer.valueOf(code) == 200) {
                            val jsonResponse = jsonObject.getJSONArray("response")
                            val gson = Gson()
                            val type = object : TypeToken<ArrayList<Response>>() {

                            }.type
                            val searchedPatientList = gson.fromJson<List<Response>>(jsonResponse.toString(), type)
                            Logs.showLog("Login", response)
                            if (searchedPatientList.size > 0) {
                                var patienNameArrayList: ArrayList<String> = ArrayList()
                                for (patList in searchedPatientList) {
                                    patienNameArrayList.add(patList.patient)

                                }
                                showOptionsMenu(patienNameArrayList, searchedPatientList)
                            }

                        } else if (Integer.valueOf(code) == 504) {
                            val error = jsonObject.getString("error")
                            Toast.makeText(applicationContext, error, Toast.LENGTH_SHORT).show()
                        }


                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    Logs.showLog("Patient List", response)
                }
            }

            override fun error(response: String) {
                Log.v("Error Add Appointment", "")

            }
        }, this@BookAppointmentActivity, params,
                Const.SEARCH_PATIENT)
        apiConnection.makeStringReq()
    }*/

    /*private fun showOptionsMenu(stringArray: ArrayList<String>, fullPatientList: List<Response>) {
        val alert = AlertDialog.Builder(this)
        alert.setTitle("Choose Patient")
        val adapter = ArrayAdapter<String>(this@BookAppointmentActivity, R.layout.spinner, stringArray)
        alert.setAdapter(adapter, DialogInterface.OnClickListener { dialog, which ->
            val strName = adapter.getItem(which)
            etFirstName.setText(fullPatientList.get(which).firstName)
            etLastName.setText(fullPatientList.get(which).lastName)
            etPhone.setText(fullPatientList.get(which).mobile)
            if (fullPatientList.get(which).gender.equals("Male")) {
                spGender.setSelection(0)
            } else {
                spGender.setSelection(1)
            }
        })
        alert.setNegativeButton("Cancel") { dialog, which -> dialog.dismiss() }
        alert.show()
    }*/


    private fun recheduleAppointment() {
        val userId = FetchData.getData(this@BookAppointmentActivity, "userId")

        Log.d("Call", "response consultantId: " + consultantId)
        Log.d("Call", "response clincID: " + clincID)
        Log.d("Call", "response date: " + date)
        Log.d("Call", "response TimeStart: " + timeStart)
        Log.d("Call", "response TimeEnd: " + timeEnd)
        Log.d("Call", "response SlotID: " + slotID)
        Log.d("Call", "response CalendarID: " + calendarID)
        Log.d("Call", "response patId: " + userId)


        val params = HashMap<String, String>()
        params["action"] = "Book Appointment"
        params["consultantId"] = consultantId
        params["clincID"] = clincID
        params["date"] = date
        params["TimeStart"] = timeStart
        params["TimeEnd"] = timeEnd
        params["SlotID"] = slotID
        params["CalendarID"] = calendarID
        params["patId"] = userId
        params["Fee"] = fee
        params["Currency"] = "PKR"
        params["TransactionID"] =""
        params["notes"] =""
        params["appId"] =appointmentId
        params["rescheduleCheck"] ="true"

        val apiConnection = ApiConnection(object : IWebListener {
            override fun success(response: String?) {

                Log.d("Call", "response: " + response)
                if (response != null) {
                    try {
                        val jsonObject = JSONObject(response)
                        val meta = jsonObject.getJSONObject("meta")
                        val code = meta.getString("code")
                        if (Integer.valueOf(code) == 200) {
                            val response = jsonObject.getString("response")
//                            Toast.makeText(this@BookAppointmentActivity, response, Toast.LENGTH_SHORT).show()
                            val intent = Intent(this@BookAppointmentActivity, TabsActivity::class.java)
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            startActivity(intent)
                            finish()
                        } else if (Integer.valueOf(code) == 504) {
                            val error = jsonObject.getString("error")
                            Toast.makeText(applicationContext, error, Toast.LENGTH_SHORT).show()
                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
            }

            override fun error(response: String) {
                Log.v("Error Add Appointment", "")

            }
        }, this@BookAppointmentActivity, params,
                Const.BookAppointment)
        apiConnection.makeStringReq()
    }



}
