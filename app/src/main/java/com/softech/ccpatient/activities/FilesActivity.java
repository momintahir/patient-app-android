/*
package com.softech.clinicconnect;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.softech.clinicconnect.adapters.DocumentAdapter;
import com.softech.clinicconnect.adapters.FilesAdapter;
import com.softech.clinicconnect.apiconnections.ApiConnection;
import com.softech.clinicconnect.data.models.models.DocumentFolder.Document;
import com.softech.clinicconnect.data.models.models.SelectedDocument.SelectedDocument;
import com.softech.clinicconnect.interfaces.IWebListener;
import com.softech.clinicconnect.sharedpreferences.FetchData;
import com.softech.clinicconnect.utils.Const;
import com.softech.clinicconnect.utils.Logs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FilesActivity extends AppCompatActivity implements IWebListener {
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    FragmentManager fragmentManager;
SelectedDocument selectedDocument;
    FilesAdapter filesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_files);
        ButterKnife.bind(this,this);
        callFilesService();
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Files");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FilesActivity.this.finish();
            }
        });
    }
    public void callFilesService() {
        String consultantID = FetchData.getData(this, "userId");
        String access_token = FetchData.getData(this, "access_token");
        String patientID = FetchData.getData(this, "patientID");
        String folderID = FetchData.getData(this, "folderID");


        Map<String, String> params = new HashMap<String, String>();
        params.put("action", "Load Selected Folders");
        params.put("consultantId", consultantID);
        params.put("access_token", access_token);
        params.put("patientId",patientID);
        params.put("folderId",folderID);
        params.put("stage","yes");
        ApiConnection apiConnection = new ApiConnection(this,
                this,params, Const.SelectedDocuments);
        apiConnection.makeStringReq();
    }

    @Override
    public void success(String response)
    {
        if(response !=null) {

            try {

                JSONObject jsonObject = new JSONObject(response);
                JSONObject meta = jsonObject.getJSONObject("meta");
                String code = meta.getString("code");
                if(Integer.valueOf(code)== 200)
                {
                    String message = meta.getString("message");
                    selectedDocument = new Gson().fromJson(response, SelectedDocument.class);
//                    linearLayout1.setVisibility(View.VISIBLE);
                    filesAdapter = new FilesAdapter(this,selectedDocument.getResponse());
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
                    recyclerView.setLayoutManager(mLayoutManager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(filesAdapter);
                    // mAdapter.notifyDataSetChanged();
                    recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

                }
                else if(Integer.valueOf(code)== 504)
                {
                    String message = meta.getString("message");

                    Toast.makeText(this, message, Toast.LENGTH_LONG).show();
                    //  recyclViewSearch.setVisibility(View.GONE);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            Logs.showLog("Login", response);
        }
    }

    @Override
    public void error(String response) {

    }
}
*/
