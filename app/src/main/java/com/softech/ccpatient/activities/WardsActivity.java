package com.softech.ccpatient.activities;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;

import com.softech.ccpatient.R;
import com.softech.ccpatient.data.models.adapters.WardsAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WardsActivity extends AppCompatActivity {

    @BindView(R.id.recyclViewWards)
    RecyclerView recyclViewWards;
    private WardsAdapter adapter;
    private ArrayList<String> hospitals = new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wards);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Wards");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WardsActivity.this.finish();
            }
        });

        ButterKnife.bind(this);

        hospitals.add("Jinnah Hospitals");
        hospitals.add("General Hospitals");
        hospitals.add("International Hospitals");
        hospitals.add("Farooq Hospitals");
        hospitals.add("Doctors Hospitals");
        hospitals.add("Gulab Devi Hospitals");
        hospitals.add("Ghanga Ram Hospitals");
        hospitals.add("Adil Hospitals");
        hospitals.add("Akhtar Saeed Hospitals");



        adapter = new WardsAdapter(hospitals, WardsActivity.this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclViewWards.setLayoutManager(mLayoutManager);
        recyclViewWards.setItemAnimator(new DefaultItemAnimator());
        recyclViewWards.setAdapter(adapter);
    }

}
