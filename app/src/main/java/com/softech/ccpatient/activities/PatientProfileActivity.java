package com.softech.ccpatient.activities;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.softech.ccpatient.R;
import com.softech.ccpatient.apiconnections.ApiConnection;
import com.softech.ccpatient.data.models.models.PatientProfile;
import com.softech.ccpatient.interfaces.IWebListener;
import com.softech.ccpatient.sharedpreferences.FetchData;
import com.softech.ccpatient.utils.Const;
import com.softech.ccpatient.utils.Logs;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PatientProfileActivity extends AppCompatActivity {

    @BindView(R.id.textViewFullName)
    TextView textViewFullName;
    @BindView(R.id.textViewDateOfBirth)
    TextView textViewDateOfBirth;
    @BindView(R.id.textViewGenderVal)
    TextView textViewGenderVal;
    @BindView(R.id.textViewAddressData)
    TextView textViewAddressData;
    @BindView(R.id.textViewCityData)
    TextView textViewCityData;
    @BindView(R.id.textViewCountryData)
    TextView textViewCountryData;
    @BindView(R.id.textViewPhoneData)
    TextView textViewPhoneData;

    private String patientId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Patient Profile");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PatientProfileActivity.this.finish();
            }
        });
        ButterKnife.bind(this);

        Bundle intent = getIntent().getExtras();
        if (intent != null) {
            patientId = intent.getString("patientId", "");
        }
        getPatientProfile();


    }

    private void getPatientProfile() {

        String consultantID = FetchData.getData(PatientProfileActivity.this, "userId");
        String access_token = FetchData.getData(PatientProfileActivity.this, "access_token");

        Map<String, String> params = new HashMap<String, String>();
        params.put("action", "Patient Profile");
        params.put("consultantId", consultantID);
        params.put("access_token", access_token);

        params.put("patientId", patientId);


        ApiConnection apiConnection = new ApiConnection(new IWebListener() {
            @Override
            public void success(String response) {

                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject meta = jsonObject.getJSONObject("meta");
                        String code = meta.getString("code");
                        if (Integer.valueOf(code) == 200) {

                            String responseString = jsonObject.getString("response");
                            Gson gson = new Gson();
                            Type type = new TypeToken<PatientProfile>() {
                            }.getType();

                            PatientProfile profile = gson.fromJson(responseString.toString(), type);

                            textViewFullName.setText(profile.getPatName());
                            textViewDateOfBirth.setText(profile.getPatDOB());
                            textViewAddressData.setText(profile.getPatAddress());
                            textViewCityData.setText(profile.getPatCity());
                            textViewCountryData.setText(profile.getPatCountry());
                            textViewGenderVal.setText(profile.getPatGender());
                            textViewPhoneData.setText(profile.getPatCellNumber());


                        } else if (Integer.valueOf(code) == 504) {

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Logs.showLog("Book an Appointment", response);
                }
            }

            @Override
            public void error(String response) {
                Log.v("Error Add Appointment", "");

            }
        }, PatientProfileActivity.this, params,
                Const.ACTION_GETPATIENTPROFILE);
        apiConnection.makeStringReq();
    }
}
