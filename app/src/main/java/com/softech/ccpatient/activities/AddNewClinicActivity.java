package com.softech.ccpatient.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.softech.ccpatient.R;
import com.softech.ccpatient.apiconnections.ApiConnection;
import com.softech.ccpatient.data.models.models.SpecialitiesModel;
import com.softech.ccpatient.interfaces.IWebListener;
import com.softech.ccpatient.sharedpreferences.FetchData;
import com.softech.ccpatient.sharedpreferences.SaveData;
import com.softech.ccpatient.utils.Const;
import com.softech.ccpatient.utils.Logs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddNewClinicActivity extends AppCompatActivity implements IWebListener {


    @BindView(R.id.editTextClinicName)
    EditText editTextClinicName;
    @BindView(R.id.editTextStreetAddress)
    EditText editTextStreetAddress;
    @BindView(R.id.editTextCountryName)
    EditText editTextCountryName;
    @BindView(R.id.editTextPhoneNumber)
    EditText editTextPhoneNumber;
    @BindView(R.id.editTextZideCode)
    EditText editTextZideCode;
    @BindView(R.id.spinnerSpeciality)
    Spinner spinnerSpeciality;
    ArrayList<String> data = new ArrayList<String>();
    @BindView(R.id.buttonAddNewClinic)
    Button buttonAddNewClinic;
    @BindView(R.id.editTextCities)
    EditText editTextCities;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_clinic);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("ADD YOUR CLINIC");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddNewClinicActivity.this.finish();
            }
        });

        ButterKnife.bind(this);
        getSpecialities();




        buttonAddNewClinic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (editTextClinicName.getText().toString().length()>0 &&
                        editTextStreetAddress.getText().toString().length()>0 &&
                        editTextCities.getText().toString().length()>0&&
                        editTextCountryName.getText().toString().length()>0 &&
                        editTextPhoneNumber.getText().toString().length()>0

                        ) {

                    String consultantID = FetchData.getData(AddNewClinicActivity.this, "userId");
                    String access_token = FetchData.getData(AddNewClinicActivity.this, "access_token");

                    Map<String, String> params = new HashMap<String, String>();
                    params.put("action", "Add New Clinic");
                    params.put("consultantId", consultantID);
                    params.put("clinicName", editTextClinicName.getText().toString());
                    params.put("speciality", spinnerSpeciality.getSelectedItem().toString());
                    params.put("phoneNumber", editTextPhoneNumber.getText().toString());
                    params.put("address", editTextStreetAddress.getText().toString());
                    params.put("phoneNumber", editTextPhoneNumber.getText().toString());
                    params.put("city", editTextCities.getText().toString());
                    params.put("country", editTextCountryName.getText().toString());
                    params.put("access_token", access_token);
                    params.put("zipcode", editTextZideCode.getText().toString());



                    ApiConnection apiConnection = new ApiConnection(AddNewClinicActivity.this,
                            AddNewClinicActivity.this,params, Const.ADDNEWCLINIC);
                    apiConnection.makeStringReq();
                } else {

                    Toast.makeText(AddNewClinicActivity.this, "Please fill all of the fields.", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    private void getSpecialities()
    {
        Map<String, String> params = new HashMap<String, String>();

        ApiConnection apiConnection = new ApiConnection(new IWebListener() {
            @Override
            public void success(String response) {

                if(response !=null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject meta = jsonObject.getJSONObject("meta");
                        String code = meta.getString("code");
                        if(Integer.valueOf(code)== 200)
                        {
                            JSONArray jsonResponse = jsonObject.getJSONArray("response");
                            Gson gson = new Gson();
                            Type type = new TypeToken<ArrayList<SpecialitiesModel>>(){}.getType();
                            List<SpecialitiesModel> specialitiesModelList = gson.fromJson(jsonResponse.toString(), type);
                            Logs.showLog("Login", response);

                            ArrayAdapter<SpecialitiesModel> adapter =
                                    new ArrayAdapter<SpecialitiesModel>(AddNewClinicActivity.this, R.layout.spinner, specialitiesModelList);
                            adapter.setDropDownViewResource(R.layout.spinner);
                            spinnerSpeciality.setAdapter(adapter);
                            spinnerSpeciality.setPrompt("Select Site");

                        }



                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Logs.showLog("Login", response);
                }
            }

            @Override
            public void error(String response) {

            }
        }, AddNewClinicActivity.this, params, Const.GETSPECIALITIES);
        apiConnection.makeStringReq();

    }


    @Override
    public void success(String response) {

        if(response !=null) {

            try {

                JSONObject jsonObject = new JSONObject(response);
                JSONObject meta = jsonObject.getJSONObject("meta");
                String code = meta.getString("code");
                if(Integer.valueOf(code)== 200)
                {

                    String message = meta.getString("message");
                    Toast.makeText(this, message, Toast.LENGTH_LONG).show();
                    JSONObject response1 = jsonObject.getJSONObject("response");
                    String ClinicId = response1.getString("ClinicId");
                    SaveData.SaveData(AddNewClinicActivity.this, "clinicID", ClinicId);

                    startActivity(new Intent(AddNewClinicActivity.this, AddCalendarActivity.class));
                    AddNewClinicActivity.this.finish();

                }



            } catch (JSONException e) {
                e.printStackTrace();
            }

            Logs.showLog("Login", response);
        }
        Log.d("response", response);
    }

    @Override
    public void error(String response) {

    }
}
