package com.softech.ccpatient.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.softech.ccpatient.R
import com.softech.ccpatient.sharedpreferences.FetchData
import com.softech.ccpatient.sharedpreferences.SaveData
import kotlinx.android.synthetic.main.activity_varification.*


class VarificationActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_varification)

        val twoFACode = FetchData.getData(this, "twoFACode")

      //  etCode.setText(twoFACode)

        btnApplyCode.setOnClickListener() {
            if (etCode.text.toString().equals(twoFACode)) {
                val intent = Intent(this@VarificationActivity, TabsActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                SaveData.SaveData(this@VarificationActivity, "exists", "true")
                startActivity(intent)
                finish()
            } else {

                Toast.makeText(this@VarificationActivity, "Please enter the correct code.", Toast.LENGTH_LONG).show()
            }
        }
    }
}

