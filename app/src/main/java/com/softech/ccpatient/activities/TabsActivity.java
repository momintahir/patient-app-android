package com.softech.ccpatient.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.google.firebase.iid.FirebaseInstanceId;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.softech.ccpatient.R;
import com.softech.ccpatient.apiconnections.ApiConnection;
import com.softech.ccpatient.fragments.DocumentFragment;
import com.softech.ccpatient.fragments.FileFragment;
import com.softech.ccpatient.fragments.FragmentAppointments;
import com.softech.ccpatient.fragments.FragmentHistory;
import com.softech.ccpatient.fragments.FragmentMyProfile;
import com.softech.ccpatient.fragments.UploadDocFragment;
import com.softech.ccpatient.fragments.WebviewFragment;
import com.softech.ccpatient.interfaces.IWebListener;
import com.softech.ccpatient.sharedpreferences.FetchData;
import com.softech.ccpatient.utils.CallBacks;
import com.softech.ccpatient.utils.Const;
import com.softech.ccpatient.utils.Dialogues;
import com.softech.ccpatient.webrtc.CallActivity;
import com.softech.ccpatient.webrtc.SocketIO;
import com.softech.ccpatient.webrtc.callbacks.WebSocketNewCallListener;
import com.softech.ccpatient.webrtc.util.AppConstants;
import com.softech.ccpatient.webrtc.util.Debugger;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;


public class TabsActivity extends AppCompatActivity implements View.OnClickListener, WebSocketNewCallListener {

    ImageView btnAdd;
    TextView textViewTitle;
    RelativeLayout rlFrag;
    static ViewPager viewPager;
    static ViewPagerAdapter adapter;
    ImageView fab;
    TabLayout tabLayout;
    private String fcmToken = "";
    Intent intent;
    static Timer timer = new Timer();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabs);
        initPermissions();
//        intent  = new Intent(this, MyService.class);
//        this.startService(intent);
        String email= FetchData.getData(this, "emailAddress");

        timer=new Timer();
       timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                //your method
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", email);
                params.put("IsOnline", String.valueOf(1));
                ApiConnection apiConnection = new ApiConnection(new IWebListener() {
                    @Override
                    public void success(String response) {

                        Log.d("Let's see Success",response.toString());
                    }

                    @Override
                    public void error(String response) {

                    }
                }, params, Const.SEND_ONLINE_STATUS);
                apiConnection.makeStringReqService();
            }
        }, 1000, 5000);


    }

    void initPermissions() {
        Dexter.withActivity(this).withPermissions(
                Manifest.permission.CAMERA,
                Manifest.permission.RECORD_AUDIO
        ).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                if (report.areAllPermissionsGranted()) {
                    SocketIO.Companion.getInstance().connectSocket(FetchData.getData(TabsActivity.this, "userId"),
                            FetchData.getData(TabsActivity.this, "firstName") + " " + FetchData.getData(TabsActivity.this, "lastName"));
                    SocketIO.Companion.getInstance().connectListeners();
                    SocketIO.Companion.getInstance().setOfferListener(TabsActivity.this, true);
                    setFont();
                    init();
                    clicks();
                    fcmToken = FetchData.getData(TabsActivity.this, AppConstants.PUSH_TOKEN);
                    if (fcmToken.equals("")) {
                        updateToken();
                    }
                    if (FetchData.getBoolean(TabsActivity.this, AppConstants.IS_NEW_TOKEN)) {
                        fcmToken = FetchData.getData(TabsActivity.this, AppConstants.PUSH_TOKEN);
                        SocketIO.Companion.getInstance().updatePushToken(fcmToken);
                        SharedPreferences sp = getSharedPreferences("appData", 0);
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putBoolean(AppConstants.IS_NEW_TOKEN, false);
                        editor.apply();
                    }
                } else {
                    Debugger.INSTANCE.e("Capturing Image", "onPermissionDenied");
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                token.continuePermissionRequest();
            }
        }).check();
    }

    public void updateToken() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                if (task.getResult() != null) {
                    fcmToken = task.getResult().getToken();
                    SharedPreferences sp = getSharedPreferences("appData", 0);
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString(AppConstants.PUSH_TOKEN, fcmToken);
                    editor.apply();
                    SocketIO.Companion.getInstance().updatePushToken(fcmToken);
                    Log.e("updateToken", "updateToken " + fcmToken);
                }
            } else {
                Log.e("getFCMToken", "getInstance Failed:${task.exception");
            }
        });
    }

    private void clicks() {

        fab.setOnClickListener(this);
        btnAdd.setOnClickListener(this);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0 || tab.getPosition() == 2) {
                    btnAdd.setVisibility(View.VISIBLE);
                } else {
                    btnAdd.setVisibility(View.GONE);
                }

                if (rlFrag.getVisibility() == View.VISIBLE) {
                    rlFrag.setVisibility(View.GONE);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                if (rlFrag.getVisibility() == View.VISIBLE) {
                    rlFrag.setVisibility(View.GONE);
                }
            }
        });


    }

    private void setFont() {
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/HelveticaNeueLTStd-ThCn.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

    }

    private void init() {

        rlFrag = findViewById(R.id.rlFrag);

        viewPager = findViewById(R.id.viewpager);
        btnAdd = findViewById(R.id.btnAdd);
        textViewTitle = findViewById(R.id.textViewTitle);
        setupViewPager(viewPager);
        viewPager.setOffscreenPageLimit(3);
        tabLayout = findViewById(R.id.tabs);
        fab = findViewById(R.id.fab);
        tabLayout.setupWithViewPager(viewPager);
    }

    public void fileFrag(String folderId) {
        rlFrag.setVisibility(View.VISIBLE);

        Bundle bundle = new Bundle();
        bundle.putString(Const.FOLDER_ID, folderId);
        FileFragment fileFragment = new FileFragment();
        fileFragment.setArguments(bundle);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frag, fileFragment);
        ft.commit();
    }

    public void webviewFrag(String url, String type,String file) {
        rlFrag.setVisibility(View.VISIBLE);

        Bundle bundle = new Bundle();
        bundle.putString(Const.FILE_PATH, url);
        bundle.putString("fileType", type);
        bundle.putString("file",file);
        WebviewFragment webviewFragment = new WebviewFragment();
        webviewFragment.setArguments(bundle);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frag, webviewFragment);
        ft.commit();
    }

    public void uploadDocFrag() {
        rlFrag.setVisibility(View.VISIBLE);

        UploadDocFragment uploadDocFragment = new UploadDocFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frag, uploadDocFragment);
        ft.commit();
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new FragmentAppointments(), "Appointments");
        adapter.addFragment(new FragmentHistory(), "Your History");
        adapter.addFragment(new DocumentFragment(), "Docs");
        adapter.addFragment(new FragmentMyProfile(), "MY Profile");
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    textViewTitle.setText("Your Appointments");
                } else if (position == 1) {
                    textViewTitle.setText("Your History");
                } else if (position == 2) {
                    textViewTitle.setText("Documents");
                }

                else if (position == 3) {
                    textViewTitle.setText("Your Profile");
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

//    @Override
//    public void onBackPressed() {
//        moveTaskToBack(true);
//        return;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {

            Dialogues.yesnoDialogue(TabsActivity.this, "Are you sure you want to logout?", new CallBacks() {
                @Override
                public void yes() {
                    SharedPreferences preferences = getSharedPreferences("appData", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.clear();
                    editor.apply();
                    Intent intent = new Intent(TabsActivity.this, LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    TabsActivity.this.finish();
                }

                @Override
                public void no() {

                }
            });

            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.fab:
                Intent intent = new Intent(this, MapsActivity.class);
                startActivity(intent);
                break;
            case R.id.btnAdd:
                if (viewPager.getCurrentItem() == 0) {
                    startActivity(new Intent(TabsActivity.this, SearchActivity.class));
                } else if (viewPager.getCurrentItem() == 2) {
                    uploadDocFrag();
                }
                break;
        }
    }

    @Override
    public void newCallListener(@NotNull JSONObject jsonObject) {
        try {
            Debugger.INSTANCE.e("offerCallback", "json :" + jsonObject);
            switch (jsonObject.getString(AppConstants.TYPE)) {
                case AppConstants.OFFER: {
                    Debugger.INSTANCE.e("offerCallback", "json : $jsonObject");
                    if (jsonObject.getBoolean("isVideo")) {
                        startNewActivity(jsonObject, true);
                    } else {
                        startNewActivity(jsonObject, false);
                    }
                    break;
                }
                case AppConstants.NEW_CALL: {
                    Debugger.INSTANCE.e("Sending ready for call in newCallListener", "rfc :" + jsonObject);
                    SocketIO.Companion.getInstance().onReadyForCall(
                            jsonObject.getString(AppConstants.CONNECTED_USER_ID),
                            jsonObject.getString("name"),
                            jsonObject.getString("callId")
                    );
                }
                break;
            }
        } catch (Exception e) {
            Debugger.INSTANCE.e("Exception", "Exception" + e.getMessage());
        }
    }

    private void startNewActivity(
            JSONObject jsonObject,
            Boolean isVideo
    ) {
        Intent intent = new Intent(this, CallActivity.class);
        intent.putExtra(AppConstants.JSON, jsonObject.toString());
        intent.putExtra(AppConstants.IS_VIDEO_CALL, isVideo);
        startActivity(intent);
        overridePendingTransition(R.anim.bottom_up, R.anim.anim_nothing);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    @Override
    protected void onDestroy() {
        Log.d("onFunctionCalled","Destory");
        super.onDestroy();
       timer.cancel();
    }


    @Override
    protected void onPause() {
        Log.d("onFunctionCalled","Pause");
        super.onPause();

    }

    public static void refreshFragments() {
        viewPager.setAdapter(adapter);
    }


}
