package com.softech.ccpatient.activities

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.softech.ccpatient.R
import com.softech.ccpatient.apiconnections.ApiConnection
import com.softech.ccpatient.interfaces.IWebListener
import com.softech.ccpatient.sharedpreferences.SaveData
import com.softech.ccpatient.utils.AESEncryption
import com.softech.ccpatient.utils.Const
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_sign_up.*
import kotlinx.android.synthetic.main.activity_sign_up.etPassword
import kotlinx.android.synthetic.main.activity_sign_up.tvCondition
import kotlinx.android.synthetic.main.activity_sign_up.tvPolicy
import kotlinx.android.synthetic.main.activity_sign_up.tvTerms
import org.json.JSONException
import org.json.JSONObject
import java.util.*
import java.util.regex.Pattern


class SignUpActivity : AppCompatActivity(), IWebListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.title = "SIGN UP"

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        toolbar.setNavigationOnClickListener { this@SignUpActivity.finish() }
        //        editTextFirstName.setText("saim");
        //        editTextLastName.setText("shafqat");
        //        editTextPassword.setText("1123456");
        //        editTextEmail.setText("saim@gmail.com");

//        etFirstName.setText("najam")
//        etLastName.setText("masood")
//        etPassword.setText("qwerty")
//        etEmail.setText("najam.khan9494@gmail.com")
        // etPhone.setText("03214793668")


        tvTerms.setOnClickListener {
            val intent = Intent(this@SignUpActivity, WebviewActivity::class.java)
            intent.putExtra("url", "https://www.healthcarepakistan.pk/terms-clinic.php")
            startActivity(intent)
        }

        tvCondition.setOnClickListener {
            val intent = Intent(this@SignUpActivity, WebviewActivity::class.java)
            intent.putExtra("url", "https://www.healthcarepakistan.pk/terms-clinic.php")
            startActivity(intent)
        }
        tvPolicy.setOnClickListener {
            val intent = Intent(this@SignUpActivity, WebviewActivity::class.java)
            intent.putExtra("url", "https://www.healthcarepakistan.pk/privacy-clinic.php")
            startActivity(intent)
        }

        btnNext.setOnClickListener {
            if (etFirstName.text.toString().isNotEmpty() &&
                    etLastName.text.toString().isNotEmpty() &&
                    etPassword.text.toString().isNotEmpty() &&
                    etEmail.text.toString().isNotEmpty()
            // && etPhone.text.toString().isNotEmpty()
            ) {

                if (isValidEmaillId(etEmail.text.toString())) {

                    Log.d("SignupActivity", "Email is valid")
                } else {
//                    Toast.makeText(applicationContext,"Invalid email format",5000).show()
                }

                if (etPassword.text.toString().length > 6) {
                    if(etPassword.text.toString() == etConfirmPassword.text.toString()) {

                        val params = HashMap<String, String>()
                        params["vEmailAddress"] = etEmail.text.toString()
                        params["vFirstName"] = etFirstName.text.toString()
                        params["password"] = etPassword.text.toString()
                        params["vLastName"] = etLastName.text.toString()
                        params["Device_type"] = "android"
                        params["Push_token"] = ""
                        params["Voip_token"] = ""
                        params["vCellNumber"] = ""

                        val jsonObject = JSONObject(params as Map<*, *>)
                        try {

                            val encrypt = AESEncryption.encrypt(jsonObject.toString(), Const.Encryption_Key, Const.Encryption_IV)
                            Log.d("dsasda", encrypt)
                            val finalParams = HashMap<String, String>()
                            finalParams["encryptObj"] = encrypt
                            val apiConnection = ApiConnection(this@SignUpActivity, this@SignUpActivity, finalParams, Const.SIGNUP)
                            apiConnection.makeStringReq()

                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    }
                    else {
                        Toast.makeText(this@SignUpActivity, "Password do not match with each other", Toast.LENGTH_SHORT).show()

                    }
//                    val apiConnection = ApiConnection(this@SignUpActivity, this@SignUpActivity, params, Const.SIGNUP)
//                    apiConnection.makeStringReq()
                } else {
                    Toast.makeText(this@SignUpActivity, "Please enter at least 7 characters password", Toast.LENGTH_SHORT).show()
                }

            } else {
                Toast.makeText(this@SignUpActivity, "All fields are mandatory. Please enter information in all fields", Toast.LENGTH_SHORT).show()
            }
        }


    }

    private fun isValidEmaillId(email: String): Boolean {

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches()
    }


    override fun success(response: String) {
        try {
            val jsonObject = JSONObject(response)
            val meta = jsonObject.getJSONObject("meta")
            val code = meta.getString("code")
            if (Integer.valueOf(code) == 200) {
                val jsonResponse = jsonObject.getJSONObject("response")
                val userId = jsonResponse.getString("userId")
                val mrn = jsonResponse.getString("MRN")
                val firstName = jsonResponse.getString("FirstName")
                val lastName = jsonResponse.getString("LastName")
                val dob = jsonResponse.getString("DOB")
                val gender = jsonResponse.getString("Gender")
                val address = jsonResponse.getString("Address")
                val city = jsonResponse.getString("City")
                val country = jsonResponse.getString("Country")
                val emailAddress = jsonResponse.getString("EmailAddress")
                val cellNumber = jsonResponse.getString("CellNumber")
                val twoFACode = jsonResponse.getString("TwoFACode")

                val sp = applicationContext.getSharedPreferences("appData", 0)
                val editor: SharedPreferences.Editor
                editor = sp.edit()
                editor.putString("userId", userId)
                editor.putString("mrn", mrn)
                editor.putString("firstName", firstName)
                editor.putString("lastName", lastName)
                editor.putString("dob", dob)
                editor.putString("gender", gender)
                editor.putString("address", address)
                editor.putString("city", city)
                editor.putString("country", country)
                editor.putString("emailAddress", emailAddress)
                editor.putString("cellNumber", cellNumber)
                editor.putString("twoFACode", twoFACode)
                editor.apply()

                if (twoFACode == "") {
                    val intent = Intent(this@SignUpActivity, ProfileActivity::class.java)
                    intent.putExtra("firstName", firstName)
                    intent.putExtra("lastName", lastName)
                    intent.putExtra("email", emailAddress)
                    intent.putExtra("password", etPassword.text.toString())
                    startActivity(intent)
                    finish()
                } else {
                    val intent = Intent(this@SignUpActivity, TabsActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    SaveData.SaveData(this@SignUpActivity, "exists", "true")
                    startActivity(intent)
                    finish()
//                    val intent = Intent(this@SignUpActivity, VarificationActivity::class.java)
//                    startActivity(intent)
                }
            } else if (Integer.valueOf(code) == 201) {
                val message = meta.getString("message")
                Toast.makeText(this, message, Toast.LENGTH_LONG).show()
            } else {
                val message = meta.getString("message")
                Toast.makeText(this, message, Toast.LENGTH_LONG).show()
            }

        } catch (e: JSONException) {
            e.printStackTrace()
        }

    }

    override fun error(response: String) {

    }
}
