package com.softech.ccpatient.activities

import android.Manifest
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import android.widget.Toast
import com.koushikdutta.async.future.FutureCallback
import com.koushikdutta.ion.Ion
import com.softech.ccpatient.R
import com.softech.ccpatient.adapters.DocumentAdapter
import com.softech.ccpatient.data.models.models.DocumentFolder.Document
import com.softech.ccpatient.sharedpreferences.FetchData
import com.softech.ccpatient.utils.AESEncryption
import com.softech.ccpatient.utils.Const
import com.softech.ccpatient.utils.FileUtils
import kotlinx.android.synthetic.main.activity_upload_document.*
import org.json.JSONException
import org.json.JSONObject
import java.io.File
import java.util.HashMap

class UploadDocumentActivity : AppCompatActivity() {
    internal var document: Document? = Document()
    internal var documentAdapter: DocumentAdapter? = null
    internal var REQUEST_IMAGE_CAPTURE = 0x1001
    internal var outputFile: File? = null
    private var pDialog: ProgressDialog? = null
    internal var mimetype: String? = String()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upload_document)
        recyclerviewDoc.setLayoutManager(LinearLayoutManager(this))
        recyclerviewDoc.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        recyclerviewDoc.setHasFixedSize(true)
        callDocumentService()
        pDialog = ProgressDialog(this)
        pDialog!!.setMessage("Loading...")
        pDialog!!.setCancelable(false)

        selectFileBtn?.setOnClickListener()
        {
            selectFile()

        }
        uploadFileBtn?.setOnClickListener()
        {

            uploadDocumentService()
        }
    }

    private fun uploadDocumentService() {
        if (!pDialog?.isShowing()!!) {
            pDialog?.show()
        }
        val consultantID = FetchData.getData(this, "userId")
        val access_token = FetchData.getData(this, "access_token")
        val patientID = FetchData.getData(this, "patientID")
        val folderID = FetchData.getData(this, "folderID")
        val params = HashMap<String, String>()
        params["action"] = "Document Upload"
        params["consultantId"] = consultantID
        params["access_token"] = access_token
        params["patientId"] = patientID
        params["folderId"] = folderID
        params["fileType"] = mimetype!!
        params["fileName"] = outputFile!!.name
        val jsonObject = JSONObject(params as Map<*, *>)
        val encryptString = AESEncryption.encrypt(jsonObject.toString(), Const.Encryption_Key, Const.Encryption_IV)
        Ion.with(this).load(Const.BASE_URL + Const.UPLOAD_DOCUMENT)
//                .setMultipartParameter("action", "Document Upload")
//                .setMultipartParameter("consultantId", consultantID)
//                .setMultipartParameter("fileName", outputFile?.name)
//                .setMultipartParameter("fileType", mimetype)
//                .setMultipartParameter("access_token", access_token)
//                .setMultipartParameter("patientId", patientID)
//                .setMultipartParameter("folderId", folderID)
                .setMultipartParameter("encryptObj", encryptString)
                .setMultipartFile("File", mimetype, outputFile)
                .asString()
                .setCallback(FutureCallback<String> { e, result ->
                    try {
                        if (pDialog?.isShowing()!!) {
                            pDialog?.dismiss()
                        }
                        val jsonObject = JSONObject(result)
                        val meta = jsonObject.getJSONObject("meta")
                        val code = meta.getString("code")
                        if (Integer.valueOf(code) == 200) {
                            val message = meta.getString("message")
                            Toast.makeText(this@UploadDocumentActivity, message, Toast.LENGTH_LONG).show()
                            this.finish()
                        } else if (Integer.valueOf(code) == 404) {
                            val message = jsonObject.getString("error")
                            Toast.makeText(this@UploadDocumentActivity, message, Toast.LENGTH_LONG).show()
                            //  recyclViewSearch.setVisibility(View.GONE);

                        }
                    } catch (e1: JSONException) {
                        e1.printStackTrace()
                    }
                })

    }

    private fun showFileChooser() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.type = "*/*"
        intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION or
                Intent.FLAG_GRANT_WRITE_URI_PERMISSION or Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION
        val i = Intent.createChooser(intent, "File")
        startActivityForResult(i, REQUEST_IMAGE_CAPTURE)
    }

    private fun selectFile() {
        if (FetchData.CheckPermission("Storage", Manifest.permission.WRITE_EXTERNAL_STORAGE, this, 101)) {
            showFileChooser()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_CAPTURE) {
            val selectedUri = data?.getData()
            if (selectedUri !== null) {
                val path = FileUtils.getPath(this@UploadDocumentActivity, selectedUri)
                outputFile = File(path!!)
                outputFile!!.exists()
                mimetype = FetchData.getMimeType(path)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 101) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (FetchData.CheckPermission("Storage", Manifest.permission.READ_EXTERNAL_STORAGE, this, 101)) {
                    showFileChooser()
                }

            }
        }

    }

    fun callDocumentService() {
        /*  val consultantID = FetchData.getData(this, "userId")
          val access_token = FetchData.getData(this, "access_token")
          val patientID = FetchData.getData(this, "patientID")
          val params = HashMap<String, String>()
          params["action"] = "Load Folders"
          params["consultantId"] = consultantID
          params["access_token"] = access_token
          params["patientId"] = patientID
          val apiConnection = ApiConnection(object : IWebListener {
              override fun success(response: String?) {

                  if (response != null) {

                      try {

                          val jsonObject = JSONObject(response)
                          val meta = jsonObject.getJSONObject("meta")
                          val code = meta.getString("code")
                          if (Integer.valueOf(code) == 200) {
                              val message = meta.getString("message")
                              document = Gson().fromJson(response, Document::class.java)
                              //                    linearLayout1.setVisibility(View.VISIBLE);
                              documentAdapter = DocumenpdftAdapter(this@UploadDocumentActivity, document?.getResponse(), true)
                              val mLayoutManager = LinearLayoutManager(this@UploadDocumentActivity)
                              recyclerviewDoc.setLayoutManager(mLayoutManager)
                              recyclerviewDoc.setItemAnimator(DefaultItemAnimator())
                              recyclerviewDoc.setAdapter(documentAdapter)
                              // mAdapter.notifyDataSetChanged();
                              recyclerviewDoc.addItemDecoration(DividerItemDecoration(this@UploadDocumentActivity, DividerItemDecoration.VERTICAL))

                          } else if (Integer.valueOf(code) == 504) {
                              document?.getResponse()?.clear()
                              val message = meta.getString("message")

                              Toast.makeText(this@UploadDocumentActivity, message, Toast.LENGTH_LONG).show()
                              //  recyclViewSearch.setVisibility(View.GONE);

                          }

                      } catch (e: JSONException) {
                          e.printStackTrace()
                      }

                      Logs.showLog("Login", response)
                  }
              }

              override fun error(response: String) {
                  Log.v("Error Add Appointment", "")

              }
          }, this, params,
                  Const.Documents)
          apiConnection.makeStringReq()*/
    }

}
