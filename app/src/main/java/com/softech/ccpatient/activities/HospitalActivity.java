package com.softech.ccpatient.activities;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;

import com.softech.ccpatient.R;
import com.softech.ccpatient.data.models.adapters.HospitalAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HospitalActivity extends AppCompatActivity {
    @BindView(R.id.recyclViewHospitals)
    RecyclerView recyclViewHospitals;
    private HospitalAdapter adapter;
    private ArrayList<String> hospitals = new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospital);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Hospitals");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HospitalActivity.this.finish();
            }
        });

        ButterKnife.bind(this);

        hospitals.add("Jinnah Hospitals");
        hospitals.add("General Hospitals");
        hospitals.add("International Hospitals");
        hospitals.add("Farooq Hospitals");

        adapter = new HospitalAdapter(hospitals, HospitalActivity.this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclViewHospitals.setLayoutManager(mLayoutManager);
        recyclViewHospitals.setItemAnimator(new DefaultItemAnimator());
        recyclViewHospitals.setAdapter(adapter);
    }

}
