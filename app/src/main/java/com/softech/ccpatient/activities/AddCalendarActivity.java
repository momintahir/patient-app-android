package com.softech.ccpatient.activities;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.softech.ccpatient.R;
import com.softech.ccpatient.apiconnections.ApiConnection;
import com.softech.ccpatient.data.models.models.SelectClinicModel;
import com.softech.ccpatient.interfaces.IWebListener;
import com.softech.ccpatient.sharedpreferences.FetchData;
import com.softech.ccpatient.utils.Const;
import com.softech.ccpatient.utils.Dialogues;
import com.softech.ccpatient.utils.Logs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddCalendarActivity extends AppCompatActivity implements IWebListener {

    @BindView(R.id.textViewStartTime)
    TextView textViewStartTime;
    @BindView(R.id.textViewEndTime)
    TextView textViewEndTime;
    @BindView(R.id.textViewStartDate)
    TextView textViewStartDate;
    @BindView(R.id.textViewEndDate)
    TextView textViewEndDate;
    @BindView(R.id.editTextClinicName)
    EditText editTextClinicName;
    @BindView(R.id.tgBMonday)
    RadioButton tgBMonday;
    @BindView(R.id.tgBTuesday)
    RadioButton tgBTuesday;
    @BindView(R.id.tgBWednesday)
    RadioButton tgBWednesday;
    @BindView(R.id.tgBThursday)
    RadioButton tgBThursday;
    @BindView(R.id.tgBFriday)
    RadioButton tgBFriday;
    @BindView(R.id.radioGroup1)
    RadioGroup radioGroup1;
    @BindView(R.id.spinnerClinicList)
    Spinner spinnerClinicList;
    @BindView(R.id.spinnerSlots)
    Spinner spinnerSlots;
    @BindView(R.id.buttonAdd)
    Button buttonAdd;
    String clinicID;
    final Calendar myCalendar = Calendar.getInstance();
    ArrayList<String> slots = new ArrayList<String>();
    String mondayStartTime = "", mondayEndTime = "", tuesStartTime = "",
            tuesEndTime = "", wedStartTime = "", wedEndTime = "",
            thuStartTime = "", thuEndTime = "", friStartTime = "",
            friEndTime = "", satStartTime = "", satEndTime = "",
            sunStartTime = "", sunEndTime = "",
            monSlot = "", tuesSlot = "", wedSlot = "", thursSlot = "", friSlot = "", satSlot = "", sunSlot = "";
    boolean mon = false, tue = false, wed = false, thu = false, fri = false, sat = false, sun = false;
    private String creatAccount = "";
    ArrayList<SelectClinicModel> connectedClinicsModelArrayList = new ArrayList<SelectClinicModel>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_calendar);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Add Calendar");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddCalendarActivity.this.finish();
            }
        });

        ButterKnife.bind(this);
        Bundle intent = getIntent().getExtras();
        if (intent != null) {
            creatAccount = intent.getString("fromTabs", "");
        }

        textViewStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showStartTime();


            }
        });
        textViewEndTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showEndTime();


            }
        });

        tgBMonday.setChecked(true);
        mon = true;
        radioGroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                if (checkedId == R.id.tgBMonday) {
                    mon = true;
                    tue = false;
                    wed = false;
                    thu = false;
                    fri = false;
                    sat = false;
                    sun = false;
                    if (mondayStartTime.equals("")) {
                        textViewStartTime.setText("Tap to add Start Time");
                    } else {
                        textViewStartTime.setText(mondayStartTime);
                    }
                    if (mondayEndTime.equals("")) {
                        textViewEndTime.setText("Tap to add End Time");
                    } else {
                        textViewEndTime.setText(mondayEndTime);
                    }

                } else if (checkedId == R.id.tgBTuesday) {
                    mon = false;
                    tue = true;
                    wed = false;
                    thu = false;
                    fri = false;
                    sat = false;
                    sun = false;
                    if (tuesStartTime.equals("")) {
                        textViewStartTime.setText("Tap to add Start Time");
                    } else {
                        textViewStartTime.setText(tuesStartTime);
                    }
                    if (tuesEndTime.equals("")) {
                        textViewEndTime.setText("Tap to add End Time");
                    } else {
                        textViewEndTime.setText(tuesEndTime);
                    }


                } else if (checkedId == R.id.tgBWednesday) {
                    mon = false;
                    tue = false;
                    wed = true;
                    thu = false;
                    fri = false;
                    sat = false;
                    sun = false;

                    if (wedStartTime.equals("")) {
                        textViewStartTime.setHint("Tap to add Start Time");
                    } else {
                        textViewStartTime.setText(wedStartTime);
                    }
                    if (wedEndTime.equals("")) {
                        textViewEndTime.setHint("Tap to add End Time");
                    } else {
                        textViewEndTime.setText(wedEndTime);
                    }

                } else if (checkedId == R.id.tgBThursday) {
                    mon = false;
                    tue = false;
                    wed = false;
                    thu = true;
                    fri = false;
                    sat = false;
                    sun = false;
                    if (thuStartTime.equals("")) {
                        textViewStartTime.setHint("Tap to add Start Time");
                    } else {
                        textViewStartTime.setText(thuStartTime);
                    }
                    if (thuEndTime.equals("")) {
                        textViewEndTime.setHint("Tap to add End Time");
                    } else {
                        textViewEndTime.setText(thuEndTime);
                    }

                } else if (checkedId == R.id.tgBFriday) {
                    mon = false;
                    tue = false;
                    wed = false;
                    thu = false;
                    fri = true;
                    sat = false;
                    sun = false;
                    if (friStartTime.equals("")) {
                        textViewStartTime.setText("Tap to add Start Time");
                    } else {
                        textViewStartTime.setText(friStartTime);
                    }
                    if (friEndTime.equals("")) {
                        textViewEndTime.setText("Tap to add End Time");
                    } else {
                        textViewEndTime.setText(friEndTime);
                    }

                } else if (checkedId == R.id.tgBSaturday) {
                    mon = false;
                    tue = false;
                    wed = false;
                    thu = false;
                    fri = false;
                    sat = true;
                    sun = false;
                    if (satStartTime.equals("")) {
                        textViewStartTime.setText("Tap to add Start Time");
                    } else {
                        textViewStartTime.setText(satStartTime);
                    }
                    if (satEndTime.equals("")) {
                        textViewEndTime.setText("Tap to add End Time");
                    } else {
                        textViewEndTime.setText(satEndTime);
                    }

                } else if (checkedId == R.id.tgBSunday) {
                    mon = false;
                    tue = false;
                    wed = false;
                    thu = false;
                    fri = false;
                    sat = false;
                    sun = true;
                    if (sunStartTime.equals("")) {
                        textViewStartTime.setText("Tap to add Start Time");
                    } else {
                        textViewStartTime.setText(sunStartTime);
                    }
                    if (sunEndTime.equals("")) {
                        textViewEndTime.setText("Tap to add End Time");
                    } else {
                        textViewEndTime.setText(sunEndTime);
                    }

                }
            }
        });


        textViewStartTime.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (mon == true) {
                    mondayStartTime = textViewStartTime.getText().toString();
                } else if (tue == true) {
                    tuesStartTime = textViewStartTime.getText().toString();
                } else if (wed == true) {
                    wedStartTime = textViewStartTime.getText().toString();
                } else if (thu == true) {
                    thuStartTime = textViewStartTime.getText().toString();
                } else if (fri == true) {
                    friStartTime = textViewStartTime.getText().toString();
                } else if (sat == true) {
                    satStartTime = textViewStartTime.getText().toString();
                } else if (sun == true) {
                    sunStartTime = textViewStartTime.getText().toString();
                }

            }
        });


        textViewEndTime.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (mon == true) {
                    mondayEndTime = textViewEndTime.getText().toString();
                } else if (tue == true) {
                    tuesEndTime = textViewEndTime.getText().toString();
                } else if (wed == true) {
                    wedEndTime = textViewEndTime.getText().toString();
                } else if (thu == true) {
                    thuEndTime = textViewEndTime.getText().toString();
                } else if (fri == true) {
                    friEndTime = textViewEndTime.getText().toString();
                } else if (sat == true) {
                    satEndTime = textViewEndTime.getText().toString();
                } else if (sun == true) {
                    sunEndTime = textViewEndTime.getText().toString();
                }
            }
        });

        spinnerSlots.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (mon == true) {
                    monSlot = spinnerSlots.getSelectedItem().toString();
                    tuesSlot = spinnerSlots.getSelectedItem().toString();
                    wedSlot = spinnerSlots.getSelectedItem().toString();
                    thursSlot = spinnerSlots.getSelectedItem().toString();
                    friSlot = spinnerSlots.getSelectedItem().toString();
                    satSlot = spinnerSlots.getSelectedItem().toString();
                    sunSlot = spinnerSlots.getSelectedItem().toString();
                } else if (tue == true) {
                    tuesSlot = spinnerSlots.getSelectedItem().toString();
                    wedSlot = spinnerSlots.getSelectedItem().toString();
                    thursSlot = spinnerSlots.getSelectedItem().toString();
                    friSlot = spinnerSlots.getSelectedItem().toString();
                    satSlot = spinnerSlots.getSelectedItem().toString();
                    sunSlot = spinnerSlots.getSelectedItem().toString();
                } else if (wed == true) {
                    wedSlot = spinnerSlots.getSelectedItem().toString();
                    thursSlot = spinnerSlots.getSelectedItem().toString();
                    friSlot = spinnerSlots.getSelectedItem().toString();
                    satSlot = spinnerSlots.getSelectedItem().toString();
                    sunSlot = spinnerSlots.getSelectedItem().toString();
                } else if (thu == true) {
                    thursSlot = spinnerSlots.getSelectedItem().toString();
                    friSlot = spinnerSlots.getSelectedItem().toString();
                    satSlot = spinnerSlots.getSelectedItem().toString();
                    sunSlot = spinnerSlots.getSelectedItem().toString();
                } else if (fri == true) {
                    friSlot = spinnerSlots.getSelectedItem().toString();
                    satSlot = spinnerSlots.getSelectedItem().toString();
                    sunSlot = spinnerSlots.getSelectedItem().toString();
                } else if (sat == true) {
                    satSlot = spinnerSlots.getSelectedItem().toString();

                    sunSlot = spinnerSlots.getSelectedItem().toString();
                } else if (sun == true) {
                    sunSlot = spinnerSlots.getSelectedItem().toString();
                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addCalendar();
            }
        });

//        String ClinicList = FetchData.getData(AddCalendarActivity.this, "cliniclist");
//        Gson gson = new Gson();
//        Type type = new TypeToken<ArrayList<CliniclistModel>>() {
//        }.getType();
//
//        final ArrayList<SelectClinicModel> connectedClinicsModelArrayList = gson.fromJson(ClinicList.toString(), type);
//
//        ArrayAdapter<SelectClinicModel> adapter =
//                new ArrayAdapter<SelectClinicModel>(AddCalendarActivity.this, R.layout.spinner, connectedClinicsModelArrayList);
//        adapter.setDropDownViewResource(R.layout.spinner);
//        spinnerClinicList.setAdapter(adapter);


        textViewStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(AddCalendarActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


        textViewEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(AddCalendarActivity.this, date1, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


        spinnerClinicList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (connectedClinicsModelArrayList.size() > 0) {
                    SelectClinicModel cliniclistModel = connectedClinicsModelArrayList.get(position);
                    clinicID = cliniclistModel.getOID();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        addSlots();
        getClinincs();
    }


    SimpleDateFormat dfDate = new SimpleDateFormat("dd/MM/yyyy");


    public boolean CheckTime() {
        boolean correct = false;
        String startTime = textViewStartTime.getText().toString();
        try {

            Date startTimeDate = new SimpleDateFormat("HH:mm:ss").parse(startTime);
            String endTime = textViewEndTime.getText().toString();
            Date endTimeDate = new SimpleDateFormat("HH:mm:ss").parse(endTime);

            if (endTimeDate.after(startTimeDate)) {
                //checkes whether the current time is between 14:49:00 and 20:11:13.
                correct = false;

            } else {
                correct = false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return correct;
    }

    public boolean CheckTime(String startTime, String endTime) {
        boolean correct = false;
        try {
            if (!(startTime.length() > 0) || (!(endTime.length() > 0))) {
                return true;
            }

            if ((startTime.equals("Tap to add Start Time")) || endTime.equals("Tap to add End Time")) {
                return true;
            }
            Date startTimeDate = new SimpleDateFormat("HH:mm aa").parse(startTime);
            Date endTimeDate = new SimpleDateFormat("HH:mm aa").parse(endTime);

            if (endTimeDate.after(startTimeDate)) {
                correct = true;

            } else {
                correct = false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return correct;
    }

    public boolean CheckDates() {
        String startDate = textViewStartDate.getText().toString();
        String endDate = textViewEndDate.getText().toString();
        boolean b = false;
        try {
            if (dfDate.parse(startDate).before(dfDate.parse(endDate))) {
                b = true;//If start date is before end date
            }

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return b;
    }

    private void getClinincs() {
        Map<String, String> params = new HashMap<String, String>();
        String consultantID = FetchData.getData(AddCalendarActivity.this, "userId");
        String access_token = FetchData.getData(AddCalendarActivity.this, "access_token");
        params.put("action", "Select Any Clinic from List");
        params.put("consultantId", consultantID);
        params.put("access_token", access_token);


        ApiConnection apiConnection = new ApiConnection(new IWebListener() {
            @Override
            public void success(String response) {

                if (response != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject meta = jsonObject.getJSONObject("meta");
                        String code = meta.getString("code");
                        if (Integer.valueOf(code) == 200) {

                            JSONArray responseObject = jsonObject.getJSONArray("response");


                            Gson gson = new Gson();
                            Type type = new TypeToken<ArrayList<SelectClinicModel>>() {
                            }.getType();

                            connectedClinicsModelArrayList = gson.fromJson(responseObject.toString(), type);

                            ArrayAdapter<SelectClinicModel> adapter =
                                    new ArrayAdapter<SelectClinicModel>(AddCalendarActivity.this, R.layout.spinner,
                                            connectedClinicsModelArrayList);
                            adapter.setDropDownViewResource(R.layout.spinner);
                            spinnerClinicList.setAdapter(adapter);

                            if(connectedClinicsModelArrayList.size()>0) {
                                SelectClinicModel cliniclistModel = connectedClinicsModelArrayList.get(0);
                                clinicID = cliniclistModel.getOID();
                            }

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Logs.showLog("Login", response);
                }
            }

            @Override
            public void error(String response) {

            }
        }, AddCalendarActivity.this, params, Const.SELECT_CLINIC);
        apiConnection.makeStringReq();

    }

    private void addSlots() {
        int slotInt = 5;
        for (int i = 0; i < 12; i++) {
            slots.add(String.valueOf(slotInt));
            slotInt = slotInt + 5;

        }

        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(AddCalendarActivity.this, R.layout.spinner, slots);
        adapter.setDropDownViewResource(R.layout.spinner2);
        spinnerSlots.setAdapter(adapter);

    }

    private void showStartTime() {
        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        String format;
                        if (hourOfDay == 0) {

                            hourOfDay += 12;

                            format = "AM";
                        } else if (hourOfDay == 12) {

                            format = "PM";

                        } else if (hourOfDay > 12) {

                            hourOfDay -= 12;

                            format = "PM";

                        } else {

                            format = "AM";
                        }

                        textViewStartTime.setText(String.format("%02d:%02d", hourOfDay, minute) + " " + format);


                    }
                }, mHour, mMinute, false);

        timePickerDialog.show();

    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }

    };

    private void updateLabel() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        textViewStartDate.setText(sdf.format(myCalendar.getTime()));
    }

    DatePickerDialog.OnDateSetListener date1 = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel1();
        }

    };

    private void updateLabel1() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        textViewEndDate.setText(sdf.format(myCalendar.getTime()));
    }


    private void showEndTime() {
        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        String format;
                        if (hourOfDay == 0) {

                            hourOfDay += 12;

                            format = "AM";
                        } else if (hourOfDay == 12) {

                            format = "PM";

                        } else if (hourOfDay > 12) {

                            hourOfDay -= 12;

                            format = "PM";

                        } else {

                            format = "AM";
                        }


                        textViewEndTime.setText(String.format("%02d:%02d", hourOfDay, minute) + " " + format);

                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();

    }


    private void addCalendar() {

        if (!(CheckDates())) {
            Toast.makeText(this, "End Date Should be after Start Date.", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!checkTime()) {
            return;
        }


        if (!(checkDaysTime())) {
            return;
        }
        if (editTextClinicName.getText().toString().length() > 0) {
            if (textViewStartDate.getText().toString().length() > 0) {
                if (textViewEndDate.getText().toString().length() > 0) {

                    String consultantID = FetchData.getData(AddCalendarActivity.this, "userId");
                    String access_token = FetchData.getData(AddCalendarActivity.this, "access_token");

                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("action", "Consultant Calendar Request");
                        jsonObject.put("consultantId", consultantID);
                        jsonObject.put("access_token", access_token);
                        jsonObject.put("clinicId", clinicID);
                        jsonObject.put("calendarName", editTextClinicName.getText().toString());
                        jsonObject.put("startDate", textViewStartDate.getText().toString());
                        jsonObject.put("endDate", textViewEndDate.getText().toString());
                        jsonObject.put("Mon", mondayDataSlot());
                        jsonObject.put("Tue", tuesdayDataSlot());
                        jsonObject.put("Wed", wednesdayDataSlot());
                        jsonObject.put("Thu", thursdayDataSlot());
                        jsonObject.put("Fri", fridayDataSlot());
                        jsonObject.put("Sat", saturdayDataSlot());
                        jsonObject.put("Sun", sundayDataSlot());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    ApiConnection apiConnection = new ApiConnection(AddCalendarActivity.this,
                            AddCalendarActivity.this, jsonObject, Const.ADDCALANDER);
                    apiConnection.makeStringReq1();


                } else {

                    Toast.makeText(this, "Please add end date", Toast.LENGTH_SHORT).show();

                }
            } else {

                Toast.makeText(this, "Please add start date", Toast.LENGTH_SHORT).show();

            }
        } else {
            Toast.makeText(this, "Please add calendar name", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean checkDaysTime() {

        boolean time = true;
        if (!(CheckTime(mondayStartTime, mondayEndTime))) {

            Toast.makeText(AddCalendarActivity.this, "Monday time is not correct.", Toast.LENGTH_SHORT).show();
            return time = false;

        } else if (!(CheckTime(tuesStartTime, tuesEndTime))) {
            Toast.makeText(AddCalendarActivity.this, "Tuesday time is not correct.", Toast.LENGTH_SHORT).show();
            return time = false;
        } else if (!(CheckTime(wedStartTime, wedEndTime))) {
            Toast.makeText(AddCalendarActivity.this, "Tuesday time is not correct.", Toast.LENGTH_SHORT).show();
            return time = false;
        } else if (!(CheckTime(thuStartTime, thuEndTime))) {
            Toast.makeText(AddCalendarActivity.this, "Thursday time is not correct.", Toast.LENGTH_SHORT).show();
            return time = false;
        } else if (!(CheckTime(friStartTime, friEndTime))) {
            Toast.makeText(AddCalendarActivity.this, "Friday time is not correct.", Toast.LENGTH_SHORT).show();
            return time = false;
        } else if (!(CheckTime(satStartTime, satEndTime))) {
            Toast.makeText(AddCalendarActivity.this, "Saturday time is not correct.", Toast.LENGTH_SHORT).show();
            return time = false;
        } else if (!(CheckTime(sunStartTime, sunEndTime))) {
            Toast.makeText(AddCalendarActivity.this, "Sunday time is not correct.", Toast.LENGTH_SHORT).show();
            return time = false;
        }

        return time = true;
    }


    private boolean checkTime() {
        boolean val = true;
        if (((mondayStartTime.length() > 0) && !(mondayStartTime.equals("Tap to add Start Time")) && (mondayEndTime.equals("Tap to add End Time"))) ||
                (((!(mondayEndTime.equals("Tap to add End Time") && mondayEndTime.length() > 0)) && (mondayStartTime.equals("Tap to add Start Time"))))) {
            val = false;
            Toast.makeText(this, "Please add correct time for monday", Toast.LENGTH_SHORT).show();
            return val;
        } else if (((tuesStartTime.length() > 0) && !(tuesStartTime.equals("Tap to add Start Time")) && (tuesEndTime.equals("Tap to add End Time"))) ||
                (((!(tuesEndTime.equals("Tap to add End Time")) && tuesEndTime.length() > 0) && (tuesStartTime.equals("Tap to add Start Time"))))) {
            val = false;
            Toast.makeText(this, "Please add correct time for tuesday", Toast.LENGTH_SHORT).show();
            return val;
        } else if (((wedStartTime.length() > 0) && !(wedStartTime.equals("Tap to add Start Time")) && (wedEndTime.equals("Tap to add End Time"))) ||
                (((!(wedEndTime.equals("Tap to add End Time")) && wedEndTime.length() > 0) && (wedStartTime.equals("Tap to add Start Time"))))) {
            val = false;
            Toast.makeText(this, "Please add correct time for wednesday", Toast.LENGTH_SHORT).show();
            return val;
        } else if (((thuStartTime.length() > 0 && !(thuStartTime.equals("Tap to add Start Time"))) && (thuEndTime.equals("Tap to add End Time"))) ||
                (((!(thuEndTime.equals("Tap to add End Time")) && thuEndTime.length() > 0) && (thuStartTime.equals("Tap to add Start Time"))))) {
            val = false;
            Toast.makeText(this, "Please add correct time for thursday", Toast.LENGTH_SHORT).show();
            return val;
        } else if (((friStartTime.length() > 0) && !(friStartTime.equals("Tap to add Start Time")) && (friEndTime.equals("Tap to add End Time"))) ||
                (((!(friEndTime.equals("Tap to add End Time")) && friEndTime.length() > 0) && (friStartTime.equals("Tap to add Start Time"))))) {
            val = false;
            Toast.makeText(this, "Please add correct time for friday", Toast.LENGTH_SHORT).show();
            return val;
        } else if ((((satStartTime.length() > 0) && !satStartTime.equals("Tap to add Start Time")) && (satEndTime.equals("Tap to add End Time"))) ||
                (((!(satEndTime.equals("Tap to add End Time")) && satEndTime.length() > 0) && (satStartTime.equals("Tap to add Start Time"))))) {
            val = false;
            Toast.makeText(this, "Please add correct time for saturday", Toast.LENGTH_SHORT).show();
            return val;
        } else if (((sunStartTime.length() > 0 && !(sunStartTime.equals("Tap to add Start Time"))) && (sunEndTime.equals("Tap to add End Time"))) ||
                (((!(sunEndTime.equals("Tap to add End Time")) && sunEndTime.length() > 0) && (sunStartTime.equals("Tap to add Start Time"))))) {
            val = false;
            Toast.makeText(this, "Please add correct time for sunday", Toast.LENGTH_SHORT).show();
            return val;
        }


        return val;

    }

    private JSONObject mondayDataSlot() {

        JSONObject jsonObject = new JSONObject();
        textViewStartTime.setHint("Tap to add Start Time");
        textViewEndTime.setHint("Tap to add End Time");


        try {
            jsonObject.put("slotId", "-1");
            if (mondayStartTime.equals("Tap to add Start Time")) {
                jsonObject.put("startTime", "");
            } else {
                jsonObject.put("startTime", mondayStartTime);
            }
            if (textViewEndTime.equals("Tap to add End Time")) {
                jsonObject.put("endTime", "");
            } else {
                jsonObject.put("endTime", mondayEndTime);
            }
            jsonObject.put("slotDuration", monSlot);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jsonObject;


    }

    private JSONObject tuesdayDataSlot() {

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("slotId", "-1");
            if (tuesStartTime.equals("Tap to add Start Time")) {
                jsonObject.put("startTime", "");
            } else {
                jsonObject.put("startTime", tuesStartTime);
            }
            if (tuesEndTime.equals("Tap to add End Time")) {
                jsonObject.put("endTime", "");
            } else {
                jsonObject.put("endTime", tuesEndTime);
            }

          ;
            jsonObject.put("slotDuration", tuesSlot);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jsonObject;


    }

    private JSONObject wednesdayDataSlot() {

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("slotId", "-1");
            if (wedStartTime.equals("Tap to add Start Time")) {
                jsonObject.put("startTime", "");
            } else {
                jsonObject.put("startTime", wedStartTime);
            }
            if (wedEndTime.equals("Tap to add End Time")) {
                jsonObject.put("endTime", "");
            } else {
                jsonObject.put("endTime", wedEndTime);
            }

            jsonObject.put("slotDuration", wedSlot);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;

    }

    private JSONObject thursdayDataSlot() {

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("slotId", "-1");
            if (thuStartTime.equals("Tap to add Start Time")) {
                jsonObject.put("startTime", "");
            } else {
                jsonObject.put("startTime", thuStartTime);
            }
            if (thuEndTime.equals("Tap to add End Time")) {
                jsonObject.put("endTime", "");
            } else {
                jsonObject.put("endTime", thuEndTime);
            }

            jsonObject.put("slotDuration", thursSlot);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jsonObject;


    }

    private JSONObject fridayDataSlot() {

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("slotId", "-1");
            if (friStartTime.equals("Tap to add Start Time")) {
                jsonObject.put("startTime", "");
            } else {
                jsonObject.put("startTime", friStartTime);
            }
            if (friEndTime.equals("Tap to add End Time")) {
                jsonObject.put("endTime", "");
            } else {
                jsonObject.put("endTime", friEndTime);
            }

            jsonObject.put("slotDuration", friSlot);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jsonObject;


    }

    private JSONObject saturdayDataSlot() {

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("slotId", "-1");

            if (satStartTime.equals("Tap to add Start Time")) {
                jsonObject.put("startTime", "");
            } else {
                jsonObject.put("startTime", satStartTime);
            }
            if (satEndTime.equals("Tap to add End Time")) {
                jsonObject.put("endTime", "");
            } else {
                jsonObject.put("endTime", satEndTime);
            }



            jsonObject.put("slotDuration", satSlot);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jsonObject;


    }

    private JSONObject sundayDataSlot() {

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("slotId", "-1");
            if (sunStartTime.equals("Tap to add Start Time")) {
                jsonObject.put("startTime", "");
            } else {
                jsonObject.put("startTime", sunStartTime);
            }
            if (sunEndTime.equals("Tap to add End Time")) {
                jsonObject.put("endTime", "");
            } else {
                jsonObject.put("endTime", sunEndTime);
            }

            jsonObject.put("slotDuration", sunSlot);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jsonObject;


    }

    @Override
    public void success(String response) {
        if (response != null) {

            try {

                JSONObject jsonObject = new JSONObject(response);
                JSONObject meta = jsonObject.getJSONObject("meta");
                String code = meta.getString("code");
                if (Integer.valueOf(code) == 200) {

                    String response1 = jsonObject.getString("response");
                    Toast.makeText(this, "Calendar Added.", Toast.LENGTH_SHORT).show();
                    if (creatAccount.length() > 0) {
                        AddCalendarActivity.this.finish();
                    } else {
                        startActivity(new Intent(AddCalendarActivity.this, TabsActivity.class));
                    }

                } else if (Integer.valueOf(code) == 504) {

                    String error = jsonObject.getString("error");
                    Dialogues.showOkDialogue(AddCalendarActivity.this, error);
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

            Logs.showLog("Login", response);
        }

    }

    @Override
    public void error(String response) {
        Toast.makeText(this, "An Error Occured, Calendar Not Added", Toast.LENGTH_SHORT).show();

    }
}
