package com.softech.ccpatient.activities

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.softech.ccpatient.R
import com.softech.ccpatient.apiconnections.ApiConnection
import com.softech.ccpatient.interfaces.IWebListener
import com.softech.ccpatient.utils.Const
import com.stripe.android.ApiResultCallback
import com.stripe.android.Stripe
import com.stripe.android.model.Source
import com.stripe.android.model.SourceParams
import kotlinx.android.synthetic.main.activity_checkout.*
import org.json.JSONObject

class CheckoutActivity : AppCompatActivity(), IWebListener {
    var FirstName= ""
    var LastName=""
    var Email =""
    var Password=""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_checkout)
        val intent = intent.extras
        if (intent != null) {
            FirstName = intent.getString("firstName", "")
             LastName = intent.getString("lastName", "")
             Email = intent.getString("email", "")
            Password=intent.getString("password", "")
        }

        payButton.setOnClickListener {

            val stripe = Stripe(
                    applicationContext,
                    "pk_test_8tozyiXNwGQSYV2VUZ9VvJ4100mLNxIoWK"
            )
            val card = cardInputWidget.card
            val cardSourceParams = SourceParams.createCardParams(card!!)
// The asynchronous way to do it. Call this method on the main thread.
            stripe.createSource(
                    cardSourceParams,
                    callback = object : ApiResultCallback<Source> {
                        override fun onSuccess(source: Source) {
                            // Store the source somewhere, use it, etc
                            Log.d("asd","asd")
                            val sp = applicationContext.getSharedPreferences("appData", 0)
                            val editor: SharedPreferences.Editor
                            editor = sp.edit()
                            editor.putString("stripeSourceId",source.id)

                            val jsonObject = JSONObject()
                            try {
                                jsonObject.put("PatientID", sp.getString("userId","").toString())
                                jsonObject.put("SourceID", source.id.toString())

                                val apiConnection = ApiConnection(this@CheckoutActivity, this@CheckoutActivity, jsonObject, Const.ADD_PATIENT_PAYMENT_SOURCE)
                                apiConnection.makeStringReq1()
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                        }

                        override fun onError(error: Exception) {
                            // Tell the user that something went wrong
                            Log.d("asd","asd")

                        }
                    }
            )

// The synchronous way to do it (DON'T DO BOTH)
//            val source = stripe.createSourceSynchronous(cardSourceParams)
        }
    }

    override fun error(response: String?) {
        Log.d("asd","asd")
    }

    override fun success(response: String?) {
        val intent = Intent(this@CheckoutActivity, ProfileActivity::class.java)
        intent.putExtra("firstName",FirstName)
        intent.putExtra("lastName",LastName)
        intent.putExtra("email",Email)
        intent.putExtra("password",Password)
        startActivity(intent)
        finish()
    }


}
