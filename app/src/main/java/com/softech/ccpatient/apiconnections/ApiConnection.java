package com.softech.ccpatient.apiconnections;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.softech.ccpatient.activities.LoginActivity;
import com.softech.ccpatient.app.AppController;
import com.softech.ccpatient.interfaces.IWebListener;
import com.softech.ccpatient.sharedpreferences.SaveData;
import com.softech.ccpatient.utils.Const;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Robin.Yaqoob on 12-Oct-17.
 */

public class ApiConnection {

    IWebListener iWeb;
    private ProgressDialog pDialog;
    private Activity activity;
    private String tag_string_req = "string_req";
    private String TAG = "Android Web Call";
    Map<String, String> params = new HashMap<String, String>();
    JSONObject params1;
    String method;


    public ApiConnection(IWebListener iWeb, Activity activity, Map<String, String> params, String method) {
        this.iWeb = iWeb;
        this.activity = activity;
        this.params = params;
        this.method = method;
    }

    public ApiConnection(IWebListener iWeb, Activity activity, JSONObject params1, String method) {
        this.iWeb = iWeb;
        this.activity = activity;
        this.params1 = params1;
        this.method = method;
    }

    public ApiConnection(IWebListener iWeb, Map<String, String> params, String method) {
        this.iWeb = iWeb;
        this.params = params;
        this.method = method;
    }

    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


    public void makeStringReq() {

        pDialog = new ProgressDialog(activity);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        showProgressDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST, Const.BASE_URL + method, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                hideProgressDialog();
                Log.d(TAG, response.toString());
                //accessTokenExpired(response.toString());

                iWeb.success(response.toString());
                //    msgResponse.setText(response.toString());


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                VolleyLog.d(TAG, "Error: " + error.getMessage());

                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        Log.v("", "");
                    } catch (UnsupportedEncodingException e1) {
                        e1.printStackTrace();
                    }
                }
                iWeb.error(error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() {


                return params;
            }

        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                300000000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }


    public void makeStringReqService() {


        StringRequest strReq = new StringRequest(Request.Method.POST, Const.BASE_URL + method, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                //accessTokenExpired(response.toString());

                iWeb.success(response.toString());
                //    msgResponse.setText(response.toString());


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());

                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        Log.v("", "");
                    } catch (UnsupportedEncodingException e1) {
                        e1.printStackTrace();
                    }
                }
                iWeb.error(error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() {


                return params;
            }

        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                300000000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }


    public void makeStringReq1() {

        pDialog = new ProgressDialog(activity);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        showProgressDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                Const.BASE_URL + method, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
             //   accessTokenExpired(response.toString());
                iWeb.success(response.toString());
                hideProgressDialog();

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hideProgressDialog();

                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        JSONObject obj = new JSONObject(res);
                    } catch (UnsupportedEncodingException e1) {
                        e1.printStackTrace();
                    } catch (JSONException e2) {
                        e2.printStackTrace();
                    }
                }
                iWeb.error(error.getMessage());
            }
        }) {

            @Override
            public byte[] getBody() throws AuthFailureError {


                return params1.toString().getBytes();
            }

            public String getBodyContentType() {
                return "application/json";
            }

        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                300000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    private void accessTokenExpired(String response) {

        try {
            JSONObject res = new JSONObject(response);
            JSONObject meta = res.getJSONObject("meta");
            String code = meta.getString("code");
            if (Integer.valueOf(code) == 404) {
                SaveData.SaveData(this.activity, "exists", "false");
                SharedPreferences preferences = this.activity.getSharedPreferences("appData", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.clear();
                editor.commit();
                Intent intent = new Intent(activity, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                activity.startActivity(intent);


            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
