package com.softech.ccpatient.apiconnections;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.softech.ccpatient.app.AppController;
import com.softech.ccpatient.interfaces.IWebListener;
import com.softech.ccpatient.utils.Const;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Robin.Yaqoob on 12-Oct-17.
 */

public class ApiConnectionBackground1 {

    IWebListener iWeb;
     private Context activity;
     private String tag_string_req = "string_req";
    private String TAG = "Android Web Call";
    Map<String, String> params = new HashMap<String, String>();
    JSONObject params1;
    String method;


    public ApiConnectionBackground1(IWebListener iWeb, Context activity, Map<String, String> params, String method) {
        this.iWeb = iWeb;
        this.activity = activity;
        this.params = params;
        this.method = method;
    }




    public void makeStringReq() {


        StringRequest strReq = new StringRequest(Request.Method.POST,
                Const.BASE_URL + method, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                iWeb.success(response.toString());
                //    msgResponse.setText(response.toString());

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());

                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                         JSONObject obj = new JSONObject(res);
                    } catch (UnsupportedEncodingException e1) {
                         e1.printStackTrace();
                    } catch (JSONException e2) {
                        // returned data is not JSONObject?
                        e2.printStackTrace();
                    }
                }
                iWeb.error(error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() {


                return params;
            }

        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }


    public void makeStringReq1() {



        StringRequest strReq = new StringRequest(Request.Method.POST,
                Const.BASE_URL + method, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                iWeb.success(response.toString());

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
 
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        JSONObject obj = new JSONObject(res);
                    } catch (UnsupportedEncodingException e1) {
                        e1.printStackTrace();
                    } catch (JSONException e2) {
                        e2.printStackTrace();
                    }
                }
                iWeb.error(error.getMessage());
            }
        }) {

            @Override
            public byte[] getBody() throws AuthFailureError {


                return params1.toString().getBytes();
            }

            public String getBodyContentType() {
                return "application/json";
            }

        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

         AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }
}
